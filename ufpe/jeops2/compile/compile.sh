DIR=$(pwd);
cd ..;

rule_files=($(find ./src -name "*.rules"));

java_files=($(find ./src/jeops/examples -name "*.java"));


for (( i = 0 ; i < ${#java_files[@]} ; i++ )) 
do 
echo javac -d ./bin -cp ./bin ${java_files[$i]}
#javac -d ./bin -cp ./bin ${java_files[$i]}
done


for (( i = 0 ; i < ${#rule_files[@]} ; i++ )) 
do 
echo java -cp ./bin jeops.compiler.Main ${rule_files[$i]}
#java -cp ./bin jeops.compiler.Main ${rule_files[$i]}
done



cd $DIR;
