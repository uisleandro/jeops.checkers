package jeops.examples.queens;

  class Jeops_RuleBase_EightQueens extends jeops.AbstractRuleBase {

	private int solutionNumber = 1;

	
    /**
     * Sets the local declaration for a given declaration index.
     * As there are no local declarations for this rule, this
     * method does nothing.
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_findSolution(int index) {
    }

    /**
     * Returns a dependency table between the local declarations 
     * and the regular ones for rule findSolution. As here are
     * no local declarations for this rule, this method returns an
     * empty array.
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_findSolution() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule findSolution
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_findSolution(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule findSolution
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule findSolution
     */
    private java.util.Hashtable getMapRulefindSolution() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("q1", jeops_examples_queens_Queen_1);
        result.put("q2", jeops_examples_queens_Queen_2);
        result.put("q3", jeops_examples_queens_Queen_3);
        result.put("q4", jeops_examples_queens_Queen_4);
        result.put("q5", jeops_examples_queens_Queen_5);
        result.put("q6", jeops_examples_queens_Queen_6);
        result.put("q7", jeops_examples_queens_Queen_7);
        result.put("q8", jeops_examples_queens_Queen_8);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule findSolution.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_findSolution(int index) {
        switch (index) {
            case 0: return "jeops.examples.queens.Queen";
            case 1: return "jeops.examples.queens.Queen";
            case 2: return "jeops.examples.queens.Queen";
            case 3: return "jeops.examples.queens.Queen";
            case 4: return "jeops.examples.queens.Queen";
            case 5: return "jeops.examples.queens.Queen";
            case 6: return "jeops.examples.queens.Queen";
            case 7: return "jeops.examples.queens.Queen";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule findSolution.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_findSolution(int index) {
        switch (index) {
            case 0: return jeops.examples.queens.Queen.class;
            case 1: return jeops.examples.queens.Queen.class;
            case 2: return jeops.examples.queens.Queen.class;
            case 3: return jeops.examples.queens.Queen.class;
            case 4: return jeops.examples.queens.Queen.class;
            case 5: return jeops.examples.queens.Queen.class;
            case 6: return jeops.examples.queens.Queen.class;
            case 7: return jeops.examples.queens.Queen.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule findSolution.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_findSolution(int index, Object value) {
        switch (index) {
            case 0: this.jeops_examples_queens_Queen_1 = (jeops.examples.queens.Queen) value; break;
            case 1: this.jeops_examples_queens_Queen_2 = (jeops.examples.queens.Queen) value; break;
            case 2: this.jeops_examples_queens_Queen_3 = (jeops.examples.queens.Queen) value; break;
            case 3: this.jeops_examples_queens_Queen_4 = (jeops.examples.queens.Queen) value; break;
            case 4: this.jeops_examples_queens_Queen_5 = (jeops.examples.queens.Queen) value; break;
            case 5: this.jeops_examples_queens_Queen_6 = (jeops.examples.queens.Queen) value; break;
            case 6: this.jeops_examples_queens_Queen_7 = (jeops.examples.queens.Queen) value; break;
            case 7: this.jeops_examples_queens_Queen_8 = (jeops.examples.queens.Queen) value; break;
        }
    }

    /**
     * Returns an object declared in the rule findSolution.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_findSolution(int index) {
        switch (index) {
            case 0: return jeops_examples_queens_Queen_1;
            case 1: return jeops_examples_queens_Queen_2;
            case 2: return jeops_examples_queens_Queen_3;
            case 3: return jeops_examples_queens_Queen_4;
            case 4: return jeops_examples_queens_Queen_5;
            case 5: return jeops_examples_queens_Queen_6;
            case 6: return jeops_examples_queens_Queen_7;
            case 7: return jeops_examples_queens_Queen_8;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule findSolution
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_findSolution() {
        return new Object[] {
                            jeops_examples_queens_Queen_1,
                            jeops_examples_queens_Queen_2,
                            jeops_examples_queens_Queen_3,
                            jeops_examples_queens_Queen_4,
                            jeops_examples_queens_Queen_5,
                            jeops_examples_queens_Queen_6,
                            jeops_examples_queens_Queen_7,
                            jeops_examples_queens_Queen_8
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule findSolution
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_findSolution(Object[] objects) {
        jeops_examples_queens_Queen_1 = (jeops.examples.queens.Queen) objects[0];
        jeops_examples_queens_Queen_2 = (jeops.examples.queens.Queen) objects[1];
        jeops_examples_queens_Queen_3 = (jeops.examples.queens.Queen) objects[2];
        jeops_examples_queens_Queen_4 = (jeops.examples.queens.Queen) objects[3];
        jeops_examples_queens_Queen_5 = (jeops.examples.queens.Queen) objects[4];
        jeops_examples_queens_Queen_6 = (jeops.examples.queens.Queen) objects[5];
        jeops_examples_queens_Queen_7 = (jeops.examples.queens.Queen) objects[6];
        jeops_examples_queens_Queen_8 = (jeops.examples.queens.Queen) objects[7];
    }

    /**
     * Precondition 0 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>q1.getRow() == 1</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_0() {
        return (jeops_examples_queens_Queen_1.getRow() == 1);
    }

    /**
     * Precondition 1 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>q2.getRow() == 2</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_1() {
        return (jeops_examples_queens_Queen_2.getRow() == 2);
    }

    /**
     * Precondition 2 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>q3.getRow() == 3</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_2() {
        return (jeops_examples_queens_Queen_3.getRow() == 3);
    }

    /**
     * Precondition 3 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>q4.getRow() == 4</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_3() {
        return (jeops_examples_queens_Queen_4.getRow() == 4);
    }

    /**
     * Precondition 4 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>q5.getRow() == 5</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_4() {
        return (jeops_examples_queens_Queen_5.getRow() == 5);
    }

    /**
     * Precondition 5 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>q6.getRow() == 6</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_5() {
        return (jeops_examples_queens_Queen_6.getRow() == 6);
    }

    /**
     * Precondition 6 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>q7.getRow() == 7</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_6() {
        return (jeops_examples_queens_Queen_7.getRow() == 7);
    }

    /**
     * Precondition 7 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>q8.getRow() == 8</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_7() {
        return (jeops_examples_queens_Queen_8.getRow() == 8);
    }

    /**
     * Precondition 8 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q1.attacks(q2)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_8() {
        return (!jeops_examples_queens_Queen_1.attacks(jeops_examples_queens_Queen_2));
    }

    /**
     * Precondition 9 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q1.attacks(q3)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_9() {
        return (!jeops_examples_queens_Queen_1.attacks(jeops_examples_queens_Queen_3));
    }

    /**
     * Precondition 10 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q1.attacks(q4)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_10() {
        return (!jeops_examples_queens_Queen_1.attacks(jeops_examples_queens_Queen_4));
    }

    /**
     * Precondition 11 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q1.attacks(q5)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_11() {
        return (!jeops_examples_queens_Queen_1.attacks(jeops_examples_queens_Queen_5));
    }

    /**
     * Precondition 12 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q1.attacks(q6)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_12() {
        return (!jeops_examples_queens_Queen_1.attacks(jeops_examples_queens_Queen_6));
    }

    /**
     * Precondition 13 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q1.attacks(q7)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_13() {
        return (!jeops_examples_queens_Queen_1.attacks(jeops_examples_queens_Queen_7));
    }

    /**
     * Precondition 14 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q1.attacks(q8)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_14() {
        return (!jeops_examples_queens_Queen_1.attacks(jeops_examples_queens_Queen_8));
    }

    /**
     * Precondition 15 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q2.attacks(q3)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_15() {
        return (!jeops_examples_queens_Queen_2.attacks(jeops_examples_queens_Queen_3));
    }

    /**
     * Precondition 16 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q2.attacks(q4)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_16() {
        return (!jeops_examples_queens_Queen_2.attacks(jeops_examples_queens_Queen_4));
    }

    /**
     * Precondition 17 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q2.attacks(q5)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_17() {
        return (!jeops_examples_queens_Queen_2.attacks(jeops_examples_queens_Queen_5));
    }

    /**
     * Precondition 18 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q2.attacks(q6)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_18() {
        return (!jeops_examples_queens_Queen_2.attacks(jeops_examples_queens_Queen_6));
    }

    /**
     * Precondition 19 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q2.attacks(q7)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_19() {
        return (!jeops_examples_queens_Queen_2.attacks(jeops_examples_queens_Queen_7));
    }

    /**
     * Precondition 20 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q2.attacks(q8)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_20() {
        return (!jeops_examples_queens_Queen_2.attacks(jeops_examples_queens_Queen_8));
    }

    /**
     * Precondition 21 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q3.attacks(q4)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_21() {
        return (!jeops_examples_queens_Queen_3.attacks(jeops_examples_queens_Queen_4));
    }

    /**
     * Precondition 22 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q3.attacks(q5)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_22() {
        return (!jeops_examples_queens_Queen_3.attacks(jeops_examples_queens_Queen_5));
    }

    /**
     * Precondition 23 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q3.attacks(q6)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_23() {
        return (!jeops_examples_queens_Queen_3.attacks(jeops_examples_queens_Queen_6));
    }

    /**
     * Precondition 24 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q3.attacks(q7)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_24() {
        return (!jeops_examples_queens_Queen_3.attacks(jeops_examples_queens_Queen_7));
    }

    /**
     * Precondition 25 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q3.attacks(q8)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_25() {
        return (!jeops_examples_queens_Queen_3.attacks(jeops_examples_queens_Queen_8));
    }

    /**
     * Precondition 26 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q4.attacks(q5)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_26() {
        return (!jeops_examples_queens_Queen_4.attacks(jeops_examples_queens_Queen_5));
    }

    /**
     * Precondition 27 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q4.attacks(q6)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_27() {
        return (!jeops_examples_queens_Queen_4.attacks(jeops_examples_queens_Queen_6));
    }

    /**
     * Precondition 28 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q4.attacks(q7)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_28() {
        return (!jeops_examples_queens_Queen_4.attacks(jeops_examples_queens_Queen_7));
    }

    /**
     * Precondition 29 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q4.attacks(q8)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_29() {
        return (!jeops_examples_queens_Queen_4.attacks(jeops_examples_queens_Queen_8));
    }

    /**
     * Precondition 30 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q5.attacks(q6)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_30() {
        return (!jeops_examples_queens_Queen_5.attacks(jeops_examples_queens_Queen_6));
    }

    /**
     * Precondition 31 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q5.attacks(q7)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_31() {
        return (!jeops_examples_queens_Queen_5.attacks(jeops_examples_queens_Queen_7));
    }

    /**
     * Precondition 32 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q5.attacks(q8)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_32() {
        return (!jeops_examples_queens_Queen_5.attacks(jeops_examples_queens_Queen_8));
    }

    /**
     * Precondition 33 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q6.attacks(q7)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_33() {
        return (!jeops_examples_queens_Queen_6.attacks(jeops_examples_queens_Queen_7));
    }

    /**
     * Precondition 34 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q6.attacks(q8)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_34() {
        return (!jeops_examples_queens_Queen_6.attacks(jeops_examples_queens_Queen_8));
    }

    /**
     * Precondition 35 of rule findSolution.<p>
     * The original expression was:<br>
     * <code>!q7.attacks(q8)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec_35() {
        return (!jeops_examples_queens_Queen_7.attacks(jeops_examples_queens_Queen_8));
    }

    /**
     * Checks whether some preconditions of rule findSolution is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean findSolution_prec(int index) {
        switch (index) {
            case 0: return findSolution_prec_0();
            case 1: return findSolution_prec_1();
            case 2: return findSolution_prec_2();
            case 3: return findSolution_prec_3();
            case 4: return findSolution_prec_4();
            case 5: return findSolution_prec_5();
            case 6: return findSolution_prec_6();
            case 7: return findSolution_prec_7();
            case 8: return findSolution_prec_8();
            case 9: return findSolution_prec_9();
            case 10: return findSolution_prec_10();
            case 11: return findSolution_prec_11();
            case 12: return findSolution_prec_12();
            case 13: return findSolution_prec_13();
            case 14: return findSolution_prec_14();
            case 15: return findSolution_prec_15();
            case 16: return findSolution_prec_16();
            case 17: return findSolution_prec_17();
            case 18: return findSolution_prec_18();
            case 19: return findSolution_prec_19();
            case 20: return findSolution_prec_20();
            case 21: return findSolution_prec_21();
            case 22: return findSolution_prec_22();
            case 23: return findSolution_prec_23();
            case 24: return findSolution_prec_24();
            case 25: return findSolution_prec_25();
            case 26: return findSolution_prec_26();
            case 27: return findSolution_prec_27();
            case 28: return findSolution_prec_28();
            case 29: return findSolution_prec_29();
            case 30: return findSolution_prec_30();
            case 31: return findSolution_prec_31();
            case 32: return findSolution_prec_32();
            case 33: return findSolution_prec_33();
            case 34: return findSolution_prec_34();
            case 35: return findSolution_prec_35();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_findSolution(int declIndex) {
        setLocalDeclarations_findSolution(declIndex);
        switch (declIndex) {
            case 0:
                if (!findSolution_prec_0()) return false;
                return true;
            case 1:
                if (!findSolution_prec_1()) return false;
                if (!findSolution_prec_8()) return false;
                return true;
            case 2:
                if (!findSolution_prec_2()) return false;
                if (!findSolution_prec_9()) return false;
                if (!findSolution_prec_15()) return false;
                return true;
            case 3:
                if (!findSolution_prec_3()) return false;
                if (!findSolution_prec_10()) return false;
                if (!findSolution_prec_16()) return false;
                if (!findSolution_prec_21()) return false;
                return true;
            case 4:
                if (!findSolution_prec_4()) return false;
                if (!findSolution_prec_11()) return false;
                if (!findSolution_prec_17()) return false;
                if (!findSolution_prec_22()) return false;
                if (!findSolution_prec_26()) return false;
                return true;
            case 5:
                if (!findSolution_prec_5()) return false;
                if (!findSolution_prec_12()) return false;
                if (!findSolution_prec_18()) return false;
                if (!findSolution_prec_23()) return false;
                if (!findSolution_prec_27()) return false;
                if (!findSolution_prec_30()) return false;
                return true;
            case 6:
                if (!findSolution_prec_6()) return false;
                if (!findSolution_prec_13()) return false;
                if (!findSolution_prec_19()) return false;
                if (!findSolution_prec_24()) return false;
                if (!findSolution_prec_28()) return false;
                if (!findSolution_prec_31()) return false;
                if (!findSolution_prec_33()) return false;
                return true;
            case 7:
                if (!findSolution_prec_7()) return false;
                if (!findSolution_prec_14()) return false;
                if (!findSolution_prec_20()) return false;
                if (!findSolution_prec_25()) return false;
                if (!findSolution_prec_29()) return false;
                if (!findSolution_prec_32()) return false;
                if (!findSolution_prec_34()) return false;
                if (!findSolution_prec_35()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule findSolution
     */
    public void findSolution() {
			System.out.println("Solution " + solutionNumber + " to the problem:");
			boolean[][] aux = new boolean[9][9]; // To begin couting by 1.
			aux[jeops_examples_queens_Queen_1.getRow()][jeops_examples_queens_Queen_1.getColumn()] = true;
			aux[jeops_examples_queens_Queen_2.getRow()][jeops_examples_queens_Queen_2.getColumn()] = true;
			aux[jeops_examples_queens_Queen_3.getRow()][jeops_examples_queens_Queen_3.getColumn()] = true;
			aux[jeops_examples_queens_Queen_4.getRow()][jeops_examples_queens_Queen_4.getColumn()] = true;
			aux[jeops_examples_queens_Queen_5.getRow()][jeops_examples_queens_Queen_5.getColumn()] = true;
			aux[jeops_examples_queens_Queen_6.getRow()][jeops_examples_queens_Queen_6.getColumn()] = true;
			aux[jeops_examples_queens_Queen_7.getRow()][jeops_examples_queens_Queen_7.getColumn()] = true;
			aux[jeops_examples_queens_Queen_8.getRow()][jeops_examples_queens_Queen_8.getColumn()] = true;
			System.out.println("/-------------------------------\\");
			for (int i = 1; i <= 8; i++) {
				for (int j = 1; j <= 8; j++) {
					if (aux[i][j]) {
						System.out.print("| o ");
					} else {
						System.out.print("|   ");
					}
				}
				System.out.println("|");
				if (i < 8) {
					System.out.println("|---|---|---|---|---|---|---|---|");
				} else {
					System.out.println("\\-------------------------------/");
				}
			}
			solutionNumber++;
	    }




    /**
     * Returns the name of the rules in this class file.
     *
     * @return the name of the rules in this class file.
     */
    public String[] getRuleNames() {
        int numberOfRules = 1;
    	String[] result = new String[numberOfRules];
        result[0] = "findSolution";
        return result;
    }

    /**
     * Returns the number of declarations of the rules in this class file.
     *
     * @return the number of declarations  of the rules in this class file.
     */
    public int[] getNumberOfDeclarations() {
        int numberOfRules = 1;
    	int[] result = new int[numberOfRules];
        result[0] = 8;
        return result;
    }

    /**
     * Returns the number of local declarations of the rules in this class file.
     *
     * @return the number of local declarations  of the rules in this class file.
     */
    public int[] getNumberOfLocalDeclarations() {
        int numberOfRules = 0;
    	int[] result = new int[numberOfRules];
        return result;
    }

    /**
     * Returns the number of preconditions of the rules in this class file.
     *
     * @return the number of preconditions  of the rules in this class file.
     */
    public int[] getNumberOfPreconditions() {
        int numberOfRules = 1;
    	int[] result = new int[numberOfRules];
        result[0] = 36;
        return result;
    }

    /**
     * Checks whether some preconditions of some rule is satisfied.
     *
     * @param ruleIndex the index of the rule to be checked
     * @param precIndex the index of the precondition to be checked
     * @return <code>true</code> if the corresponding precondition for the
     *          given rule is satisfied. <code>false</code> otherwise.
     */
    public boolean checkPrecondition(int ruleIndex, int precIndex) {
        switch (ruleIndex) {
            case 0: return findSolution_prec(precIndex);
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference only the elements declared up to the given index
     * are true.
     *
     * @param ruleIndex the index of the rule to be checked
     * @param declIndex the index of the declaration to be checked
     * @return <code>true</code> if all the preconditions of a rule which
     *          reference only the elements declared up to the given index
     *          are satisfied; <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return checkPrecForDeclaration_findSolution(declIndex);
            default: return false;
        }
    }

    /**
     * Returns the class name of an object declared in a rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration
     * @return the class name of the declared object.
     */
    public String getDeclaredClassName(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return getDeclaredClassName_findSolution(declIndex);
            default: return null;
        }
    }

    /**
     * Returns the class of an object declared in a rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration
     * @return the class of the declared object.
     */
    public Class getDeclaredClass(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return getDeclaredClass_findSolution(declIndex);
            default: return null;
        }
    }

    /**
     * Fires one of the rules in this rule base.
     *
     * @param ruleIndex the index of the rule to be fired
     */
    protected void internalFireRule(int ruleIndex) {
        switch (ruleIndex) {
            case 0: findSolution(); break;
        }
    }

    /**
     * Returns the number of rules.
     *
     * @return the number of rules.
     */
    public int getNumberOfRules() {
        return 1;
    }

    /**
     * Returns a table that maps each declared identifier of a rule
     * into its corresponding value.
     *
     * @param ruleIndex the index of the rule
     * @return a table that maps each declared identifier of a rule
     *          into its corresponding value.
     */
    public java.util.Hashtable getInternalRuleMapping(int ruleIndex) {
        switch (ruleIndex) {
            case 0: return getMapRulefindSolution();
            default: return new java.util.Hashtable();
        }
    }

    /**
     * Sets an object that represents a declaration of some rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration in the rule.
     * @param value the value of the object being set.
     */
    public void setObject(int ruleIndex, int declIndex, Object value) {
        switch (ruleIndex) {
            case 0: setObject_findSolution(declIndex, value); break;
        }
    }

    /**
     * Returns an object that represents a declaration of some rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration in the rule.
     * @return the value of the corresponding object.
     */
    public Object getObject(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return getObject_findSolution(declIndex);
            default: return null;
        }
    }

    /**
     * Returns a dependency table between the local declarations and the
     * regular ones.
     *
     * @param ruleIndex the index of the rule
     * @return a two-dimensional array with the dependencies between the
     *          local declarations and the regular ones.
     */
    public Object[][] getLocalDeclarationDependency(int ruleIndex) {
        switch (ruleIndex) {
            case 0: return getLocalDeclarationDependency_findSolution();
            default: return null;
        }
    }
    /**
     * Defines all variables bound to the local declarations of
     * some rule.
     *
     * @param ruleIndex the index of the rule
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations.
     *          of some rule.
     */
    protected void setLocalObjects(int ruleIndex, Object[][] objects) {
        switch (ruleIndex) {
            case 0: setLocalObjects_findSolution(objects); break;
        }
    }
    /**
     * Returns all variables bound to the declarations of
     * some rule.
     *
     * @param ruleIndex the index of the rule
     * @return an object array of the variables bound to the
     *          declarations of some rule.
     */
    protected Object[] getObjects(int ruleIndex) {
        switch (ruleIndex) {
            case 0: return getObjects_findSolution();
            default: return null;
        }
    }
    /**
     * Defines all variables bound to the declarations of
     * some rule.
     *
     * @param ruleIndex the index of the rule
     * @param objects an object array of the variables bound to the
     *          declarations of some rule.
     */
    protected void setObjects(int ruleIndex, Object[] objects) {
        switch (ruleIndex) {
            case 0: setObjects_findSolution(objects); break;
        }
    }

    /**
     * Returns a mapping from the classes of the objects declared in the
     * rules to the number of occurrences of each one.
     *
     * @return a mapping from the classes of the objects declared in the
     *          rules to the number of occurrences of each one.
     */
    public java.util.Hashtable getClassDeclarationCount() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("jeops.examples.queens.Queen", new Integer(8));
        return result;
    }

    /*
     * The variables declared in the preconditions.
     */
    jeops.examples.queens.Queen jeops_examples_queens_Queen_1;
    jeops.examples.queens.Queen jeops_examples_queens_Queen_2;
    jeops.examples.queens.Queen jeops_examples_queens_Queen_3;
    jeops.examples.queens.Queen jeops_examples_queens_Queen_4;
    jeops.examples.queens.Queen jeops_examples_queens_Queen_5;
    jeops.examples.queens.Queen jeops_examples_queens_Queen_6;
    jeops.examples.queens.Queen jeops_examples_queens_Queen_7;
    jeops.examples.queens.Queen jeops_examples_queens_Queen_8;

    /**
     * Constant used to aviod the creation of several empty arrays.
     */
    private static final Object[] EMPTY_OBJECT_ARRAY = new Object[0];

    /**
     * Constant used to aviod the creation of several empty double arrays.
     */
    private static final Object[][] EMPTY_OBJECT_DOUBLE_ARRAY = new Object[0][0];

    /**
     * Constant used to aviod the creation of several empty vectors.
     */
    private static final java.util.Vector EMPTY_VECTOR = new java.util.Vector();

    /**
     * Class constructor.
     *
     * @param objectBase an object base over with this rule base will work.
     * @param knowledgeBase the knowledge base that contains this rule base.
     */
    public Jeops_RuleBase_EightQueens(jeops.ObjectBase objectBase,
                                jeops.AbstractKnowledgeBase knowledgeBase) {
        super(objectBase, knowledgeBase);
    }

}

/**
 * Knowledge base created by JEOPS from file ./src/jeops/examples/queens/EightQueens.rules
 *
 * @version Apr 23, 2016
 */
public class EightQueens extends jeops.AbstractKnowledgeBase {

    /**
     * Creates a new knowledge base with the specified conflict set with the
     * desired conflict resolution policy.
     *
     * @param conflictSet a conflict set with the desired resolution policy
     */
    public EightQueens(jeops.conflict.ConflictSet conflictSet) {
        super(conflictSet);
        jeops.AbstractRuleBase ruleBase;
        jeops.ObjectBase objectBase = getObjectBase();
        ruleBase = new Jeops_RuleBase_EightQueens(objectBase, this);
        setRuleBase(ruleBase);
    }

    /**
     * Creates a new knowledge base, using the default conflict resolution
     * policy.
     */
    public EightQueens() {
        this(new jeops.conflict.DefaultConflictSet());
    }

}
