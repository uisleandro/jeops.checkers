package jeops.examples.hanoi;

/**
 * Rule base used to solve the towers of hanoi problem.
 */
  class Jeops_RuleBase_BaseHanoi extends jeops.AbstractRuleBase {

	
    /**
     * Sets the local declaration for a given declaration index.
     * As there are no local declarations for this rule, this
     * method does nothing.
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_createsFirstSubProblem(int index) {
    }

    /**
     * Returns a dependency table between the local declarations 
     * and the regular ones for rule createsFirstSubProblem. As here are
     * no local declarations for this rule, this method returns an
     * empty array.
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_createsFirstSubProblem() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule createsFirstSubProblem
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_createsFirstSubProblem(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule createsFirstSubProblem
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule createsFirstSubProblem
     */
    private java.util.Hashtable getMapRulecreatesFirstSubProblem() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("p", jeops_examples_hanoi_Hanoi_1);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule createsFirstSubProblem.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_createsFirstSubProblem(int index) {
        switch (index) {
            case 0: return "jeops.examples.hanoi.Hanoi";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule createsFirstSubProblem.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_createsFirstSubProblem(int index) {
        switch (index) {
            case 0: return jeops.examples.hanoi.Hanoi.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule createsFirstSubProblem.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_createsFirstSubProblem(int index, Object value) {
        switch (index) {
            case 0: this.jeops_examples_hanoi_Hanoi_1 = (jeops.examples.hanoi.Hanoi) value; break;
        }
    }

    /**
     * Returns an object declared in the rule createsFirstSubProblem.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_createsFirstSubProblem(int index) {
        switch (index) {
            case 0: return jeops_examples_hanoi_Hanoi_1;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule createsFirstSubProblem
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_createsFirstSubProblem() {
        return new Object[] {
                            jeops_examples_hanoi_Hanoi_1
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule createsFirstSubProblem
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_createsFirstSubProblem(Object[] objects) {
        jeops_examples_hanoi_Hanoi_1 = (jeops.examples.hanoi.Hanoi) objects[0];
    }

    /**
     * Precondition 0 of rule createsFirstSubProblem.<p>
     * The original expression was:<br>
     * <code>p.getDiscs() > 1</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean createsFirstSubProblem_prec_0() {
        return (jeops_examples_hanoi_Hanoi_1.getDiscs() > 1);
    }

    /**
     * Precondition 1 of rule createsFirstSubProblem.<p>
     * The original expression was:<br>
     * <code>!p.isOk()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean createsFirstSubProblem_prec_1() {
        return (!jeops_examples_hanoi_Hanoi_1.isOk());
    }

    /**
     * Precondition 2 of rule createsFirstSubProblem.<p>
     * The original expression was:<br>
     * <code>p.getSub1() == null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean createsFirstSubProblem_prec_2() {
        return (jeops_examples_hanoi_Hanoi_1.getSub1() == null);
    }

    /**
     * Checks whether some preconditions of rule createsFirstSubProblem is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean createsFirstSubProblem_prec(int index) {
        switch (index) {
            case 0: return createsFirstSubProblem_prec_0();
            case 1: return createsFirstSubProblem_prec_1();
            case 2: return createsFirstSubProblem_prec_2();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_createsFirstSubProblem(int declIndex) {
        setLocalDeclarations_createsFirstSubProblem(declIndex);
        switch (declIndex) {
            case 0:
                if (!createsFirstSubProblem_prec_0()) return false;
                if (!createsFirstSubProblem_prec_1()) return false;
                if (!createsFirstSubProblem_prec_2()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule createsFirstSubProblem
     */
    public void createsFirstSubProblem() {
	    Hanoi ps1;
	    ps1 = new Hanoi(jeops_examples_hanoi_Hanoi_1.getDiscs() - 1, jeops_examples_hanoi_Hanoi_1.getSource(), jeops_examples_hanoi_Hanoi_1.getIntermediate());
	    jeops_examples_hanoi_Hanoi_1.setSub1(ps1);
	    tell(ps1);
	    modified(jeops_examples_hanoi_Hanoi_1);
	    }


	
	
    /**
     * Sets the local declaration for a given declaration index.
     * As there are no local declarations for this rule, this
     * method does nothing.
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_createsSecondSubproblem(int index) {
    }

    /**
     * Returns a dependency table between the local declarations 
     * and the regular ones for rule createsSecondSubproblem. As here are
     * no local declarations for this rule, this method returns an
     * empty array.
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_createsSecondSubproblem() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule createsSecondSubproblem
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_createsSecondSubproblem(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule createsSecondSubproblem
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule createsSecondSubproblem
     */
    private java.util.Hashtable getMapRulecreatesSecondSubproblem() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("p", jeops_examples_hanoi_Hanoi_1);
        result.put("ps1", jeops_examples_hanoi_Hanoi_2);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule createsSecondSubproblem.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_createsSecondSubproblem(int index) {
        switch (index) {
            case 0: return "jeops.examples.hanoi.Hanoi";
            case 1: return "jeops.examples.hanoi.Hanoi";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule createsSecondSubproblem.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_createsSecondSubproblem(int index) {
        switch (index) {
            case 0: return jeops.examples.hanoi.Hanoi.class;
            case 1: return jeops.examples.hanoi.Hanoi.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule createsSecondSubproblem.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_createsSecondSubproblem(int index, Object value) {
        switch (index) {
            case 0: this.jeops_examples_hanoi_Hanoi_1 = (jeops.examples.hanoi.Hanoi) value; break;
            case 1: this.jeops_examples_hanoi_Hanoi_2 = (jeops.examples.hanoi.Hanoi) value; break;
        }
    }

    /**
     * Returns an object declared in the rule createsSecondSubproblem.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_createsSecondSubproblem(int index) {
        switch (index) {
            case 0: return jeops_examples_hanoi_Hanoi_1;
            case 1: return jeops_examples_hanoi_Hanoi_2;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule createsSecondSubproblem
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_createsSecondSubproblem() {
        return new Object[] {
                            jeops_examples_hanoi_Hanoi_1,
                            jeops_examples_hanoi_Hanoi_2
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule createsSecondSubproblem
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_createsSecondSubproblem(Object[] objects) {
        jeops_examples_hanoi_Hanoi_1 = (jeops.examples.hanoi.Hanoi) objects[0];
        jeops_examples_hanoi_Hanoi_2 = (jeops.examples.hanoi.Hanoi) objects[1];
    }

    /**
     * Precondition 0 of rule createsSecondSubproblem.<p>
     * The original expression was:<br>
     * <code>p.getDiscs() > 1</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean createsSecondSubproblem_prec_0() {
        return (jeops_examples_hanoi_Hanoi_1.getDiscs() > 1);
    }

    /**
     * Precondition 1 of rule createsSecondSubproblem.<p>
     * The original expression was:<br>
     * <code>!p.isOk()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean createsSecondSubproblem_prec_1() {
        return (!jeops_examples_hanoi_Hanoi_1.isOk());
    }

    /**
     * Precondition 2 of rule createsSecondSubproblem.<p>
     * The original expression was:<br>
     * <code>ps1 == p.getSub1()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean createsSecondSubproblem_prec_2() {
        return (jeops_examples_hanoi_Hanoi_2 == jeops_examples_hanoi_Hanoi_1.getSub1());
    }

    /**
     * Precondition 3 of rule createsSecondSubproblem.<p>
     * The original expression was:<br>
     * <code>ps1 != null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean createsSecondSubproblem_prec_3() {
        return (jeops_examples_hanoi_Hanoi_2 != null);
    }

    /**
     * Precondition 4 of rule createsSecondSubproblem.<p>
     * The original expression was:<br>
     * <code>ps1.isOk()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean createsSecondSubproblem_prec_4() {
        return (jeops_examples_hanoi_Hanoi_2.isOk());
    }

    /**
     * Precondition 5 of rule createsSecondSubproblem.<p>
     * The original expression was:<br>
     * <code>p.getSub2() == null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean createsSecondSubproblem_prec_5() {
        return (jeops_examples_hanoi_Hanoi_1.getSub2() == null);
    }

    /**
     * Checks whether some preconditions of rule createsSecondSubproblem is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean createsSecondSubproblem_prec(int index) {
        switch (index) {
            case 0: return createsSecondSubproblem_prec_0();
            case 1: return createsSecondSubproblem_prec_1();
            case 2: return createsSecondSubproblem_prec_2();
            case 3: return createsSecondSubproblem_prec_3();
            case 4: return createsSecondSubproblem_prec_4();
            case 5: return createsSecondSubproblem_prec_5();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_createsSecondSubproblem(int declIndex) {
        setLocalDeclarations_createsSecondSubproblem(declIndex);
        switch (declIndex) {
            case 0:
                if (!createsSecondSubproblem_prec_0()) return false;
                if (!createsSecondSubproblem_prec_1()) return false;
                if (!createsSecondSubproblem_prec_5()) return false;
                return true;
            case 1:
                if (!createsSecondSubproblem_prec_2()) return false;
                if (!createsSecondSubproblem_prec_3()) return false;
                if (!createsSecondSubproblem_prec_4()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule createsSecondSubproblem
     */
    public void createsSecondSubproblem() {
	    jeops_examples_hanoi_Hanoi_1.addMove(jeops_examples_hanoi_Hanoi_1.getSource(), jeops_examples_hanoi_Hanoi_1.getDestination());
	    Hanoi ps2;
	    ps2 = new Hanoi(jeops_examples_hanoi_Hanoi_1.getDiscs() - 1, jeops_examples_hanoi_Hanoi_1.getIntermediate(), jeops_examples_hanoi_Hanoi_1.getDestination());
	    jeops_examples_hanoi_Hanoi_1.setSub2(ps2);
	    tell (ps2);
	    modified(jeops_examples_hanoi_Hanoi_1);
	    }


	
	
    /**
     * Sets the local declaration for a given declaration index.
     * As there are no local declarations for this rule, this
     * method does nothing.
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_bothSubproblemsOk(int index) {
    }

    /**
     * Returns a dependency table between the local declarations 
     * and the regular ones for rule bothSubproblemsOk. As here are
     * no local declarations for this rule, this method returns an
     * empty array.
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_bothSubproblemsOk() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule bothSubproblemsOk
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_bothSubproblemsOk(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule bothSubproblemsOk
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule bothSubproblemsOk
     */
    private java.util.Hashtable getMapRulebothSubproblemsOk() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("p", jeops_examples_hanoi_Hanoi_1);
        result.put("ps1", jeops_examples_hanoi_Hanoi_2);
        result.put("ps2", jeops_examples_hanoi_Hanoi_3);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule bothSubproblemsOk.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_bothSubproblemsOk(int index) {
        switch (index) {
            case 0: return "jeops.examples.hanoi.Hanoi";
            case 1: return "jeops.examples.hanoi.Hanoi";
            case 2: return "jeops.examples.hanoi.Hanoi";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule bothSubproblemsOk.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_bothSubproblemsOk(int index) {
        switch (index) {
            case 0: return jeops.examples.hanoi.Hanoi.class;
            case 1: return jeops.examples.hanoi.Hanoi.class;
            case 2: return jeops.examples.hanoi.Hanoi.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule bothSubproblemsOk.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_bothSubproblemsOk(int index, Object value) {
        switch (index) {
            case 0: this.jeops_examples_hanoi_Hanoi_1 = (jeops.examples.hanoi.Hanoi) value; break;
            case 1: this.jeops_examples_hanoi_Hanoi_2 = (jeops.examples.hanoi.Hanoi) value; break;
            case 2: this.jeops_examples_hanoi_Hanoi_3 = (jeops.examples.hanoi.Hanoi) value; break;
        }
    }

    /**
     * Returns an object declared in the rule bothSubproblemsOk.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_bothSubproblemsOk(int index) {
        switch (index) {
            case 0: return jeops_examples_hanoi_Hanoi_1;
            case 1: return jeops_examples_hanoi_Hanoi_2;
            case 2: return jeops_examples_hanoi_Hanoi_3;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule bothSubproblemsOk
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_bothSubproblemsOk() {
        return new Object[] {
                            jeops_examples_hanoi_Hanoi_1,
                            jeops_examples_hanoi_Hanoi_2,
                            jeops_examples_hanoi_Hanoi_3
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule bothSubproblemsOk
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_bothSubproblemsOk(Object[] objects) {
        jeops_examples_hanoi_Hanoi_1 = (jeops.examples.hanoi.Hanoi) objects[0];
        jeops_examples_hanoi_Hanoi_2 = (jeops.examples.hanoi.Hanoi) objects[1];
        jeops_examples_hanoi_Hanoi_3 = (jeops.examples.hanoi.Hanoi) objects[2];
    }

    /**
     * Precondition 0 of rule bothSubproblemsOk.<p>
     * The original expression was:<br>
     * <code>ps1 != null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean bothSubproblemsOk_prec_0() {
        return (jeops_examples_hanoi_Hanoi_2 != null);
    }

    /**
     * Precondition 1 of rule bothSubproblemsOk.<p>
     * The original expression was:<br>
     * <code>ps2 != null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean bothSubproblemsOk_prec_1() {
        return (jeops_examples_hanoi_Hanoi_3 != null);
    }

    /**
     * Precondition 2 of rule bothSubproblemsOk.<p>
     * The original expression was:<br>
     * <code>ps1 == p.getSub1()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean bothSubproblemsOk_prec_2() {
        return (jeops_examples_hanoi_Hanoi_2 == jeops_examples_hanoi_Hanoi_1.getSub1());
    }

    /**
     * Precondition 3 of rule bothSubproblemsOk.<p>
     * The original expression was:<br>
     * <code>ps2 == p.getSub2()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean bothSubproblemsOk_prec_3() {
        return (jeops_examples_hanoi_Hanoi_3 == jeops_examples_hanoi_Hanoi_1.getSub2());
    }

    /**
     * Precondition 4 of rule bothSubproblemsOk.<p>
     * The original expression was:<br>
     * <code>!p.isOk()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean bothSubproblemsOk_prec_4() {
        return (!jeops_examples_hanoi_Hanoi_1.isOk());
    }

    /**
     * Precondition 5 of rule bothSubproblemsOk.<p>
     * The original expression was:<br>
     * <code>ps1.isOk()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean bothSubproblemsOk_prec_5() {
        return (jeops_examples_hanoi_Hanoi_2.isOk());
    }

    /**
     * Precondition 6 of rule bothSubproblemsOk.<p>
     * The original expression was:<br>
     * <code>ps2.isOk()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean bothSubproblemsOk_prec_6() {
        return (jeops_examples_hanoi_Hanoi_3.isOk());
    }

    /**
     * Checks whether some preconditions of rule bothSubproblemsOk is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean bothSubproblemsOk_prec(int index) {
        switch (index) {
            case 0: return bothSubproblemsOk_prec_0();
            case 1: return bothSubproblemsOk_prec_1();
            case 2: return bothSubproblemsOk_prec_2();
            case 3: return bothSubproblemsOk_prec_3();
            case 4: return bothSubproblemsOk_prec_4();
            case 5: return bothSubproblemsOk_prec_5();
            case 6: return bothSubproblemsOk_prec_6();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_bothSubproblemsOk(int declIndex) {
        setLocalDeclarations_bothSubproblemsOk(declIndex);
        switch (declIndex) {
            case 0:
                if (!bothSubproblemsOk_prec_4()) return false;
                return true;
            case 1:
                if (!bothSubproblemsOk_prec_0()) return false;
                if (!bothSubproblemsOk_prec_2()) return false;
                if (!bothSubproblemsOk_prec_5()) return false;
                return true;
            case 2:
                if (!bothSubproblemsOk_prec_1()) return false;
                if (!bothSubproblemsOk_prec_3()) return false;
                if (!bothSubproblemsOk_prec_6()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule bothSubproblemsOk
     */
    public void bothSubproblemsOk() {
	    jeops_examples_hanoi_Hanoi_1.setOk(true);
	    modified(jeops_examples_hanoi_Hanoi_1);
	    }


	
	
    /**
     * Sets the local declaration for a given declaration index.
     * As there are no local declarations for this rule, this
     * method does nothing.
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_oneDisc(int index) {
    }

    /**
     * Returns a dependency table between the local declarations 
     * and the regular ones for rule oneDisc. As here are
     * no local declarations for this rule, this method returns an
     * empty array.
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_oneDisc() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule oneDisc
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_oneDisc(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule oneDisc
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule oneDisc
     */
    private java.util.Hashtable getMapRuleoneDisc() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("p", jeops_examples_hanoi_Hanoi_1);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule oneDisc.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_oneDisc(int index) {
        switch (index) {
            case 0: return "jeops.examples.hanoi.Hanoi";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule oneDisc.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_oneDisc(int index) {
        switch (index) {
            case 0: return jeops.examples.hanoi.Hanoi.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule oneDisc.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_oneDisc(int index, Object value) {
        switch (index) {
            case 0: this.jeops_examples_hanoi_Hanoi_1 = (jeops.examples.hanoi.Hanoi) value; break;
        }
    }

    /**
     * Returns an object declared in the rule oneDisc.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_oneDisc(int index) {
        switch (index) {
            case 0: return jeops_examples_hanoi_Hanoi_1;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule oneDisc
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_oneDisc() {
        return new Object[] {
                            jeops_examples_hanoi_Hanoi_1
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule oneDisc
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_oneDisc(Object[] objects) {
        jeops_examples_hanoi_Hanoi_1 = (jeops.examples.hanoi.Hanoi) objects[0];
    }

    /**
     * Precondition 0 of rule oneDisc.<p>
     * The original expression was:<br>
     * <code>p.getDiscs() == 1</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean oneDisc_prec_0() {
        return (jeops_examples_hanoi_Hanoi_1.getDiscs() == 1);
    }

    /**
     * Precondition 1 of rule oneDisc.<p>
     * The original expression was:<br>
     * <code>!p.isOk()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean oneDisc_prec_1() {
        return (!jeops_examples_hanoi_Hanoi_1.isOk());
    }

    /**
     * Checks whether some preconditions of rule oneDisc is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean oneDisc_prec(int index) {
        switch (index) {
            case 0: return oneDisc_prec_0();
            case 1: return oneDisc_prec_1();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_oneDisc(int declIndex) {
        setLocalDeclarations_oneDisc(declIndex);
        switch (declIndex) {
            case 0:
                if (!oneDisc_prec_0()) return false;
                if (!oneDisc_prec_1()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule oneDisc
     */
    public void oneDisc() {
	    jeops_examples_hanoi_Hanoi_1.addMove(jeops_examples_hanoi_Hanoi_1.getSource(), jeops_examples_hanoi_Hanoi_1.getDestination());
	    jeops_examples_hanoi_Hanoi_1.setOk(true);
	    modified(jeops_examples_hanoi_Hanoi_1);
	    }




    /**
     * Returns the name of the rules in this class file.
     *
     * @return the name of the rules in this class file.
     */
    public String[] getRuleNames() {
        int numberOfRules = 4;
    	String[] result = new String[numberOfRules];
        result[0] = "createsFirstSubProblem";
        result[1] = "createsSecondSubproblem";
        result[2] = "bothSubproblemsOk";
        result[3] = "oneDisc";
        return result;
    }

    /**
     * Returns the number of declarations of the rules in this class file.
     *
     * @return the number of declarations  of the rules in this class file.
     */
    public int[] getNumberOfDeclarations() {
        int numberOfRules = 4;
    	int[] result = new int[numberOfRules];
        result[0] = 1;
        result[1] = 2;
        result[2] = 3;
        result[3] = 1;
        return result;
    }

    /**
     * Returns the number of local declarations of the rules in this class file.
     *
     * @return the number of local declarations  of the rules in this class file.
     */
    public int[] getNumberOfLocalDeclarations() {
        int numberOfRules = 0;
    	int[] result = new int[numberOfRules];
        return result;
    }

    /**
     * Returns the number of preconditions of the rules in this class file.
     *
     * @return the number of preconditions  of the rules in this class file.
     */
    public int[] getNumberOfPreconditions() {
        int numberOfRules = 4;
    	int[] result = new int[numberOfRules];
        result[0] = 3;
        result[1] = 6;
        result[2] = 7;
        result[3] = 2;
        return result;
    }

    /**
     * Checks whether some preconditions of some rule is satisfied.
     *
     * @param ruleIndex the index of the rule to be checked
     * @param precIndex the index of the precondition to be checked
     * @return <code>true</code> if the corresponding precondition for the
     *          given rule is satisfied. <code>false</code> otherwise.
     */
    public boolean checkPrecondition(int ruleIndex, int precIndex) {
        switch (ruleIndex) {
            case 0: return createsFirstSubProblem_prec(precIndex);
            case 1: return createsSecondSubproblem_prec(precIndex);
            case 2: return bothSubproblemsOk_prec(precIndex);
            case 3: return oneDisc_prec(precIndex);
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference only the elements declared up to the given index
     * are true.
     *
     * @param ruleIndex the index of the rule to be checked
     * @param declIndex the index of the declaration to be checked
     * @return <code>true</code> if all the preconditions of a rule which
     *          reference only the elements declared up to the given index
     *          are satisfied; <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return checkPrecForDeclaration_createsFirstSubProblem(declIndex);
            case 1: return checkPrecForDeclaration_createsSecondSubproblem(declIndex);
            case 2: return checkPrecForDeclaration_bothSubproblemsOk(declIndex);
            case 3: return checkPrecForDeclaration_oneDisc(declIndex);
            default: return false;
        }
    }

    /**
     * Returns the class name of an object declared in a rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration
     * @return the class name of the declared object.
     */
    public String getDeclaredClassName(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return getDeclaredClassName_createsFirstSubProblem(declIndex);
            case 1: return getDeclaredClassName_createsSecondSubproblem(declIndex);
            case 2: return getDeclaredClassName_bothSubproblemsOk(declIndex);
            case 3: return getDeclaredClassName_oneDisc(declIndex);
            default: return null;
        }
    }

    /**
     * Returns the class of an object declared in a rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration
     * @return the class of the declared object.
     */
    public Class getDeclaredClass(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return getDeclaredClass_createsFirstSubProblem(declIndex);
            case 1: return getDeclaredClass_createsSecondSubproblem(declIndex);
            case 2: return getDeclaredClass_bothSubproblemsOk(declIndex);
            case 3: return getDeclaredClass_oneDisc(declIndex);
            default: return null;
        }
    }

    /**
     * Fires one of the rules in this rule base.
     *
     * @param ruleIndex the index of the rule to be fired
     */
    protected void internalFireRule(int ruleIndex) {
        switch (ruleIndex) {
            case 0: createsFirstSubProblem(); break;
            case 1: createsSecondSubproblem(); break;
            case 2: bothSubproblemsOk(); break;
            case 3: oneDisc(); break;
        }
    }

    /**
     * Returns the number of rules.
     *
     * @return the number of rules.
     */
    public int getNumberOfRules() {
        return 4;
    }

    /**
     * Returns a table that maps each declared identifier of a rule
     * into its corresponding value.
     *
     * @param ruleIndex the index of the rule
     * @return a table that maps each declared identifier of a rule
     *          into its corresponding value.
     */
    public java.util.Hashtable getInternalRuleMapping(int ruleIndex) {
        switch (ruleIndex) {
            case 0: return getMapRulecreatesFirstSubProblem();
            case 1: return getMapRulecreatesSecondSubproblem();
            case 2: return getMapRulebothSubproblemsOk();
            case 3: return getMapRuleoneDisc();
            default: return new java.util.Hashtable();
        }
    }

    /**
     * Sets an object that represents a declaration of some rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration in the rule.
     * @param value the value of the object being set.
     */
    public void setObject(int ruleIndex, int declIndex, Object value) {
        switch (ruleIndex) {
            case 0: setObject_createsFirstSubProblem(declIndex, value); break;
            case 1: setObject_createsSecondSubproblem(declIndex, value); break;
            case 2: setObject_bothSubproblemsOk(declIndex, value); break;
            case 3: setObject_oneDisc(declIndex, value); break;
        }
    }

    /**
     * Returns an object that represents a declaration of some rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration in the rule.
     * @return the value of the corresponding object.
     */
    public Object getObject(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return getObject_createsFirstSubProblem(declIndex);
            case 1: return getObject_createsSecondSubproblem(declIndex);
            case 2: return getObject_bothSubproblemsOk(declIndex);
            case 3: return getObject_oneDisc(declIndex);
            default: return null;
        }
    }

    /**
     * Returns a dependency table between the local declarations and the
     * regular ones.
     *
     * @param ruleIndex the index of the rule
     * @return a two-dimensional array with the dependencies between the
     *          local declarations and the regular ones.
     */
    public Object[][] getLocalDeclarationDependency(int ruleIndex) {
        switch (ruleIndex) {
            case 0: return getLocalDeclarationDependency_createsFirstSubProblem();
            case 1: return getLocalDeclarationDependency_createsSecondSubproblem();
            case 2: return getLocalDeclarationDependency_bothSubproblemsOk();
            case 3: return getLocalDeclarationDependency_oneDisc();
            default: return null;
        }
    }
    /**
     * Defines all variables bound to the local declarations of
     * some rule.
     *
     * @param ruleIndex the index of the rule
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations.
     *          of some rule.
     */
    protected void setLocalObjects(int ruleIndex, Object[][] objects) {
        switch (ruleIndex) {
            case 0: setLocalObjects_createsFirstSubProblem(objects); break;
            case 1: setLocalObjects_createsSecondSubproblem(objects); break;
            case 2: setLocalObjects_bothSubproblemsOk(objects); break;
            case 3: setLocalObjects_oneDisc(objects); break;
        }
    }
    /**
     * Returns all variables bound to the declarations of
     * some rule.
     *
     * @param ruleIndex the index of the rule
     * @return an object array of the variables bound to the
     *          declarations of some rule.
     */
    protected Object[] getObjects(int ruleIndex) {
        switch (ruleIndex) {
            case 0: return getObjects_createsFirstSubProblem();
            case 1: return getObjects_createsSecondSubproblem();
            case 2: return getObjects_bothSubproblemsOk();
            case 3: return getObjects_oneDisc();
            default: return null;
        }
    }
    /**
     * Defines all variables bound to the declarations of
     * some rule.
     *
     * @param ruleIndex the index of the rule
     * @param objects an object array of the variables bound to the
     *          declarations of some rule.
     */
    protected void setObjects(int ruleIndex, Object[] objects) {
        switch (ruleIndex) {
            case 0: setObjects_createsFirstSubProblem(objects); break;
            case 1: setObjects_createsSecondSubproblem(objects); break;
            case 2: setObjects_bothSubproblemsOk(objects); break;
            case 3: setObjects_oneDisc(objects); break;
        }
    }

    /**
     * Returns a mapping from the classes of the objects declared in the
     * rules to the number of occurrences of each one.
     *
     * @return a mapping from the classes of the objects declared in the
     *          rules to the number of occurrences of each one.
     */
    public java.util.Hashtable getClassDeclarationCount() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("jeops.examples.hanoi.Hanoi", new Integer(3));
        return result;
    }

    /*
     * The variables declared in the preconditions.
     */
    jeops.examples.hanoi.Hanoi jeops_examples_hanoi_Hanoi_1;
    jeops.examples.hanoi.Hanoi jeops_examples_hanoi_Hanoi_2;
    jeops.examples.hanoi.Hanoi jeops_examples_hanoi_Hanoi_3;

    /**
     * Constant used to aviod the creation of several empty arrays.
     */
    private static final Object[] EMPTY_OBJECT_ARRAY = new Object[0];

    /**
     * Constant used to aviod the creation of several empty double arrays.
     */
    private static final Object[][] EMPTY_OBJECT_DOUBLE_ARRAY = new Object[0][0];

    /**
     * Constant used to aviod the creation of several empty vectors.
     */
    private static final java.util.Vector EMPTY_VECTOR = new java.util.Vector();

    /**
     * Class constructor.
     *
     * @param objectBase an object base over with this rule base will work.
     * @param knowledgeBase the knowledge base that contains this rule base.
     */
    public Jeops_RuleBase_BaseHanoi(jeops.ObjectBase objectBase,
                                jeops.AbstractKnowledgeBase knowledgeBase) {
        super(objectBase, knowledgeBase);
    }

}

/**
 * Knowledge base created by JEOPS from file ./src/jeops/examples/hanoi/BaseHanoi.rules
 *
 * @version Apr 23, 2016
 */
public class BaseHanoi extends jeops.AbstractKnowledgeBase {

    /**
     * Creates a new knowledge base with the specified conflict set with the
     * desired conflict resolution policy.
     *
     * @param conflictSet a conflict set with the desired resolution policy
     */
    public BaseHanoi(jeops.conflict.ConflictSet conflictSet) {
        super(conflictSet);
        jeops.AbstractRuleBase ruleBase;
        jeops.ObjectBase objectBase = getObjectBase();
        ruleBase = new Jeops_RuleBase_BaseHanoi(objectBase, this);
        setRuleBase(ruleBase);
    }

    /**
     * Creates a new knowledge base, using the default conflict resolution
     * policy.
     */
    public BaseHanoi() {
        this(new jeops.conflict.DefaultConflictSet());
    }

}
