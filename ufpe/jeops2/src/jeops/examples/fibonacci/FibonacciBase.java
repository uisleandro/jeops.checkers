package jeops.examples.fibonacci;

  class Jeops_RuleBase_FibonacciBase extends jeops.AbstractRuleBase {

  
    /**
     * Sets the local declaration for a given declaration index.
     * As there are no local declarations for this rule, this
     * method does nothing.
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_GoDown(int index) {
    }

    /**
     * Returns a dependency table between the local declarations 
     * and the regular ones for rule GoDown. As here are
     * no local declarations for this rule, this method returns an
     * empty array.
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_GoDown() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule GoDown
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_GoDown(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule GoDown
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule GoDown
     */
    private java.util.Hashtable getMapRuleGoDown() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("f", jeops_examples_fibonacci_Fibonacci_1);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule GoDown.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_GoDown(int index) {
        switch (index) {
            case 0: return "jeops.examples.fibonacci.Fibonacci";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule GoDown.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_GoDown(int index) {
        switch (index) {
            case 0: return jeops.examples.fibonacci.Fibonacci.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule GoDown.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_GoDown(int index, Object value) {
        switch (index) {
            case 0: this.jeops_examples_fibonacci_Fibonacci_1 = (jeops.examples.fibonacci.Fibonacci) value; break;
        }
    }

    /**
     * Returns an object declared in the rule GoDown.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_GoDown(int index) {
        switch (index) {
            case 0: return jeops_examples_fibonacci_Fibonacci_1;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule GoDown
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_GoDown() {
        return new Object[] {
                            jeops_examples_fibonacci_Fibonacci_1
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule GoDown
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_GoDown(Object[] objects) {
        jeops_examples_fibonacci_Fibonacci_1 = (jeops.examples.fibonacci.Fibonacci) objects[0];
    }

    /**
     * Precondition 0 of rule GoDown.<p>
     * The original expression was:<br>
     * <code>f.getValue() == -1</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean GoDown_prec_0() {
        return (jeops_examples_fibonacci_Fibonacci_1.getValue() == -1);
    }

    /**
     * Precondition 1 of rule GoDown.<p>
     * The original expression was:<br>
     * <code>f.getN() >= 2</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean GoDown_prec_1() {
        return (jeops_examples_fibonacci_Fibonacci_1.getN() >= 2);
    }

    /**
     * Precondition 2 of rule GoDown.<p>
     * The original expression was:<br>
     * <code>f.getSon1() == null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean GoDown_prec_2() {
        return (jeops_examples_fibonacci_Fibonacci_1.getSon1() == null);
    }

    /**
     * Checks whether some preconditions of rule GoDown is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean GoDown_prec(int index) {
        switch (index) {
            case 0: return GoDown_prec_0();
            case 1: return GoDown_prec_1();
            case 2: return GoDown_prec_2();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_GoDown(int declIndex) {
        setLocalDeclarations_GoDown(declIndex);
        switch (declIndex) {
            case 0:
                if (!GoDown_prec_0()) return false;
                if (!GoDown_prec_1()) return false;
                if (!GoDown_prec_2()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule GoDown
     */
    public void GoDown() {
      Fibonacci f1 = new Fibonacci(jeops_examples_fibonacci_Fibonacci_1.getN() - 1);
      Fibonacci f2 = new Fibonacci(jeops_examples_fibonacci_Fibonacci_1.getN() - 2);
      jeops_examples_fibonacci_Fibonacci_1.setSon1(f1);
      jeops_examples_fibonacci_Fibonacci_1.setSon2(f2);
      tell(f1);    // Let's tell our knowledge base
      tell(f2);    //   that these two sons exist.
      modified(jeops_examples_fibonacci_Fibonacci_1);
      }



  
    /**
     * Sets the local declaration for a given declaration index.
     * As there are no local declarations for this rule, this
     * method does nothing.
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_BaseCase(int index) {
    }

    /**
     * Returns a dependency table between the local declarations 
     * and the regular ones for rule BaseCase. As here are
     * no local declarations for this rule, this method returns an
     * empty array.
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_BaseCase() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule BaseCase
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_BaseCase(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule BaseCase
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule BaseCase
     */
    private java.util.Hashtable getMapRuleBaseCase() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("f", jeops_examples_fibonacci_Fibonacci_1);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule BaseCase.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_BaseCase(int index) {
        switch (index) {
            case 0: return "jeops.examples.fibonacci.Fibonacci";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule BaseCase.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_BaseCase(int index) {
        switch (index) {
            case 0: return jeops.examples.fibonacci.Fibonacci.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule BaseCase.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_BaseCase(int index, Object value) {
        switch (index) {
            case 0: this.jeops_examples_fibonacci_Fibonacci_1 = (jeops.examples.fibonacci.Fibonacci) value; break;
        }
    }

    /**
     * Returns an object declared in the rule BaseCase.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_BaseCase(int index) {
        switch (index) {
            case 0: return jeops_examples_fibonacci_Fibonacci_1;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule BaseCase
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_BaseCase() {
        return new Object[] {
                            jeops_examples_fibonacci_Fibonacci_1
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule BaseCase
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_BaseCase(Object[] objects) {
        jeops_examples_fibonacci_Fibonacci_1 = (jeops.examples.fibonacci.Fibonacci) objects[0];
    }

    /**
     * Precondition 0 of rule BaseCase.<p>
     * The original expression was:<br>
     * <code>f.getN() <= 1</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean BaseCase_prec_0() {
        return (jeops_examples_fibonacci_Fibonacci_1.getN() <= 1);
    }

    /**
     * Precondition 1 of rule BaseCase.<p>
     * The original expression was:<br>
     * <code>f.getValue() == -1</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean BaseCase_prec_1() {
        return (jeops_examples_fibonacci_Fibonacci_1.getValue() == -1);
    }

    /**
     * Checks whether some preconditions of rule BaseCase is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean BaseCase_prec(int index) {
        switch (index) {
            case 0: return BaseCase_prec_0();
            case 1: return BaseCase_prec_1();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_BaseCase(int declIndex) {
        setLocalDeclarations_BaseCase(declIndex);
        switch (declIndex) {
            case 0:
                if (!BaseCase_prec_0()) return false;
                if (!BaseCase_prec_1()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule BaseCase
     */
    public void BaseCase() {
      jeops_examples_fibonacci_Fibonacci_1.setValue(jeops_examples_fibonacci_Fibonacci_1.getN());
      modified(jeops_examples_fibonacci_Fibonacci_1);   // Yes, I modified f
      }



  
    /**
     * Sets the local declaration for a given declaration index.
     * As there are no local declarations for this rule, this
     * method does nothing.
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_GoUp(int index) {
    }

    /**
     * Returns a dependency table between the local declarations 
     * and the regular ones for rule GoUp. As here are
     * no local declarations for this rule, this method returns an
     * empty array.
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_GoUp() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule GoUp
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_GoUp(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule GoUp
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule GoUp
     */
    private java.util.Hashtable getMapRuleGoUp() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("f", jeops_examples_fibonacci_Fibonacci_1);
        result.put("f1", jeops_examples_fibonacci_Fibonacci_2);
        result.put("f2", jeops_examples_fibonacci_Fibonacci_3);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule GoUp.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_GoUp(int index) {
        switch (index) {
            case 0: return "jeops.examples.fibonacci.Fibonacci";
            case 1: return "jeops.examples.fibonacci.Fibonacci";
            case 2: return "jeops.examples.fibonacci.Fibonacci";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule GoUp.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_GoUp(int index) {
        switch (index) {
            case 0: return jeops.examples.fibonacci.Fibonacci.class;
            case 1: return jeops.examples.fibonacci.Fibonacci.class;
            case 2: return jeops.examples.fibonacci.Fibonacci.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule GoUp.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_GoUp(int index, Object value) {
        switch (index) {
            case 0: this.jeops_examples_fibonacci_Fibonacci_1 = (jeops.examples.fibonacci.Fibonacci) value; break;
            case 1: this.jeops_examples_fibonacci_Fibonacci_2 = (jeops.examples.fibonacci.Fibonacci) value; break;
            case 2: this.jeops_examples_fibonacci_Fibonacci_3 = (jeops.examples.fibonacci.Fibonacci) value; break;
        }
    }

    /**
     * Returns an object declared in the rule GoUp.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_GoUp(int index) {
        switch (index) {
            case 0: return jeops_examples_fibonacci_Fibonacci_1;
            case 1: return jeops_examples_fibonacci_Fibonacci_2;
            case 2: return jeops_examples_fibonacci_Fibonacci_3;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule GoUp
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_GoUp() {
        return new Object[] {
                            jeops_examples_fibonacci_Fibonacci_1,
                            jeops_examples_fibonacci_Fibonacci_2,
                            jeops_examples_fibonacci_Fibonacci_3
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule GoUp
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_GoUp(Object[] objects) {
        jeops_examples_fibonacci_Fibonacci_1 = (jeops.examples.fibonacci.Fibonacci) objects[0];
        jeops_examples_fibonacci_Fibonacci_2 = (jeops.examples.fibonacci.Fibonacci) objects[1];
        jeops_examples_fibonacci_Fibonacci_3 = (jeops.examples.fibonacci.Fibonacci) objects[2];
    }

    /**
     * Precondition 0 of rule GoUp.<p>
     * The original expression was:<br>
     * <code>f1 != null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean GoUp_prec_0() {
        return (jeops_examples_fibonacci_Fibonacci_2 != null);
    }

    /**
     * Precondition 1 of rule GoUp.<p>
     * The original expression was:<br>
     * <code>f2 != null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean GoUp_prec_1() {
        return (jeops_examples_fibonacci_Fibonacci_3 != null);
    }

    /**
     * Precondition 2 of rule GoUp.<p>
     * The original expression was:<br>
     * <code>f1 == f.getSon1()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean GoUp_prec_2() {
        return (jeops_examples_fibonacci_Fibonacci_2 == jeops_examples_fibonacci_Fibonacci_1.getSon1());
    }

    /**
     * Precondition 3 of rule GoUp.<p>
     * The original expression was:<br>
     * <code>f2 == f.getSon2()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean GoUp_prec_3() {
        return (jeops_examples_fibonacci_Fibonacci_3 == jeops_examples_fibonacci_Fibonacci_1.getSon2());
    }

    /**
     * Precondition 4 of rule GoUp.<p>
     * The original expression was:<br>
     * <code>f.getValue() == -1</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean GoUp_prec_4() {
        return (jeops_examples_fibonacci_Fibonacci_1.getValue() == -1);
    }

    /**
     * Precondition 5 of rule GoUp.<p>
     * The original expression was:<br>
     * <code>f.getN() >= 2</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean GoUp_prec_5() {
        return (jeops_examples_fibonacci_Fibonacci_1.getN() >= 2);
    }

    /**
     * Precondition 6 of rule GoUp.<p>
     * The original expression was:<br>
     * <code>f1.getValue() != -1</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean GoUp_prec_6() {
        return (jeops_examples_fibonacci_Fibonacci_2.getValue() != -1);
    }

    /**
     * Precondition 7 of rule GoUp.<p>
     * The original expression was:<br>
     * <code>f2.getValue() != -1</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean GoUp_prec_7() {
        return (jeops_examples_fibonacci_Fibonacci_3.getValue() != -1);
    }

    /**
     * Checks whether some preconditions of rule GoUp is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean GoUp_prec(int index) {
        switch (index) {
            case 0: return GoUp_prec_0();
            case 1: return GoUp_prec_1();
            case 2: return GoUp_prec_2();
            case 3: return GoUp_prec_3();
            case 4: return GoUp_prec_4();
            case 5: return GoUp_prec_5();
            case 6: return GoUp_prec_6();
            case 7: return GoUp_prec_7();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_GoUp(int declIndex) {
        setLocalDeclarations_GoUp(declIndex);
        switch (declIndex) {
            case 0:
                if (!GoUp_prec_4()) return false;
                if (!GoUp_prec_5()) return false;
                return true;
            case 1:
                if (!GoUp_prec_0()) return false;
                if (!GoUp_prec_2()) return false;
                if (!GoUp_prec_6()) return false;
                return true;
            case 2:
                if (!GoUp_prec_1()) return false;
                if (!GoUp_prec_3()) return false;
                if (!GoUp_prec_7()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule GoUp
     */
    public void GoUp() {
      jeops_examples_fibonacci_Fibonacci_1.setValue(jeops_examples_fibonacci_Fibonacci_2.getValue() + jeops_examples_fibonacci_Fibonacci_3.getValue());
      retract(jeops_examples_fibonacci_Fibonacci_2);  // I don't need
      retract(jeops_examples_fibonacci_Fibonacci_3);  //   them anymore...
      modified(jeops_examples_fibonacci_Fibonacci_1);
      }




    /**
     * Returns the name of the rules in this class file.
     *
     * @return the name of the rules in this class file.
     */
    public String[] getRuleNames() {
        int numberOfRules = 3;
    	String[] result = new String[numberOfRules];
        result[0] = "GoDown";
        result[1] = "BaseCase";
        result[2] = "GoUp";
        return result;
    }

    /**
     * Returns the number of declarations of the rules in this class file.
     *
     * @return the number of declarations  of the rules in this class file.
     */
    public int[] getNumberOfDeclarations() {
        int numberOfRules = 3;
    	int[] result = new int[numberOfRules];
        result[0] = 1;
        result[1] = 1;
        result[2] = 3;
        return result;
    }

    /**
     * Returns the number of local declarations of the rules in this class file.
     *
     * @return the number of local declarations  of the rules in this class file.
     */
    public int[] getNumberOfLocalDeclarations() {
        int numberOfRules = 0;
    	int[] result = new int[numberOfRules];
        return result;
    }

    /**
     * Returns the number of preconditions of the rules in this class file.
     *
     * @return the number of preconditions  of the rules in this class file.
     */
    public int[] getNumberOfPreconditions() {
        int numberOfRules = 3;
    	int[] result = new int[numberOfRules];
        result[0] = 3;
        result[1] = 2;
        result[2] = 8;
        return result;
    }

    /**
     * Checks whether some preconditions of some rule is satisfied.
     *
     * @param ruleIndex the index of the rule to be checked
     * @param precIndex the index of the precondition to be checked
     * @return <code>true</code> if the corresponding precondition for the
     *          given rule is satisfied. <code>false</code> otherwise.
     */
    public boolean checkPrecondition(int ruleIndex, int precIndex) {
        switch (ruleIndex) {
            case 0: return GoDown_prec(precIndex);
            case 1: return BaseCase_prec(precIndex);
            case 2: return GoUp_prec(precIndex);
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference only the elements declared up to the given index
     * are true.
     *
     * @param ruleIndex the index of the rule to be checked
     * @param declIndex the index of the declaration to be checked
     * @return <code>true</code> if all the preconditions of a rule which
     *          reference only the elements declared up to the given index
     *          are satisfied; <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return checkPrecForDeclaration_GoDown(declIndex);
            case 1: return checkPrecForDeclaration_BaseCase(declIndex);
            case 2: return checkPrecForDeclaration_GoUp(declIndex);
            default: return false;
        }
    }

    /**
     * Returns the class name of an object declared in a rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration
     * @return the class name of the declared object.
     */
    public String getDeclaredClassName(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return getDeclaredClassName_GoDown(declIndex);
            case 1: return getDeclaredClassName_BaseCase(declIndex);
            case 2: return getDeclaredClassName_GoUp(declIndex);
            default: return null;
        }
    }

    /**
     * Returns the class of an object declared in a rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration
     * @return the class of the declared object.
     */
    public Class getDeclaredClass(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return getDeclaredClass_GoDown(declIndex);
            case 1: return getDeclaredClass_BaseCase(declIndex);
            case 2: return getDeclaredClass_GoUp(declIndex);
            default: return null;
        }
    }

    /**
     * Fires one of the rules in this rule base.
     *
     * @param ruleIndex the index of the rule to be fired
     */
    protected void internalFireRule(int ruleIndex) {
        switch (ruleIndex) {
            case 0: GoDown(); break;
            case 1: BaseCase(); break;
            case 2: GoUp(); break;
        }
    }

    /**
     * Returns the number of rules.
     *
     * @return the number of rules.
     */
    public int getNumberOfRules() {
        return 3;
    }

    /**
     * Returns a table that maps each declared identifier of a rule
     * into its corresponding value.
     *
     * @param ruleIndex the index of the rule
     * @return a table that maps each declared identifier of a rule
     *          into its corresponding value.
     */
    public java.util.Hashtable getInternalRuleMapping(int ruleIndex) {
        switch (ruleIndex) {
            case 0: return getMapRuleGoDown();
            case 1: return getMapRuleBaseCase();
            case 2: return getMapRuleGoUp();
            default: return new java.util.Hashtable();
        }
    }

    /**
     * Sets an object that represents a declaration of some rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration in the rule.
     * @param value the value of the object being set.
     */
    public void setObject(int ruleIndex, int declIndex, Object value) {
        switch (ruleIndex) {
            case 0: setObject_GoDown(declIndex, value); break;
            case 1: setObject_BaseCase(declIndex, value); break;
            case 2: setObject_GoUp(declIndex, value); break;
        }
    }

    /**
     * Returns an object that represents a declaration of some rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration in the rule.
     * @return the value of the corresponding object.
     */
    public Object getObject(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return getObject_GoDown(declIndex);
            case 1: return getObject_BaseCase(declIndex);
            case 2: return getObject_GoUp(declIndex);
            default: return null;
        }
    }

    /**
     * Returns a dependency table between the local declarations and the
     * regular ones.
     *
     * @param ruleIndex the index of the rule
     * @return a two-dimensional array with the dependencies between the
     *          local declarations and the regular ones.
     */
    public Object[][] getLocalDeclarationDependency(int ruleIndex) {
        switch (ruleIndex) {
            case 0: return getLocalDeclarationDependency_GoDown();
            case 1: return getLocalDeclarationDependency_BaseCase();
            case 2: return getLocalDeclarationDependency_GoUp();
            default: return null;
        }
    }
    /**
     * Defines all variables bound to the local declarations of
     * some rule.
     *
     * @param ruleIndex the index of the rule
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations.
     *          of some rule.
     */
    protected void setLocalObjects(int ruleIndex, Object[][] objects) {
        switch (ruleIndex) {
            case 0: setLocalObjects_GoDown(objects); break;
            case 1: setLocalObjects_BaseCase(objects); break;
            case 2: setLocalObjects_GoUp(objects); break;
        }
    }
    /**
     * Returns all variables bound to the declarations of
     * some rule.
     *
     * @param ruleIndex the index of the rule
     * @return an object array of the variables bound to the
     *          declarations of some rule.
     */
    protected Object[] getObjects(int ruleIndex) {
        switch (ruleIndex) {
            case 0: return getObjects_GoDown();
            case 1: return getObjects_BaseCase();
            case 2: return getObjects_GoUp();
            default: return null;
        }
    }
    /**
     * Defines all variables bound to the declarations of
     * some rule.
     *
     * @param ruleIndex the index of the rule
     * @param objects an object array of the variables bound to the
     *          declarations of some rule.
     */
    protected void setObjects(int ruleIndex, Object[] objects) {
        switch (ruleIndex) {
            case 0: setObjects_GoDown(objects); break;
            case 1: setObjects_BaseCase(objects); break;
            case 2: setObjects_GoUp(objects); break;
        }
    }

    /**
     * Returns a mapping from the classes of the objects declared in the
     * rules to the number of occurrences of each one.
     *
     * @return a mapping from the classes of the objects declared in the
     *          rules to the number of occurrences of each one.
     */
    public java.util.Hashtable getClassDeclarationCount() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("jeops.examples.fibonacci.Fibonacci", new Integer(3));
        return result;
    }

    /*
     * The variables declared in the preconditions.
     */
    jeops.examples.fibonacci.Fibonacci jeops_examples_fibonacci_Fibonacci_1;
    jeops.examples.fibonacci.Fibonacci jeops_examples_fibonacci_Fibonacci_2;
    jeops.examples.fibonacci.Fibonacci jeops_examples_fibonacci_Fibonacci_3;

    /**
     * Constant used to aviod the creation of several empty arrays.
     */
    private static final Object[] EMPTY_OBJECT_ARRAY = new Object[0];

    /**
     * Constant used to aviod the creation of several empty double arrays.
     */
    private static final Object[][] EMPTY_OBJECT_DOUBLE_ARRAY = new Object[0][0];

    /**
     * Constant used to aviod the creation of several empty vectors.
     */
    private static final java.util.Vector EMPTY_VECTOR = new java.util.Vector();

    /**
     * Class constructor.
     *
     * @param objectBase an object base over with this rule base will work.
     * @param knowledgeBase the knowledge base that contains this rule base.
     */
    public Jeops_RuleBase_FibonacciBase(jeops.ObjectBase objectBase,
                                jeops.AbstractKnowledgeBase knowledgeBase) {
        super(objectBase, knowledgeBase);
    }

}
/**
 * Knowledge base created by JEOPS from file ./src/jeops/examples/fibonacci/FibonacciBase.rules
 *
 * @version Apr 23, 2016
 */
public class FibonacciBase extends jeops.AbstractKnowledgeBase {

    /**
     * Creates a new knowledge base with the specified conflict set with the
     * desired conflict resolution policy.
     *
     * @param conflictSet a conflict set with the desired resolution policy
     */
    public FibonacciBase(jeops.conflict.ConflictSet conflictSet) {
        super(conflictSet);
        jeops.AbstractRuleBase ruleBase;
        jeops.ObjectBase objectBase = getObjectBase();
        ruleBase = new Jeops_RuleBase_FibonacciBase(objectBase, this);
        setRuleBase(ruleBase);
    }

    /**
     * Creates a new knowledge base, using the default conflict resolution
     * policy.
     */
    public FibonacciBase() {
        this(new jeops.conflict.DefaultConflictSet());
    }

}
