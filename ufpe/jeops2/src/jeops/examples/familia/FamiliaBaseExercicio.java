package jeops.examples.familia;

 class Jeops_RuleBase_FamiliaBaseExercicio extends jeops.AbstractRuleBase {

    
    /**
     * Local declaration 0 of rule encontraAvos.<p>
     * The original expression was:<br>
     * <code>p.getPai()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public void setencontraAvos_localDecl_0() {
        local_decl_jeops_examples_familia_Pessoa_1 = (jeops_examples_familia_Pessoa_1.getPai());
    }

    /**
     * Sets the local declaration for a given declaration index
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_encontraAvos(int declIndex) {
        if (declIndex == 0) setencontraAvos_localDecl_0();
    }

    /**
     * Returns a dependency table between the local declarations
     * and the regular ones for rule encontraAvos
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_encontraAvos() {
        Object[][] result = new Object[1][];
        result[0] = new Object[2];
        result[0][0] = local_decl_jeops_examples_familia_Pessoa_1;
        result[0][1] = jeops_examples_familia_Pessoa_1;
        return result;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule encontraAvos
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_encontraAvos(Object[][] objects) {
        local_decl_jeops_examples_familia_Pessoa_1 = (jeops.examples.familia.Pessoa) objects[0][0];
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule encontraAvos
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule encontraAvos
     */
    private java.util.Hashtable getMapRuleencontraAvos() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("p", jeops_examples_familia_Pessoa_1);
        result.put("o", jeops_examples_familia_Objetivo_1);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule encontraAvos.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_encontraAvos(int index) {
        switch (index) {
            case 0: return "jeops.examples.familia.Pessoa";
            case 1: return "jeops.examples.familia.Objetivo";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule encontraAvos.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_encontraAvos(int index) {
        switch (index) {
            case 0: return jeops.examples.familia.Pessoa.class;
            case 1: return jeops.examples.familia.Objetivo.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule encontraAvos.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_encontraAvos(int index, Object value) {
        switch (index) {
            case 0: this.jeops_examples_familia_Pessoa_1 = (jeops.examples.familia.Pessoa) value; break;
            case 1: this.jeops_examples_familia_Objetivo_1 = (jeops.examples.familia.Objetivo) value; break;
        }
    }

    /**
     * Returns an object declared in the rule encontraAvos.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_encontraAvos(int index) {
        switch (index) {
            case 0: return jeops_examples_familia_Pessoa_1;
            case 1: return jeops_examples_familia_Objetivo_1;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule encontraAvos
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_encontraAvos() {
        return new Object[] {
                            jeops_examples_familia_Pessoa_1,
                            jeops_examples_familia_Objetivo_1
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule encontraAvos
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_encontraAvos(Object[] objects) {
        jeops_examples_familia_Pessoa_1 = (jeops.examples.familia.Pessoa) objects[0];
        jeops_examples_familia_Objetivo_1 = (jeops.examples.familia.Objetivo) objects[1];
    }

    /**
     * Precondition 0 of rule encontraAvos.<p>
     * The original expression was:<br>
     * <code>p == o.getAlvo()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean encontraAvos_prec_0() {
        return (jeops_examples_familia_Pessoa_1 == jeops_examples_familia_Objetivo_1.getAlvo());
    }

    /**
     * Precondition 1 of rule encontraAvos.<p>
     * The original expression was:<br>
     * <code>o.isAtivo()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean encontraAvos_prec_1() {
        return (jeops_examples_familia_Objetivo_1.isAtivo());
    }

    /**
     * Precondition 2 of rule encontraAvos.<p>
     * The original expression was:<br>
     * <code>pai != null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean encontraAvos_prec_2() {
        return (local_decl_jeops_examples_familia_Pessoa_1 != null);
    }

    /**
     * Precondition 3 of rule encontraAvos.<p>
     * The original expression was:<br>
     * <code>pai.getPai() != null || pai.getMae() != null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean encontraAvos_prec_3() {
        return (local_decl_jeops_examples_familia_Pessoa_1.getPai() != null || local_decl_jeops_examples_familia_Pessoa_1.getMae() != null);
    }

    /**
     * Checks whether some preconditions of rule encontraAvos is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean encontraAvos_prec(int index) {
        switch (index) {
            case 0: return encontraAvos_prec_0();
            case 1: return encontraAvos_prec_1();
            case 2: return encontraAvos_prec_2();
            case 3: return encontraAvos_prec_3();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_encontraAvos(int declIndex) {
        setLocalDeclarations_encontraAvos(declIndex);
        switch (declIndex) {
            case 0:
                if (!encontraAvos_prec_2()) return false;
                if (!encontraAvos_prec_3()) return false;
                return true;
            case 1:
                if (!encontraAvos_prec_0()) return false;
                if (!encontraAvos_prec_1()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule encontraAvos
     */
    public void encontraAvos() {
			jeops_examples_familia_Objetivo_1.desativar();
            
            if (local_decl_jeops_examples_familia_Pessoa_1.getPai() != null) {
				System.out.println(local_decl_jeops_examples_familia_Pessoa_1.getPai().getNome() + " é avô de "+ jeops_examples_familia_Pessoa_1.getNome());
            }
            
			if (local_decl_jeops_examples_familia_Pessoa_1.getMae() != null) {
				System.out.println(local_decl_jeops_examples_familia_Pessoa_1.getMae().getNome() + " é avó de "+ jeops_examples_familia_Pessoa_1.getNome());
            }
            
            tell(new Objetivo(local_decl_jeops_examples_familia_Pessoa_1));
	    }



    /**
     * Returns the name of the rules in this class file.
     *
     * @return the name of the rules in this class file.
     */
    public String[] getRuleNames() {
        int numberOfRules = 1;
    	String[] result = new String[numberOfRules];
        result[0] = "encontraAvos";
        return result;
    }

    /**
     * Returns the number of declarations of the rules in this class file.
     *
     * @return the number of declarations  of the rules in this class file.
     */
    public int[] getNumberOfDeclarations() {
        int numberOfRules = 1;
    	int[] result = new int[numberOfRules];
        result[0] = 2;
        return result;
    }

    /**
     * Returns the number of local declarations of the rules in this class file.
     *
     * @return the number of local declarations  of the rules in this class file.
     */
    public int[] getNumberOfLocalDeclarations() {
        int numberOfRules = 1;
    	int[] result = new int[numberOfRules];
        result[0] = 1;
        return result;
    }

    /**
     * Returns the number of preconditions of the rules in this class file.
     *
     * @return the number of preconditions  of the rules in this class file.
     */
    public int[] getNumberOfPreconditions() {
        int numberOfRules = 1;
    	int[] result = new int[numberOfRules];
        result[0] = 4;
        return result;
    }

    /**
     * Checks whether some preconditions of some rule is satisfied.
     *
     * @param ruleIndex the index of the rule to be checked
     * @param precIndex the index of the precondition to be checked
     * @return <code>true</code> if the corresponding precondition for the
     *          given rule is satisfied. <code>false</code> otherwise.
     */
    public boolean checkPrecondition(int ruleIndex, int precIndex) {
        switch (ruleIndex) {
            case 0: return encontraAvos_prec(precIndex);
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference only the elements declared up to the given index
     * are true.
     *
     * @param ruleIndex the index of the rule to be checked
     * @param declIndex the index of the declaration to be checked
     * @return <code>true</code> if all the preconditions of a rule which
     *          reference only the elements declared up to the given index
     *          are satisfied; <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return checkPrecForDeclaration_encontraAvos(declIndex);
            default: return false;
        }
    }

    /**
     * Returns the class name of an object declared in a rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration
     * @return the class name of the declared object.
     */
    public String getDeclaredClassName(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return getDeclaredClassName_encontraAvos(declIndex);
            default: return null;
        }
    }

    /**
     * Returns the class of an object declared in a rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration
     * @return the class of the declared object.
     */
    public Class getDeclaredClass(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return getDeclaredClass_encontraAvos(declIndex);
            default: return null;
        }
    }

    /**
     * Fires one of the rules in this rule base.
     *
     * @param ruleIndex the index of the rule to be fired
     */
    protected void internalFireRule(int ruleIndex) {
        switch (ruleIndex) {
            case 0: encontraAvos(); break;
        }
    }

    /**
     * Returns the number of rules.
     *
     * @return the number of rules.
     */
    public int getNumberOfRules() {
        return 1;
    }

    /**
     * Returns a table that maps each declared identifier of a rule
     * into its corresponding value.
     *
     * @param ruleIndex the index of the rule
     * @return a table that maps each declared identifier of a rule
     *          into its corresponding value.
     */
    public java.util.Hashtable getInternalRuleMapping(int ruleIndex) {
        switch (ruleIndex) {
            case 0: return getMapRuleencontraAvos();
            default: return new java.util.Hashtable();
        }
    }

    /**
     * Sets an object that represents a declaration of some rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration in the rule.
     * @param value the value of the object being set.
     */
    public void setObject(int ruleIndex, int declIndex, Object value) {
        switch (ruleIndex) {
            case 0: setObject_encontraAvos(declIndex, value); break;
        }
    }

    /**
     * Returns an object that represents a declaration of some rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration in the rule.
     * @return the value of the corresponding object.
     */
    public Object getObject(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return getObject_encontraAvos(declIndex);
            default: return null;
        }
    }

    /**
     * Returns a dependency table between the local declarations and the
     * regular ones.
     *
     * @param ruleIndex the index of the rule
     * @return a two-dimensional array with the dependencies between the
     *          local declarations and the regular ones.
     */
    public Object[][] getLocalDeclarationDependency(int ruleIndex) {
        switch (ruleIndex) {
            case 0: return getLocalDeclarationDependency_encontraAvos();
            default: return null;
        }
    }
    /**
     * Defines all variables bound to the local declarations of
     * some rule.
     *
     * @param ruleIndex the index of the rule
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations.
     *          of some rule.
     */
    protected void setLocalObjects(int ruleIndex, Object[][] objects) {
        switch (ruleIndex) {
            case 0: setLocalObjects_encontraAvos(objects); break;
        }
    }
    /**
     * Returns all variables bound to the declarations of
     * some rule.
     *
     * @param ruleIndex the index of the rule
     * @return an object array of the variables bound to the
     *          declarations of some rule.
     */
    protected Object[] getObjects(int ruleIndex) {
        switch (ruleIndex) {
            case 0: return getObjects_encontraAvos();
            default: return null;
        }
    }
    /**
     * Defines all variables bound to the declarations of
     * some rule.
     *
     * @param ruleIndex the index of the rule
     * @param objects an object array of the variables bound to the
     *          declarations of some rule.
     */
    protected void setObjects(int ruleIndex, Object[] objects) {
        switch (ruleIndex) {
            case 0: setObjects_encontraAvos(objects); break;
        }
    }

    /**
     * Returns a mapping from the classes of the objects declared in the
     * rules to the number of occurrences of each one.
     *
     * @return a mapping from the classes of the objects declared in the
     *          rules to the number of occurrences of each one.
     */
    public java.util.Hashtable getClassDeclarationCount() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("jeops.examples.familia.Objetivo", new Integer(1));
        result.put("jeops.examples.familia.Pessoa", new Integer(1));
        return result;
    }

    /*
     * The variables declared in the preconditions.
     */
    jeops.examples.familia.Pessoa jeops_examples_familia_Pessoa_1;
    jeops.examples.familia.Objetivo jeops_examples_familia_Objetivo_1;
    jeops.examples.familia.Pessoa local_decl_jeops_examples_familia_Pessoa_1;

    /**
     * Constant used to aviod the creation of several empty arrays.
     */
    private static final Object[] EMPTY_OBJECT_ARRAY = new Object[0];

    /**
     * Constant used to aviod the creation of several empty double arrays.
     */
    private static final Object[][] EMPTY_OBJECT_DOUBLE_ARRAY = new Object[0][0];

    /**
     * Constant used to aviod the creation of several empty vectors.
     */
    private static final java.util.Vector EMPTY_VECTOR = new java.util.Vector();

    /**
     * Class constructor.
     *
     * @param objectBase an object base over with this rule base will work.
     * @param knowledgeBase the knowledge base that contains this rule base.
     */
    public Jeops_RuleBase_FamiliaBaseExercicio(jeops.ObjectBase objectBase,
                                jeops.AbstractKnowledgeBase knowledgeBase) {
        super(objectBase, knowledgeBase);
    }

}
/**
 * Knowledge base created by JEOPS from file ./src/jeops/examples/familia/FamiliaBaseExercicio.rules
 *
 * @version Apr 23, 2016
 */
public class FamiliaBaseExercicio extends jeops.AbstractKnowledgeBase {

    /**
     * Creates a new knowledge base with the specified conflict set with the
     * desired conflict resolution policy.
     *
     * @param conflictSet a conflict set with the desired resolution policy
     */
    public FamiliaBaseExercicio(jeops.conflict.ConflictSet conflictSet) {
        super(conflictSet);
        jeops.AbstractRuleBase ruleBase;
        jeops.ObjectBase objectBase = getObjectBase();
        ruleBase = new Jeops_RuleBase_FamiliaBaseExercicio(objectBase, this);
        setRuleBase(ruleBase);
    }

    /**
     * Creates a new knowledge base, using the default conflict resolution
     * policy.
     */
    public FamiliaBaseExercicio() {
        this(new jeops.conflict.DefaultConflictSet());
    }

}
