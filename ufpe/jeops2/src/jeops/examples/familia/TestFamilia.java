package jeops.examples.familia;

public class TestFamilia {
	public static void main(String[] args) {
		FamiliaBase kb = new FamiliaBase();
		
		Pessoa avom = new Pessoa("Joseh");
		Pessoa avof = new Pessoa("Joana");
		Pessoa pai = new Pessoa("Joao");
		Pessoa mae = new Pessoa("Maria");
		Pessoa filho = new Pessoa("Francisco");

		filho.setPai(pai);
		filho.setMae(mae);
		
		pai.setMae(avof);
		pai.setPai(avom);

		Objetivo objetivo = new Objetivo(filho);
		
		kb.tell(objetivo);
		kb.tell(filho);
		kb.tell(pai);
		kb.tell(mae);
		
		kb.run();
		
	}
}
