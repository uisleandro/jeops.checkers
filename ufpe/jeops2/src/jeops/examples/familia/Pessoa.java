package jeops.examples.familia;

public class Pessoa {

	private String nome;

	private Pessoa pai;
	
	private Pessoa mae;
	
	public Pessoa(String nome) {
		this.nome = nome;
	}

	public Pessoa getMae() {
		return mae;
	}

	public void setMae(Pessoa mae) {
		this.mae = mae;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Pessoa getPai() {
		return pai;
	}

	public void setPai(Pessoa pai) {
		this.pai = pai;
	}
	
}
