package jeops.examples.familia;

public class Objetivo {
	
	private boolean ativo;

	private Pessoa alvo;
	
	public Objetivo(Pessoa alvo) {
		super();
		this.alvo = alvo;
		this.ativo = true;
	}

	public Pessoa getAlvo() {
		return alvo;
	}

	public void setAlvo(Pessoa alvo) {
		this.alvo = alvo;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void ativar() {
		this.ativo = true;
	}
	
	public void desativar() {
		this.ativo = false;		
	}
	
}
