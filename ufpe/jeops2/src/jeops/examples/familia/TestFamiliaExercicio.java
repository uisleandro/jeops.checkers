package jeops.examples.familia;

public class TestFamiliaExercicio {
	public static void main(String[] args) {
		FamiliaBaseExercicio kb = new FamiliaBaseExercicio();
		
		Pessoa avom = new Pessoa("José");
		Pessoa avof = new Pessoa("Joana");
		Pessoa pai = new Pessoa("João");
		Pessoa mae = new Pessoa("Maria");
		Pessoa filho = new Pessoa("Francisco");

		filho.setPai(pai);
		filho.setMae(mae);
		
		pai.setMae(avof);
		pai.setPai(avom);

		Objetivo objetivo = new Objetivo(filho);
		
		kb.tell(objetivo);
		kb.tell(filho);
		kb.tell(pai);
		kb.tell(mae);
		
		kb.run();
		
	}
}
