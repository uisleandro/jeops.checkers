package jeops.examples.factorial;

/**
 * Rule base used to solve the factorial problem with JEOPS.
 *
 * @author Carlos Figueira Filho (<a href="mailto:csff@di.ufpe.br">csff@di.ufpe.br</a>)
 */
  class Jeops_RuleBase_FactBase extends jeops.AbstractRuleBase {

	
    /**
     * Sets the local declaration for a given declaration index.
     * As there are no local declarations for this rule, this
     * method does nothing.
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_goDown(int index) {
    }

    /**
     * Returns a dependency table between the local declarations 
     * and the regular ones for rule goDown. As here are
     * no local declarations for this rule, this method returns an
     * empty array.
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_goDown() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule goDown
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_goDown(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule goDown
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule goDown
     */
    private java.util.Hashtable getMapRulegoDown() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("f", jeops_examples_factorial_Fact_1);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule goDown.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_goDown(int index) {
        switch (index) {
            case 0: return "jeops.examples.factorial.Fact";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule goDown.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_goDown(int index) {
        switch (index) {
            case 0: return jeops.examples.factorial.Fact.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule goDown.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_goDown(int index, Object value) {
        switch (index) {
            case 0: this.jeops_examples_factorial_Fact_1 = (jeops.examples.factorial.Fact) value; break;
        }
    }

    /**
     * Returns an object declared in the rule goDown.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_goDown(int index) {
        switch (index) {
            case 0: return jeops_examples_factorial_Fact_1;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule goDown
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_goDown() {
        return new Object[] {
                            jeops_examples_factorial_Fact_1
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule goDown
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_goDown(Object[] objects) {
        jeops_examples_factorial_Fact_1 = (jeops.examples.factorial.Fact) objects[0];
    }

    /**
     * Precondition 0 of rule goDown.<p>
     * The original expression was:<br>
     * <code>!f.isOk()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean goDown_prec_0() {
        return (!jeops_examples_factorial_Fact_1.isOk());
    }

    /**
     * Precondition 1 of rule goDown.<p>
     * The original expression was:<br>
     * <code>f.getN() > 1</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean goDown_prec_1() {
        return (jeops_examples_factorial_Fact_1.getN() > 1);
    }

    /**
     * Precondition 2 of rule goDown.<p>
     * The original expression was:<br>
     * <code>f.getSubProblem() == null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean goDown_prec_2() {
        return (jeops_examples_factorial_Fact_1.getSubProblem() == null);
    }

    /**
     * Checks whether some preconditions of rule goDown is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean goDown_prec(int index) {
        switch (index) {
            case 0: return goDown_prec_0();
            case 1: return goDown_prec_1();
            case 2: return goDown_prec_2();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_goDown(int declIndex) {
        setLocalDeclarations_goDown(declIndex);
        switch (declIndex) {
            case 0:
                if (!goDown_prec_0()) return false;
                if (!goDown_prec_1()) return false;
                if (!goDown_prec_2()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule goDown
     */
    public void goDown() {
			Fact sub = new Fact(jeops_examples_factorial_Fact_1.getN() - 1);
			jeops_examples_factorial_Fact_1.setSubProblem(sub);
			tell(sub);
			modified(jeops_examples_factorial_Fact_1);
	    }



	
    /**
     * Sets the local declaration for a given declaration index.
     * As there are no local declarations for this rule, this
     * method does nothing.
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_goUp(int index) {
    }

    /**
     * Returns a dependency table between the local declarations 
     * and the regular ones for rule goUp. As here are
     * no local declarations for this rule, this method returns an
     * empty array.
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_goUp() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule goUp
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_goUp(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule goUp
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule goUp
     */
    private java.util.Hashtable getMapRulegoUp() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("f", jeops_examples_factorial_Fact_1);
        result.put("sub", jeops_examples_factorial_Fact_2);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule goUp.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_goUp(int index) {
        switch (index) {
            case 0: return "jeops.examples.factorial.Fact";
            case 1: return "jeops.examples.factorial.Fact";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule goUp.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_goUp(int index) {
        switch (index) {
            case 0: return jeops.examples.factorial.Fact.class;
            case 1: return jeops.examples.factorial.Fact.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule goUp.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_goUp(int index, Object value) {
        switch (index) {
            case 0: this.jeops_examples_factorial_Fact_1 = (jeops.examples.factorial.Fact) value; break;
            case 1: this.jeops_examples_factorial_Fact_2 = (jeops.examples.factorial.Fact) value; break;
        }
    }

    /**
     * Returns an object declared in the rule goUp.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_goUp(int index) {
        switch (index) {
            case 0: return jeops_examples_factorial_Fact_1;
            case 1: return jeops_examples_factorial_Fact_2;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule goUp
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_goUp() {
        return new Object[] {
                            jeops_examples_factorial_Fact_1,
                            jeops_examples_factorial_Fact_2
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule goUp
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_goUp(Object[] objects) {
        jeops_examples_factorial_Fact_1 = (jeops.examples.factorial.Fact) objects[0];
        jeops_examples_factorial_Fact_2 = (jeops.examples.factorial.Fact) objects[1];
    }

    /**
     * Precondition 0 of rule goUp.<p>
     * The original expression was:<br>
     * <code>!f.isOk()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean goUp_prec_0() {
        return (!jeops_examples_factorial_Fact_1.isOk());
    }

    /**
     * Precondition 1 of rule goUp.<p>
     * The original expression was:<br>
     * <code>sub == f.getSubProblem()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean goUp_prec_1() {
        return (jeops_examples_factorial_Fact_2 == jeops_examples_factorial_Fact_1.getSubProblem());
    }

    /**
     * Precondition 2 of rule goUp.<p>
     * The original expression was:<br>
     * <code>sub != null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean goUp_prec_2() {
        return (jeops_examples_factorial_Fact_2 != null);
    }

    /**
     * Precondition 3 of rule goUp.<p>
     * The original expression was:<br>
     * <code>sub.isOk()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean goUp_prec_3() {
        return (jeops_examples_factorial_Fact_2.isOk());
    }

    /**
     * Checks whether some preconditions of rule goUp is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean goUp_prec(int index) {
        switch (index) {
            case 0: return goUp_prec_0();
            case 1: return goUp_prec_1();
            case 2: return goUp_prec_2();
            case 3: return goUp_prec_3();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_goUp(int declIndex) {
        setLocalDeclarations_goUp(declIndex);
        switch (declIndex) {
            case 0:
                if (!goUp_prec_0()) return false;
                return true;
            case 1:
                if (!goUp_prec_1()) return false;
                if (!goUp_prec_2()) return false;
                if (!goUp_prec_3()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule goUp
     */
    public void goUp() {
			jeops_examples_factorial_Fact_1.setResult(jeops_examples_factorial_Fact_1.getN() * jeops_examples_factorial_Fact_2.getResult());
			modified(jeops_examples_factorial_Fact_1);
	    }



	
    /**
     * Sets the local declaration for a given declaration index.
     * As there are no local declarations for this rule, this
     * method does nothing.
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_baseCase(int index) {
    }

    /**
     * Returns a dependency table between the local declarations 
     * and the regular ones for rule baseCase. As here are
     * no local declarations for this rule, this method returns an
     * empty array.
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_baseCase() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule baseCase
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_baseCase(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule baseCase
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule baseCase
     */
    private java.util.Hashtable getMapRulebaseCase() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("f", jeops_examples_factorial_Fact_1);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule baseCase.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_baseCase(int index) {
        switch (index) {
            case 0: return "jeops.examples.factorial.Fact";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule baseCase.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_baseCase(int index) {
        switch (index) {
            case 0: return jeops.examples.factorial.Fact.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule baseCase.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_baseCase(int index, Object value) {
        switch (index) {
            case 0: this.jeops_examples_factorial_Fact_1 = (jeops.examples.factorial.Fact) value; break;
        }
    }

    /**
     * Returns an object declared in the rule baseCase.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_baseCase(int index) {
        switch (index) {
            case 0: return jeops_examples_factorial_Fact_1;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule baseCase
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_baseCase() {
        return new Object[] {
                            jeops_examples_factorial_Fact_1
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule baseCase
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_baseCase(Object[] objects) {
        jeops_examples_factorial_Fact_1 = (jeops.examples.factorial.Fact) objects[0];
    }

    /**
     * Precondition 0 of rule baseCase.<p>
     * The original expression was:<br>
     * <code>!f.isOk()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean baseCase_prec_0() {
        return (!jeops_examples_factorial_Fact_1.isOk());
    }

    /**
     * Precondition 1 of rule baseCase.<p>
     * The original expression was:<br>
     * <code>f.getN() <= 1</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean baseCase_prec_1() {
        return (jeops_examples_factorial_Fact_1.getN() <= 1);
    }

    /**
     * Checks whether some preconditions of rule baseCase is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean baseCase_prec(int index) {
        switch (index) {
            case 0: return baseCase_prec_0();
            case 1: return baseCase_prec_1();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_baseCase(int declIndex) {
        setLocalDeclarations_baseCase(declIndex);
        switch (declIndex) {
            case 0:
                if (!baseCase_prec_0()) return false;
                if (!baseCase_prec_1()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule baseCase
     */
    public void baseCase() {
			jeops_examples_factorial_Fact_1.setResult(1);
			modified(jeops_examples_factorial_Fact_1);
	    }




    /**
     * Returns the name of the rules in this class file.
     *
     * @return the name of the rules in this class file.
     */
    public String[] getRuleNames() {
        int numberOfRules = 3;
    	String[] result = new String[numberOfRules];
        result[0] = "goDown";
        result[1] = "goUp";
        result[2] = "baseCase";
        return result;
    }

    /**
     * Returns the number of declarations of the rules in this class file.
     *
     * @return the number of declarations  of the rules in this class file.
     */
    public int[] getNumberOfDeclarations() {
        int numberOfRules = 3;
    	int[] result = new int[numberOfRules];
        result[0] = 1;
        result[1] = 2;
        result[2] = 1;
        return result;
    }

    /**
     * Returns the number of local declarations of the rules in this class file.
     *
     * @return the number of local declarations  of the rules in this class file.
     */
    public int[] getNumberOfLocalDeclarations() {
        int numberOfRules = 0;
    	int[] result = new int[numberOfRules];
        return result;
    }

    /**
     * Returns the number of preconditions of the rules in this class file.
     *
     * @return the number of preconditions  of the rules in this class file.
     */
    public int[] getNumberOfPreconditions() {
        int numberOfRules = 3;
    	int[] result = new int[numberOfRules];
        result[0] = 3;
        result[1] = 4;
        result[2] = 2;
        return result;
    }

    /**
     * Checks whether some preconditions of some rule is satisfied.
     *
     * @param ruleIndex the index of the rule to be checked
     * @param precIndex the index of the precondition to be checked
     * @return <code>true</code> if the corresponding precondition for the
     *          given rule is satisfied. <code>false</code> otherwise.
     */
    public boolean checkPrecondition(int ruleIndex, int precIndex) {
        switch (ruleIndex) {
            case 0: return goDown_prec(precIndex);
            case 1: return goUp_prec(precIndex);
            case 2: return baseCase_prec(precIndex);
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference only the elements declared up to the given index
     * are true.
     *
     * @param ruleIndex the index of the rule to be checked
     * @param declIndex the index of the declaration to be checked
     * @return <code>true</code> if all the preconditions of a rule which
     *          reference only the elements declared up to the given index
     *          are satisfied; <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return checkPrecForDeclaration_goDown(declIndex);
            case 1: return checkPrecForDeclaration_goUp(declIndex);
            case 2: return checkPrecForDeclaration_baseCase(declIndex);
            default: return false;
        }
    }

    /**
     * Returns the class name of an object declared in a rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration
     * @return the class name of the declared object.
     */
    public String getDeclaredClassName(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return getDeclaredClassName_goDown(declIndex);
            case 1: return getDeclaredClassName_goUp(declIndex);
            case 2: return getDeclaredClassName_baseCase(declIndex);
            default: return null;
        }
    }

    /**
     * Returns the class of an object declared in a rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration
     * @return the class of the declared object.
     */
    public Class getDeclaredClass(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return getDeclaredClass_goDown(declIndex);
            case 1: return getDeclaredClass_goUp(declIndex);
            case 2: return getDeclaredClass_baseCase(declIndex);
            default: return null;
        }
    }

    /**
     * Fires one of the rules in this rule base.
     *
     * @param ruleIndex the index of the rule to be fired
     */
    protected void internalFireRule(int ruleIndex) {
        switch (ruleIndex) {
            case 0: goDown(); break;
            case 1: goUp(); break;
            case 2: baseCase(); break;
        }
    }

    /**
     * Returns the number of rules.
     *
     * @return the number of rules.
     */
    public int getNumberOfRules() {
        return 3;
    }

    /**
     * Returns a table that maps each declared identifier of a rule
     * into its corresponding value.
     *
     * @param ruleIndex the index of the rule
     * @return a table that maps each declared identifier of a rule
     *          into its corresponding value.
     */
    public java.util.Hashtable getInternalRuleMapping(int ruleIndex) {
        switch (ruleIndex) {
            case 0: return getMapRulegoDown();
            case 1: return getMapRulegoUp();
            case 2: return getMapRulebaseCase();
            default: return new java.util.Hashtable();
        }
    }

    /**
     * Sets an object that represents a declaration of some rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration in the rule.
     * @param value the value of the object being set.
     */
    public void setObject(int ruleIndex, int declIndex, Object value) {
        switch (ruleIndex) {
            case 0: setObject_goDown(declIndex, value); break;
            case 1: setObject_goUp(declIndex, value); break;
            case 2: setObject_baseCase(declIndex, value); break;
        }
    }

    /**
     * Returns an object that represents a declaration of some rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration in the rule.
     * @return the value of the corresponding object.
     */
    public Object getObject(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return getObject_goDown(declIndex);
            case 1: return getObject_goUp(declIndex);
            case 2: return getObject_baseCase(declIndex);
            default: return null;
        }
    }

    /**
     * Returns a dependency table between the local declarations and the
     * regular ones.
     *
     * @param ruleIndex the index of the rule
     * @return a two-dimensional array with the dependencies between the
     *          local declarations and the regular ones.
     */
    public Object[][] getLocalDeclarationDependency(int ruleIndex) {
        switch (ruleIndex) {
            case 0: return getLocalDeclarationDependency_goDown();
            case 1: return getLocalDeclarationDependency_goUp();
            case 2: return getLocalDeclarationDependency_baseCase();
            default: return null;
        }
    }
    /**
     * Defines all variables bound to the local declarations of
     * some rule.
     *
     * @param ruleIndex the index of the rule
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations.
     *          of some rule.
     */
    protected void setLocalObjects(int ruleIndex, Object[][] objects) {
        switch (ruleIndex) {
            case 0: setLocalObjects_goDown(objects); break;
            case 1: setLocalObjects_goUp(objects); break;
            case 2: setLocalObjects_baseCase(objects); break;
        }
    }
    /**
     * Returns all variables bound to the declarations of
     * some rule.
     *
     * @param ruleIndex the index of the rule
     * @return an object array of the variables bound to the
     *          declarations of some rule.
     */
    protected Object[] getObjects(int ruleIndex) {
        switch (ruleIndex) {
            case 0: return getObjects_goDown();
            case 1: return getObjects_goUp();
            case 2: return getObjects_baseCase();
            default: return null;
        }
    }
    /**
     * Defines all variables bound to the declarations of
     * some rule.
     *
     * @param ruleIndex the index of the rule
     * @param objects an object array of the variables bound to the
     *          declarations of some rule.
     */
    protected void setObjects(int ruleIndex, Object[] objects) {
        switch (ruleIndex) {
            case 0: setObjects_goDown(objects); break;
            case 1: setObjects_goUp(objects); break;
            case 2: setObjects_baseCase(objects); break;
        }
    }

    /**
     * Returns a mapping from the classes of the objects declared in the
     * rules to the number of occurrences of each one.
     *
     * @return a mapping from the classes of the objects declared in the
     *          rules to the number of occurrences of each one.
     */
    public java.util.Hashtable getClassDeclarationCount() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("jeops.examples.factorial.Fact", new Integer(2));
        return result;
    }

    /*
     * The variables declared in the preconditions.
     */
    jeops.examples.factorial.Fact jeops_examples_factorial_Fact_1;
    jeops.examples.factorial.Fact jeops_examples_factorial_Fact_2;

    /**
     * Constant used to aviod the creation of several empty arrays.
     */
    private static final Object[] EMPTY_OBJECT_ARRAY = new Object[0];

    /**
     * Constant used to aviod the creation of several empty double arrays.
     */
    private static final Object[][] EMPTY_OBJECT_DOUBLE_ARRAY = new Object[0][0];

    /**
     * Constant used to aviod the creation of several empty vectors.
     */
    private static final java.util.Vector EMPTY_VECTOR = new java.util.Vector();

    /**
     * Class constructor.
     *
     * @param objectBase an object base over with this rule base will work.
     * @param knowledgeBase the knowledge base that contains this rule base.
     */
    public Jeops_RuleBase_FactBase(jeops.ObjectBase objectBase,
                                jeops.AbstractKnowledgeBase knowledgeBase) {
        super(objectBase, knowledgeBase);
    }

}

/**
 * Knowledge base created by JEOPS from file ./src/jeops/examples/factorial/FactBase.rules
 *
 * @version Apr 23, 2016
 */
public class FactBase extends jeops.AbstractKnowledgeBase {

    /**
     * Creates a new knowledge base with the specified conflict set with the
     * desired conflict resolution policy.
     *
     * @param conflictSet a conflict set with the desired resolution policy
     */
    public FactBase(jeops.conflict.ConflictSet conflictSet) {
        super(conflictSet);
        jeops.AbstractRuleBase ruleBase;
        jeops.ObjectBase objectBase = getObjectBase();
        ruleBase = new Jeops_RuleBase_FactBase(objectBase, this);
        setRuleBase(ruleBase);
    }

    /**
     * Creates a new knowledge base, using the default conflict resolution
     * policy.
     */
    public FactBase() {
        this(new jeops.conflict.DefaultConflictSet());
    }

}
