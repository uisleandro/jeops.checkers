package jeops.examples.auction;

public class TestAuction {

	public static void main(java.lang.String[] args) {
		int price = Integer.parseInt(args[0]);
		int money = Integer.parseInt(args[1]);
		AuctionBase ab = new AuctionBase();
		Auction a = new Auction(price, money);
		ab.addRuleFireListener(new jeops.RuleFireListener() {
			public void ruleFiring(jeops.RuleEvent e) {
			    int i = e.getRuleIndex();
			    String name = e.getKnowledgeBase().getRuleBase().getRuleNames()[i];
			    System.out.println("Firing rule " + name);
			}
			public void ruleFired(jeops.RuleEvent e) {}
		    });
		ab.tell(a);
		ab.run();
	}
}
