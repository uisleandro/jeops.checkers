package jeops.examples.auction;

public class Auction {

	public int times = 3;
	public int money;
	public int price;
	public int decrease;
	public int increase;
	public int auctionprice;
	public int n = 0;

	public Auction(int times) {
		this.times = times;
	}
	
	public Auction(int price, int money) {
		this.price = price;
		this.money = money;	
	}
	
	public Auction(int price, int decrease, int money, int increase) {
		this.price = price;
		this.decrease = decrease;
		this.money = money;
		this.increase = increase;
	}
	
	public int getTimes() {
		return times;
	}

	public int getMoney() {
		return money;
	}
	
	public int getPrice() {
		return price;
	}
	
	public int getDecrease() {
		return decrease;
	}
	
	public int getIncrease() {
		return increase;
	}
	
	public int getAuctionPrice() {
		return auctionprice;	
	}
	
	public int getN() {
		return n;
	}
	
	public void setTimes(int times) {
		this.times = times;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
	public void setMoney(int money) {
		this.money = money;
	}
	
	public void setN(int n) {
		this.n = n;
	}
	
	public void setAuctionPrice(int auctionprice) {
		this.auctionprice = auctionprice;
	}
}
