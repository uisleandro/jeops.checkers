package jeops.examples.auction;

  class Jeops_RuleBase_AuctionBase extends jeops.AbstractRuleBase {

  
    /**
     * Sets the local declaration for a given declaration index.
     * As there are no local declarations for this rule, this
     * method does nothing.
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_Lose(int index) {
    }

    /**
     * Returns a dependency table between the local declarations 
     * and the regular ones for rule Lose. As here are
     * no local declarations for this rule, this method returns an
     * empty array.
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_Lose() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule Lose
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_Lose(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule Lose
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule Lose
     */
    private java.util.Hashtable getMapRuleLose() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("a", jeops_examples_auction_Auction_1);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule Lose.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_Lose(int index) {
        switch (index) {
            case 0: return "jeops.examples.auction.Auction";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule Lose.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_Lose(int index) {
        switch (index) {
            case 0: return jeops.examples.auction.Auction.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule Lose.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_Lose(int index, Object value) {
        switch (index) {
            case 0: this.jeops_examples_auction_Auction_1 = (jeops.examples.auction.Auction) value; break;
        }
    }

    /**
     * Returns an object declared in the rule Lose.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_Lose(int index) {
        switch (index) {
            case 0: return jeops_examples_auction_Auction_1;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule Lose
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_Lose() {
        return new Object[] {
                            jeops_examples_auction_Auction_1
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule Lose
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_Lose(Object[] objects) {
        jeops_examples_auction_Auction_1 = (jeops.examples.auction.Auction) objects[0];
    }

    /**
     * Precondition 0 of rule Lose.<p>
     * The original expression was:<br>
     * <code>a.getPrice() > a.getMoney()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean Lose_prec_0() {
        return (jeops_examples_auction_Auction_1.getPrice() > jeops_examples_auction_Auction_1.getMoney());
    }

    /**
     * Checks whether some preconditions of rule Lose is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean Lose_prec(int index) {
        switch (index) {
            case 0: return Lose_prec_0();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_Lose(int declIndex) {
        setLocalDeclarations_Lose(declIndex);
        switch (declIndex) {
            case 0:
                if (!Lose_prec_0()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule Lose
     */
    public void Lose() {
      System.out.println("Saler want to sale it by $" + jeops_examples_auction_Auction_1.getPrice() + ".");
      System.out.println("Buyer want to buy it by $" + jeops_examples_auction_Auction_1.getMoney() + ".");
      System.out.println("So, this auction is not success!!");
      System.out.println();
      int a1 = jeops_examples_auction_Auction_1.getPrice()-jeops_examples_auction_Auction_1.getDecrease();
      int a2 = jeops_examples_auction_Auction_1.getMoney()+jeops_examples_auction_Auction_1.getIncrease();
      int n = jeops_examples_auction_Auction_1.getN()+1;
      jeops_examples_auction_Auction_1.setPrice(a1);
      jeops_examples_auction_Auction_1.setMoney(a2);
      jeops_examples_auction_Auction_1.setN(n);
      modified(jeops_examples_auction_Auction_1);
      }



  
    /**
     * Sets the local declaration for a given declaration index.
     * As there are no local declarations for this rule, this
     * method does nothing.
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_Get(int index) {
    }

    /**
     * Returns a dependency table between the local declarations 
     * and the regular ones for rule Get. As here are
     * no local declarations for this rule, this method returns an
     * empty array.
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_Get() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule Get
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_Get(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule Get
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule Get
     */
    private java.util.Hashtable getMapRuleGet() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("a", jeops_examples_auction_Auction_1);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule Get.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_Get(int index) {
        switch (index) {
            case 0: return "jeops.examples.auction.Auction";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule Get.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_Get(int index) {
        switch (index) {
            case 0: return jeops.examples.auction.Auction.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule Get.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_Get(int index, Object value) {
        switch (index) {
            case 0: this.jeops_examples_auction_Auction_1 = (jeops.examples.auction.Auction) value; break;
        }
    }

    /**
     * Returns an object declared in the rule Get.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_Get(int index) {
        switch (index) {
            case 0: return jeops_examples_auction_Auction_1;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule Get
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_Get() {
        return new Object[] {
                            jeops_examples_auction_Auction_1
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule Get
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_Get(Object[] objects) {
        jeops_examples_auction_Auction_1 = (jeops.examples.auction.Auction) objects[0];
    }

    /**
     * Precondition 0 of rule Get.<p>
     * The original expression was:<br>
     * <code>a.getPrice() < a.getMoney()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean Get_prec_0() {
        return (jeops_examples_auction_Auction_1.getPrice() < jeops_examples_auction_Auction_1.getMoney());
    }

    /**
     * Checks whether some preconditions of rule Get is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean Get_prec(int index) {
        switch (index) {
            case 0: return Get_prec_0();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_Get(int declIndex) {
        setLocalDeclarations_Get(declIndex);
        switch (declIndex) {
            case 0:
                if (!Get_prec_0()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule Get
     */
    public void Get() {
      jeops_examples_auction_Auction_1.setAuctionPrice(jeops_examples_auction_Auction_1.getMoney());
      modified(jeops_examples_auction_Auction_1);
      System.out.println("WA!! This auction is success!!");
      System.out.println("Buyer pay $" + jeops_examples_auction_Auction_1.getAuctionPrice() + " to buy the thing!!");
      System.out.println("He auction " + jeops_examples_auction_Auction_1.getN() + " times!!");
      System.out.println("HA!! HA!! Good boy!!");
      System.out.println();
      }



  
    /**
     * Sets the local declaration for a given declaration index.
     * As there are no local declarations for this rule, this
     * method does nothing.
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_Draw(int index) {
    }

    /**
     * Returns a dependency table between the local declarations 
     * and the regular ones for rule Draw. As here are
     * no local declarations for this rule, this method returns an
     * empty array.
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_Draw() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule Draw
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_Draw(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule Draw
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule Draw
     */
    private java.util.Hashtable getMapRuleDraw() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("a", jeops_examples_auction_Auction_1);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule Draw.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_Draw(int index) {
        switch (index) {
            case 0: return "jeops.examples.auction.Auction";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule Draw.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_Draw(int index) {
        switch (index) {
            case 0: return jeops.examples.auction.Auction.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule Draw.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_Draw(int index, Object value) {
        switch (index) {
            case 0: this.jeops_examples_auction_Auction_1 = (jeops.examples.auction.Auction) value; break;
        }
    }

    /**
     * Returns an object declared in the rule Draw.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_Draw(int index) {
        switch (index) {
            case 0: return jeops_examples_auction_Auction_1;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule Draw
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_Draw() {
        return new Object[] {
                            jeops_examples_auction_Auction_1
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule Draw
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_Draw(Object[] objects) {
        jeops_examples_auction_Auction_1 = (jeops.examples.auction.Auction) objects[0];
    }

    /**
     * Precondition 0 of rule Draw.<p>
     * The original expression was:<br>
     * <code>a.getPrice() == a.getMoney()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean Draw_prec_0() {
        return (jeops_examples_auction_Auction_1.getPrice() == jeops_examples_auction_Auction_1.getMoney());
    }

    /**
     * Checks whether some preconditions of rule Draw is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean Draw_prec(int index) {
        switch (index) {
            case 0: return Draw_prec_0();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_Draw(int declIndex) {
        setLocalDeclarations_Draw(declIndex);
        switch (declIndex) {
            case 0:
                if (!Draw_prec_0()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule Draw
     */
    public void Draw() {
      jeops_examples_auction_Auction_1.setAuctionPrice(jeops_examples_auction_Auction_1.getMoney());
      System.out.println("WA!! This auction is success!!");
      System.out.println("Buyer pay $" + jeops_examples_auction_Auction_1.getAuctionPrice() + "to buy the thing!!");
      System.out.println("He auction " + jeops_examples_auction_Auction_1.getN() + " times!!");
      System.out.println("HA!! HA!! Good boy!!");
      System.out.println();
      }



    /**
     * Returns the name of the rules in this class file.
     *
     * @return the name of the rules in this class file.
     */
    public String[] getRuleNames() {
        int numberOfRules = 3;
    	String[] result = new String[numberOfRules];
        result[0] = "Lose";
        result[1] = "Get";
        result[2] = "Draw";
        return result;
    }

    /**
     * Returns the number of declarations of the rules in this class file.
     *
     * @return the number of declarations  of the rules in this class file.
     */
    public int[] getNumberOfDeclarations() {
        int numberOfRules = 3;
    	int[] result = new int[numberOfRules];
        result[0] = 1;
        result[1] = 1;
        result[2] = 1;
        return result;
    }

    /**
     * Returns the number of local declarations of the rules in this class file.
     *
     * @return the number of local declarations  of the rules in this class file.
     */
    public int[] getNumberOfLocalDeclarations() {
        int numberOfRules = 0;
    	int[] result = new int[numberOfRules];
        return result;
    }

    /**
     * Returns the number of preconditions of the rules in this class file.
     *
     * @return the number of preconditions  of the rules in this class file.
     */
    public int[] getNumberOfPreconditions() {
        int numberOfRules = 3;
    	int[] result = new int[numberOfRules];
        result[0] = 1;
        result[1] = 1;
        result[2] = 1;
        return result;
    }

    /**
     * Checks whether some preconditions of some rule is satisfied.
     *
     * @param ruleIndex the index of the rule to be checked
     * @param precIndex the index of the precondition to be checked
     * @return <code>true</code> if the corresponding precondition for the
     *          given rule is satisfied. <code>false</code> otherwise.
     */
    public boolean checkPrecondition(int ruleIndex, int precIndex) {
        switch (ruleIndex) {
            case 0: return Lose_prec(precIndex);
            case 1: return Get_prec(precIndex);
            case 2: return Draw_prec(precIndex);
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference only the elements declared up to the given index
     * are true.
     *
     * @param ruleIndex the index of the rule to be checked
     * @param declIndex the index of the declaration to be checked
     * @return <code>true</code> if all the preconditions of a rule which
     *          reference only the elements declared up to the given index
     *          are satisfied; <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return checkPrecForDeclaration_Lose(declIndex);
            case 1: return checkPrecForDeclaration_Get(declIndex);
            case 2: return checkPrecForDeclaration_Draw(declIndex);
            default: return false;
        }
    }

    /**
     * Returns the class name of an object declared in a rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration
     * @return the class name of the declared object.
     */
    public String getDeclaredClassName(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return getDeclaredClassName_Lose(declIndex);
            case 1: return getDeclaredClassName_Get(declIndex);
            case 2: return getDeclaredClassName_Draw(declIndex);
            default: return null;
        }
    }

    /**
     * Returns the class of an object declared in a rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration
     * @return the class of the declared object.
     */
    public Class getDeclaredClass(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return getDeclaredClass_Lose(declIndex);
            case 1: return getDeclaredClass_Get(declIndex);
            case 2: return getDeclaredClass_Draw(declIndex);
            default: return null;
        }
    }

    /**
     * Fires one of the rules in this rule base.
     *
     * @param ruleIndex the index of the rule to be fired
     */
    protected void internalFireRule(int ruleIndex) {
        switch (ruleIndex) {
            case 0: Lose(); break;
            case 1: Get(); break;
            case 2: Draw(); break;
        }
    }

    /**
     * Returns the number of rules.
     *
     * @return the number of rules.
     */
    public int getNumberOfRules() {
        return 3;
    }

    /**
     * Returns a table that maps each declared identifier of a rule
     * into its corresponding value.
     *
     * @param ruleIndex the index of the rule
     * @return a table that maps each declared identifier of a rule
     *          into its corresponding value.
     */
    public java.util.Hashtable getInternalRuleMapping(int ruleIndex) {
        switch (ruleIndex) {
            case 0: return getMapRuleLose();
            case 1: return getMapRuleGet();
            case 2: return getMapRuleDraw();
            default: return new java.util.Hashtable();
        }
    }

    /**
     * Sets an object that represents a declaration of some rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration in the rule.
     * @param value the value of the object being set.
     */
    public void setObject(int ruleIndex, int declIndex, Object value) {
        switch (ruleIndex) {
            case 0: setObject_Lose(declIndex, value); break;
            case 1: setObject_Get(declIndex, value); break;
            case 2: setObject_Draw(declIndex, value); break;
        }
    }

    /**
     * Returns an object that represents a declaration of some rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration in the rule.
     * @return the value of the corresponding object.
     */
    public Object getObject(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return getObject_Lose(declIndex);
            case 1: return getObject_Get(declIndex);
            case 2: return getObject_Draw(declIndex);
            default: return null;
        }
    }

    /**
     * Returns a dependency table between the local declarations and the
     * regular ones.
     *
     * @param ruleIndex the index of the rule
     * @return a two-dimensional array with the dependencies between the
     *          local declarations and the regular ones.
     */
    public Object[][] getLocalDeclarationDependency(int ruleIndex) {
        switch (ruleIndex) {
            case 0: return getLocalDeclarationDependency_Lose();
            case 1: return getLocalDeclarationDependency_Get();
            case 2: return getLocalDeclarationDependency_Draw();
            default: return null;
        }
    }
    /**
     * Defines all variables bound to the local declarations of
     * some rule.
     *
     * @param ruleIndex the index of the rule
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations.
     *          of some rule.
     */
    protected void setLocalObjects(int ruleIndex, Object[][] objects) {
        switch (ruleIndex) {
            case 0: setLocalObjects_Lose(objects); break;
            case 1: setLocalObjects_Get(objects); break;
            case 2: setLocalObjects_Draw(objects); break;
        }
    }
    /**
     * Returns all variables bound to the declarations of
     * some rule.
     *
     * @param ruleIndex the index of the rule
     * @return an object array of the variables bound to the
     *          declarations of some rule.
     */
    protected Object[] getObjects(int ruleIndex) {
        switch (ruleIndex) {
            case 0: return getObjects_Lose();
            case 1: return getObjects_Get();
            case 2: return getObjects_Draw();
            default: return null;
        }
    }
    /**
     * Defines all variables bound to the declarations of
     * some rule.
     *
     * @param ruleIndex the index of the rule
     * @param objects an object array of the variables bound to the
     *          declarations of some rule.
     */
    protected void setObjects(int ruleIndex, Object[] objects) {
        switch (ruleIndex) {
            case 0: setObjects_Lose(objects); break;
            case 1: setObjects_Get(objects); break;
            case 2: setObjects_Draw(objects); break;
        }
    }

    /**
     * Returns a mapping from the classes of the objects declared in the
     * rules to the number of occurrences of each one.
     *
     * @return a mapping from the classes of the objects declared in the
     *          rules to the number of occurrences of each one.
     */
    public java.util.Hashtable getClassDeclarationCount() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("jeops.examples.auction.Auction", new Integer(1));
        return result;
    }

    /*
     * The variables declared in the preconditions.
     */
    jeops.examples.auction.Auction jeops_examples_auction_Auction_1;

    /**
     * Constant used to aviod the creation of several empty arrays.
     */
    private static final Object[] EMPTY_OBJECT_ARRAY = new Object[0];

    /**
     * Constant used to aviod the creation of several empty double arrays.
     */
    private static final Object[][] EMPTY_OBJECT_DOUBLE_ARRAY = new Object[0][0];

    /**
     * Constant used to aviod the creation of several empty vectors.
     */
    private static final java.util.Vector EMPTY_VECTOR = new java.util.Vector();

    /**
     * Class constructor.
     *
     * @param objectBase an object base over with this rule base will work.
     * @param knowledgeBase the knowledge base that contains this rule base.
     */
    public Jeops_RuleBase_AuctionBase(jeops.ObjectBase objectBase,
                                jeops.AbstractKnowledgeBase knowledgeBase) {
        super(objectBase, knowledgeBase);
    }

}
/**
 * Knowledge base created by JEOPS from file ./src/jeops/examples/auction/AuctionBase1.rules
 *
 * @version Apr 23, 2016
 */
public class AuctionBase extends jeops.AbstractKnowledgeBase {

    /**
     * Creates a new knowledge base with the specified conflict set with the
     * desired conflict resolution policy.
     *
     * @param conflictSet a conflict set with the desired resolution policy
     */
    public AuctionBase(jeops.conflict.ConflictSet conflictSet) {
        super(conflictSet);
        jeops.AbstractRuleBase ruleBase;
        jeops.ObjectBase objectBase = getObjectBase();
        ruleBase = new Jeops_RuleBase_AuctionBase(objectBase, this);
        setRuleBase(ruleBase);
    }

    /**
     * Creates a new knowledge base, using the default conflict resolution
     * policy.
     */
    public AuctionBase() {
        this(new jeops.conflict.DefaultConflictSet());
    }

}
