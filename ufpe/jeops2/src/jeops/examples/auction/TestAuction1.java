package jeops.examples.auction;

public class TestAuction1 {

	public static void main(java.lang.String[] args) {
		int price = Integer.parseInt(args[0]);
		int money = Integer.parseInt(args[2]);
		int decrease = Integer.parseInt(args[1]);
		int increase = Integer.parseInt(args[3]);
		AuctionBase ab = new AuctionBase();
		Auction a = new Auction(price, decrease, money, increase);
		ab.tell(a);
		ab.run();
	}
}
