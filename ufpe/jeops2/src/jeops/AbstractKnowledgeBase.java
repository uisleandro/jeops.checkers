/*
 * JEOPS - The Java Embedded Object Production System
 * Copyright (c) 2000   Carlos Figueira Filho
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: Carlos Figueira Filho (csff@cin.ufpe.br)
 */

package jeops;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import jeops.conflict.ConflictSet;
import jeops.conflict.ConflictSetElement;
import jeops.conflict.InternalConflictSetEvent;
import jeops.conflict.InternalConflictSetListener;
import jeops.conflict.NoMoreElementsException;

/**
 * The main class of JEOPS. This class models the knowledge the agent
 * has about the world. In it are stored the facts (objects) and
 * (production) rules that act on the first. This class must be subclassed
 * by a concrete knowledge base so that it is provided with the object
 * and rule base needed for work.
 *
 * @author Carlos Figueira Filho (<a href="mailto:csff@di.ufpe.br">csff@di.ufpe.br</a>)
 * @version 0.01  12 Mar 2000   Class adapted from previous version of JEOPS.
 */
public abstract class AbstractKnowledgeBase implements InternalConflictSetListener {

	/**
	 * The fact (object) base.
	 */
	private ObjectBase objectBase;

	/**
	 * The (production) rule base.
	 */
	private AbstractRuleBase ruleBase;
	
	/**
	 * The number of rules in the rule base.
	 */
	private int numberOfRules;

	/**
	 * The conflict set associated with this knowledge base.
	 */
	private ConflictSet conflictSet;

	/**
	 * The set of rule firing listeners.
	 */
	private Vector ruleFiringListeners;

	/**
	 * The set of conflict set listeners.
	 */
	private Vector conflictSetListeners;

	/**
	 * Indicates whether this knowledge base is a listener of its
	 * conflict set.
	 */
	private boolean isListener = false;

	/**
	 * Flag indicating whether there is any listener for rule events in
	 * this knowledge base.
	 */
	private boolean debug = false;

	/**
	 * Table that maps the objects that appeared as a local declaration
	 * in the objects that were used to create it.
	 */
	private Hashtable localDeclDependency;

	/**
	 * Creates a new knowledge base. The superclass must set the rule
	 * base of this kb prior to using it.
	 *
	 * @param conflictSet the conflict set associated with this knowledge base.
	 */
	public AbstractKnowledgeBase(ConflictSet conflictSet) {
		objectBase = new ObjectBase();
		setConflictSet(conflictSet);
		this.ruleFiringListeners = new Vector();
		this.conflictSetListeners = new Vector();
		this.localDeclDependency = new Hashtable();
	}

	/**
	 * Defines the (production) rule base of this knowledge base.
	 *
	 * @param ruleBase the rule base of this knowledge base.
	 */
	protected void setRuleBase(AbstractRuleBase ruleBase) {
		this.ruleBase = ruleBase;
		this.numberOfRules = ruleBase.getNumberOfRules();
	}

	/**
	 * Returns the (production) rule base of this knowledge base.
	 *
	 * @return the (production) rule base of this knowledge base.
	 */
	public AbstractRuleBase getRuleBase() {
		return ruleBase;
	}

	/**
	 * Returns the fact (object) base.
	 *
	 * @return the fact (object) base.
	 */
	protected ObjectBase getObjectBase() {
		return objectBase;
	}

	/**
	 * Adds the specified rule fire listener to receive events from this
	 * knowledge base.
	 *
	 * @param l the rule base listener
	 */
	public void addRuleFireListener(RuleFireListener l) {
		if (!ruleFiringListeners.contains(l)) {
			ruleFiringListeners.addElement(l);
			debug = true;
		}
	}

	/**
	 * Removes the specified rule fire listener so that it no longer
	 * receives rule fire events from this knowledge base.
	 *
	 * @param l the rule fire listener
	 */
	public void removeRuleFireListener(RuleFireListener l) {
		ruleFiringListeners.removeElement(l);
		if (ruleFiringListeners.size() == 0 &&
					conflictSetListeners.size() == 0) {
			debug = false;
		}
	}

	/**
	 * Adds the specified conflict set listener to receive events from
	 * this knowledge base.
	 *
	 * @param l the conflict set listener
	 */
	public void addConflictSetListener(RuleFireListener l) {
		if (!isListener) {
			conflictSet.addInternalConflictSetListener(this);
			isListener = true;
		}
		if (!conflictSetListeners.contains(l)) {
			conflictSetListeners.addElement(l);
			debug = true;
		}
	}

	/**
	 * Removes the specified conflict set listener so that it no longer
	 * receives conflict set events from this knowledge base.
	 *
	 * @param l the conflict set listener
	 */
	public void removeConflictSetListener(RuleFireListener l) {
		conflictSetListeners.removeElement(l);
		if (conflictSetListeners.size() == 0) {
			debug = false;
			conflictSet.removeInternalConflictSetListener(this);
			isListener = false;
		} else {
			debug = ruleFiringListeners.size() != 0;
		}
	}

	/**
	 * Dispatch a rule fired event to all registered listeners.
	 */
	private void fireRuleFiredEvent(RuleEvent e) {
		for (int i = 0; i < ruleFiringListeners.size(); i++) {
			((RuleFireListener) ruleFiringListeners.elementAt(i)).ruleFired(e);
		}
	}

	/**
	 * Dispatch a rule firing event to all registered listeners.
	 */
	private void fireRuleFiringEvent(RuleEvent e) {
		for (int i = 0; i < ruleFiringListeners.size(); i++) {
			((RuleFireListener) ruleFiringListeners.elementAt(i)).ruleFiring(e);
		}
	}

	/**
	 * Invoked when an element is added to the conflict set.
	 *
	 * @param e the conflict set event.
	 */
	public void internalElementAdded(InternalConflictSetEvent e) {
		fireElementAddedEvent(new ConflictSetEvent(this, e.getElement()));
	}

	/**
	 * Invoked when an element is removed from the conflict set.
	 *
	 * @param e the conflict set event.
	 */
	public void internalElementRemoved(InternalConflictSetEvent e) {
		fireElementRemovedEvent(new ConflictSetEvent(this, e.getElement()));
	}

	/**
	 * Dispatch an element added event to all registered listeners.
	 */
	private void fireElementAddedEvent(ConflictSetEvent e) {
		for (int i = 0; i < conflictSetListeners.size(); i++) {
			((ConflictSetListener) conflictSetListeners.elementAt(i)).elementAdded(e);
		}
	}

	/**
	 * Dispatch an element removed event to all registered listeners.
	 */
	private void fireElementRemovedEvent(ConflictSetEvent e) {
		for (int i = 0; i < conflictSetListeners.size(); i++) {
			((ConflictSetListener) conflictSetListeners.elementAt(i)).elementRemoved(e);
		}
	}

	/**
	 * Adds a new entry to the table that maps the objects bound to local
	 * declaration to the objects bound to the declarations that were used
	 * to create it.
	 *
	 * @param local the object bound to a local declaration.
	 * @param regular an object bound to a regular declaration that was
	 *          used to create the former.
	 */
	private void addToLocalDeclDependency(Object local, Object regular) {
		Vector v = (Vector) localDeclDependency.get(local);
		if (v == null) {
			v = new Vector();
			localDeclDependency.put(local, v);
		}
		if (!v.contains(regular)) {
			v.addElement(regular);
		}
	}

	/**
	 * Inserts a new object in this knowledge base.
	 *
	 * @param obj the object being inserted.
	 */
	public void tell(Object obj) {

		// First of all, putting it into the object base
		objectBase.tell(obj);

		// Now, trying to use it to instantiate new rules.
		int ruleNumber = -1, i;
		Class objClass = obj.getClass();
		String objClassName = objClass.getName();
		int[] numberOfAllDeclarations = ruleBase.getNumberOfDeclarations();

		for (ruleNumber = 0; ruleNumber < numberOfRules; ruleNumber++) {
			int numberOfDeclarations = numberOfAllDeclarations[ruleNumber];
			for (int newObject = 0; newObject < numberOfDeclarations; newObject++) {
				int[] sizes = new int[numberOfDeclarations];
				boolean noObjects = false;
				for (i = 0; !noObjects && i < sizes.length; i++) {
					Class declClass = ruleBase.getDeclaredClass(ruleNumber, i);
					String declClassName = ruleBase.getDeclaredClassName(ruleNumber, i);
					if (i == newObject) {
						sizes[i] = 1;
						if (!declClass.isAssignableFrom(objClass)) {
							noObjects = true;
						}
					} else {
						sizes[i] = objectBase.objects(declClassName).size();
						if (sizes[i] == 0) { // No objects of a needed class
							noObjects = true;
						}
					}
				}
				if (noObjects) {
					continue;
				}
				
				int[] number = new int[numberOfDeclarations];

				boolean end = false;
				int declNumber = 0;
				while (!end) {
					String className = ruleBase.getDeclaredClassName(ruleNumber, declNumber);
					Object object = null;
					if (declNumber == newObject) {
						object = obj;
					} else {
						Vector classObjects = objectBase.objects(className);
						object = classObjects.elementAt(number[declNumber]);
					}
					ruleBase.setObject(ruleNumber, declNumber, object);

					boolean precOk;
					try {
						precOk = ruleBase.checkPrecForDeclaration(ruleNumber, declNumber);
					} catch (Exception e) {
						precOk = false;
					}
					if (precOk) {
						
						// All preconditions up to declNumber are satisfied.

						if (declNumber != numberOfDeclarations - 1) {
							
							// If it's not the last declaration, we still
							// have to check the remaining ones.
							number[++declNumber] = 0;
						} else {

							// If it's the last one, these objects make the
							// current rule fireable. Let's then insert it into
							// the conflict set.
							ConflictSetElement e;
							e = new ConflictSetElement(ruleNumber,
								ruleBase.getObjects(ruleNumber),
								ruleBase.getLocalDeclarationDependency(ruleNumber));
							conflictSet.insertElement(e);

							// Advancing to the next candidates
							number[declNumber]++;
							while (declNumber >= 0 && number[declNumber] == sizes[declNumber]) {
								number[declNumber] = 0;
								declNumber--;
								if (declNumber >= 0) {
									number[declNumber]++;
								}
							}
						}
					} else {

						// Let's store information about the local declaration dependency.
						Object[][] localDep = ruleBase.getLocalDeclarationDependency(ruleNumber);
						for (int j = 0; j < localDep.length; j++) {
							Object[] line = localDep[j];
							if (line[0] != null) {
								for (int k = 1; k < line.length; k++) {
									addToLocalDeclDependency(line[0], line[k]);
								}
							}
						}

						// Not this time, advancing to the next candidates
						number[declNumber]++;
						while (declNumber >= 0 && number[declNumber] == sizes[declNumber]) {
							number[declNumber] = 0;
							declNumber--;
							if (declNumber >= 0) {
								number[declNumber]++;
							}
						}
					}
					end = (declNumber < 0) || (number[0] >= sizes[0]);
				}
			}
		}
	}

	/**
	 * Remove all facts (objects) of the object base.
	 */
	public void flush() {
		objectBase.flush();
		conflictSet.flush();
	}

	/**
	 * Returns the objects of a given class.
	 *
	 * @param className the name of the class.
	 */
	public Vector objects(String className) {
		return objectBase.objects(className);
	}

	/**
	 * Removes a given object of this knowledge base.
	 *
	 * @param obj the object being removed.
	 * @return <code>true</code> if the given object belonged to this base;
	 *          <code>false</code> otherwise.
	 */
	public boolean retract(Object obj) {
		boolean result = objectBase.remove(obj);
		conflictSet.removeElementsWith(obj);
		return result;
	}

	/**
	 * Tells this base that an object was modified, so that the rules
	 * can be retested against the object.
	 *
	 * @param obj the object that was modified.
	 */
	protected void modified(Object obj) {
		boolean totell = retract(obj);

		// Let's see if this object caused the modification of some
		// other object in the conflict set, so that we must recheck
		// it to see if the rule is still fireable.
		Vector otherModifieds = conflictSet.getModifiedObjects(obj);
		
		// And let's check to see if the object could be used to modify
		// some element that is not in the conflict set.
		Vector otherModifieds2 = (Vector) localDeclDependency.get(obj);
		if (otherModifieds2 != null) {
			for (int i = 0; i < otherModifieds2.size(); i++) {
				Object aux = otherModifieds2.elementAt(i);
				if (!otherModifieds.contains(aux)) {
					otherModifieds.addElement(aux);
				}
			}
		}

		// If the transitively modified object is not in the object base,
		// there's no reason to put it back.
		boolean[] toBe_asserted = new boolean[otherModifieds.size()];

		for (int i = 0; i < otherModifieds.size(); i++) {
			toBe_asserted[i] = retract(otherModifieds.elementAt(i));
		}
		for (int i = 0; i < otherModifieds.size(); i++) {
			if (toBe_asserted[i]) {
				tell(otherModifieds.elementAt(i));
			}
		}
		if (totell) {
			tell(obj);
		}
	}

	/**
	 * Fires the rules in the rule base with the objects present in the
	 *    object base, until no rule is fireable anymore.
	 */
	public void run() {
		while (!conflictSet.isEmpty()) {
			try {
				ConflictSetElement element = conflictSet.nextElement();
				int ruleIndex = element.getRuleIndex();
				ruleBase.setRuleIndex(ruleIndex);
				Object[] objects = element.getObjects();
				ruleBase.setObjects(ruleIndex, objects);
				Object[][] localObjects = element.getLocalObjects();
				ruleBase.setLocalObjects(ruleIndex, localObjects);
				Hashtable map = null;
				if (debug) {
					map = ruleBase.getInternalRuleMapping(ruleIndex);
					fireRuleFiringEvent(new RuleEvent(this, ruleIndex, map));
				}
				ruleBase.internalFireRule(ruleIndex);
				if (debug) {
					fireRuleFiredEvent(new RuleEvent(this, ruleIndex, map));
				}
			} catch (NoMoreElementsException e) {} // It won't happen.
		}
	}

	/**
	 * Defines a conflict set policy for this knowledge base.
	 *
	 * @param conflictSet the new conflict set that implements its
	 *          associated conflict resolution policy.
	 */
	public void setConflictSet(ConflictSet conflictSet) {
		this.conflictSet = conflictSet;
	}
}
