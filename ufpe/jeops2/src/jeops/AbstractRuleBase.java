/*
 * JEOPS - The Java Embedded Object Production System
 * Copyright (c) 2000   Carlos Figueira Filho
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: Carlos Figueira Filho (csff@cin.ufpe.br)
 */

package jeops;

import java.util.Hashtable;
import java.util.Vector;
import jeops.conflict.ConflictSet;

/**
 * A generic rule base of Jeops. Objects of this class represent the pairs
 * that it can be treated generically by any rule base.
 *
 * @version 0.03  07.04.2000   Method for retrieving an array of dependencies
 *                             between local declarations and declarations.
 *                             The two-dimensional array will have as many
 *                             rows as there are local declarations; each
 *                             row will have the object bound to the local
 *                             declaration in the first column, and the
 *                             remaining columns will have the objects bound
 *                             to the regular declaration that created the
 *                             locally declared object.
 * @author Carlos Figueira Filho (<a href="mailto:csff@di.ufpe.br">csff@di.ufpe.br</a>)
 * @history 0.01  09.03.2000
 * @history 0.02  01.04.2000   Methods for getting and setting all objects
 *                             in a single time. It's used to retrieve the
 *                             values bound to the declarations to store in
 *                             the conflict set.
 */
public abstract class AbstractRuleBase implements Cloneable {

	/**
	 * The object base over with this rule base will work.
	 */
	private ObjectBase objectBase;

	/**
	 * The knowledge base that contains this rule base.
	 */
	private AbstractKnowledgeBase knowledgeBase;

	/**
	 * The index of the rule that has the focus. A rule is said to have
	 * focus when it's ready to be fired, i.e., the variables are filled
	 * such as the rule's preconditions are satisfied.
	 */
	private int ruleIndex;

	/**
	 * Class constructor.
	 *
	 * @param objectBase an object base over with this rule base will work.
	 * @param knowledgeBase the knowledge base that contains this rule base.
	 */
	public AbstractRuleBase(ObjectBase objectBase,
									AbstractKnowledgeBase knowledgeBase) {
		this.objectBase = objectBase;
		this.knowledgeBase = knowledgeBase;
	}

	/**
	 * Returns the index of the rule that has the focus. A rule is said to
	 * have focus when it's ready to be fired, i.e., the variables are
	 * filled such as the rule's preconditions are satisfied.
	 *
	 * @return the index of the rule that has the focus.
	 */
	public int getRuleIndex() {
		return ruleIndex;
	}

	/**
	 * Defines the index of the rule that has the focus. A rule is said to
	 * have focus when it's ready to be fired, i.e., the variables are
	 * filled such as the rule's preconditions are satisfied.
	 *
	 * @param value the index of the rule that has the focus.
	 */
	public void setRuleIndex(int value) {
		this.ruleIndex = value;
	}

	/**
	 * Returns the number of rules in this base.
	 *
	 * @return the number of rules in this base.
	 */
	public abstract int getNumberOfRules();

	/**
	 * Checks whether some preconditions of some rule in this rule
	 * base is satisfied.
	 *
	 * @param ruleIndex the index of the rule to be checked
	 * @param precIndex the index of the precondition to be checked
	 * @return <code>true</code> if the corresponding precondition for the
	 *          given rule is satisfied. <code>false</code> otherwise.
	 */
	public abstract boolean checkPrecondition(int ruleIndex, int precIndex);

	/**
	 * Checks whether all the preconditions of a rule which
	 * reference only the elements declared up to the given index
	 * are true.
	 *
	 * @param ruleIndex the index of the rule to be checked
	 * @param declIndex the index of the declaration to be checked
	 * @return <code>true</code> if all the preconditions of a rule which
	 *          reference only the elements declared up to the given index
	 *          are satisfied; <code>false</code> otherwise.
	 */
	public abstract boolean checkPrecForDeclaration(int ruleIndex, int declIndex);

	/**
	 * Fires one of the rules in this rule base.
	 *
	 * @param ruleIndex the index of the rule to be fired.
	 */
	protected abstract void internalFireRule(int ruleIndex);

	/**
	 * Returns a table that maps each declared identifier of a rule into
	 * its corresponding value.
	 *
	 * @param ruleIndex the index of the rule whose mapping is required.
	 * @return a table that maps each declared identifier of a rule into
	 *          its corresponding value.
	 */
	public abstract Hashtable getInternalRuleMapping(int ruleIndex);

	/**
	 * Fires one of the rules in this rule base.
	 *
	 * @param ruleIndex the index of the rule to be fired.
	 */
	public void fireRule(int ruleIndex) {
		this.ruleIndex = ruleIndex;
		internalFireRule(ruleIndex);
	}

	/**
	 * Returns the number of preconditions of the rules in this rule base.
	 *
	 * @return the number of preconditions  of the rules in this rule base.
	 */
	public abstract int[] getNumberOfPreconditions();

	/**
	 * Returns the number of declarations of the rules in this rule base.
	 *
	 * @return the number of declarations  of the rules in this rule base.
	 */
	public abstract int[] getNumberOfDeclarations();

	/**
	 * Returns the number of local declarations of the rules in this rule base.
	 *
	 * @return the number of local declarations of the rules in this rule base.
	 */
	public abstract int[] getNumberOfLocalDeclarations();

	/**
	 * Returns the name of the rules in this rule base.
	 *
	 * @return the name of the rules in this rule base.
	 */
	public abstract String[] getRuleNames();
	
	/**
	 * Returns a mapping from the classes of the objects declared in the
	 * rules to the number of occurrences of each one.
	 *
	 * @return a mapping from the classes of the objects declared in the
	 *          rules to the number of occurrences of each one.
	 */
	public abstract Hashtable getClassDeclarationCount();

	/**
	 * Returns the class name of an object declared in a rule.
	 *
	 * @param ruleIndex the index of the rule
	 * @param declarationIndex the index of the declaration.
	 * @return the class name of the declared object.
	 */
	public abstract String getDeclaredClassName(int ruleIndex, int declarationIndex);

	/**
	 * Returns the class of an object declared in a rule.
	 *
	 * @param ruleIndex the index of the rule
	 * @param declarationIndex the index of the declaration.
	 * @return the class of the declared object.
	 */
	public abstract Class getDeclaredClass(int ruleIndex, int declarationIndex);

	/**
	 * Sets an object that represents a declaration of some rule.
	 *
	 * @param ruleIndex the index of the rule
	 * @param declarationIndex the index of the declaration in the rule
	 * @param value the value of the object being set.
	 */
	public abstract void setObject(int ruleIndex, int declarationIndex, Object value);

	/**
	 * Returns an object that represents a declaration of some rule.
	 *
	 * @param ruleIndex the index of the rule
	 * @param declarationIndex the index of the declaration in the rule
	 * @return the value of the corresponding object.
	 */
	public abstract Object getObject(int ruleIndex, int declarationIndex);

	/**
	 * Returns all variables bound to the declarations of some rule.
	 * This method has a <code>protected</code> access control because
	 * only the knowledge base should get to use it.
	 *
	 * @param ruleIndex the index of the rule
	 * @return an object array of the variables bound to the declarations
	 *          of some rule.
	 */
	protected abstract Object[] getObjects(int ruleIndex);

	/**
	 * Defines all variables bound to the declarations of some rule.
	 * This method has a <code>protected</code> access control because
	 * only the knowledge base should get to use it.
	 *
	 * @param ruleIndex the index of the rule
	 * @param objects an an object array of the variables bound to the
	 *          declarations of some rule.
	 */
	protected abstract void setObjects(int ruleIndex, Object[] objects);

	/**
	 * Defines all variables bound to the local declarations of some rule.
	 * This method has a <code>protected</code> access control because
	 * only the knowledge base should get to use it.
	 *
	 * @param ruleIndex the index of the rule
	 * @param objects a 2-dimensional object array whose first column
	 *          contains of the variables bound to the local declarations
	 *          of some rule.
	 */
	protected abstract void setLocalObjects(int ruleIndex, Object[][] objects);

	/**
	 * Returns a dependency table between the local declarations and the
	 * regular ones. It will be a two-dimensional array that will have as
	 * many rows as there are local declarations; each row will have the
	 * object bound to the local declaration in the first column, and the
     * remaining columns will have the objects bound to the regular
     * declaration that created the locally declared object.
	 *
	 * @param ruleIndex the index of the rule.
	 * @return a two-dimensional array with the dependencies between the
	 *          local declarations and the regular ones.
	 */
	public abstract Object[][] getLocalDeclarationDependency(int ruleIndex);

	/*
	 * Compares this object with the given one.
	 *
	 * @param obj the object to be compared with this one
	 * @return <code>true</code> if they're the same object;
	 *          <code>false</code> otherwise.
	 */
	public boolean equals(Object obj) {
		boolean result = true;
		if (obj instanceof AbstractRuleBase) {
			AbstractRuleBase tmp = (AbstractRuleBase) obj;
			if (tmp.getRuleIndex() != this.getRuleIndex()) {
				result = false;
			} else {
				int noDecl = getNumberOfDeclarations()[ruleIndex];
				for (int i = 0; result && i < noDecl; i++) {
					Object obj1, obj2;
					obj1 = this.getObject(ruleIndex, i);
					obj2 = tmp.getObject(ruleIndex, i);
					if (!obj1.equals(obj2)) {
						result = false;
					}
				}
			}
		} else {
			result = false;
		}
		return result;
	}

	/*
	 * Returns a hash code for this object.
	 *
	 * @return a hash code for this object.
	 */
	public int hashCode() {
		int result;
		result = getRuleNames()[ruleIndex].hashCode();
		int noDecl = getNumberOfDeclarations()[ruleIndex];
		for (int i = 0; i < noDecl; i++) {
			Object obj;
			obj = this.getObject(ruleIndex, i);
			result += obj.hashCode();
		}
		return result;
	}

	/**
	 * Adds an object into this rule base.
	 *
	 * @param obj the object to be inserted into this base.
	 */
	public void tell(Object obj) {
		// Saving the state of this base
		Object[] declarations = getObjects(ruleIndex);
		Object[][] localDecl = getLocalDeclarationDependency(ruleIndex);

		knowledgeBase.tell(obj);

		// Restoring the previous state.
		setLocalObjects(ruleIndex, localDecl);
		setObjects(ruleIndex, declarations);
	}

	/**
	 * Removes an object from this rule base.
	 *
	 * @param obj the object to be removed from this base.
	 */
	public void retract(Object obj) {
		knowledgeBase.retract(obj);
	}

	/**
	 * Removes all the objects from this rule base.
	 */
	public void flush() {
		knowledgeBase.flush();
	}

	/**
	 * Tells this base that an object was modified, so that the rules
	 * can be retested against the object.
	 *
	 * @param obj the object that was modified.
	 */
	public void modified(Object obj) {
		// Saving the state of this base
		Object[] declarations = getObjects(ruleIndex);
		Object[][] localDecl = getLocalDeclarationDependency(ruleIndex);

		knowledgeBase.modified(obj);

		// Restoring the previous state.
		setLocalObjects(ruleIndex, localDecl);
		setObjects(ruleIndex, declarations);
	}

	/**
	 * Creates a copy of this object.
	 *
	 * @return a clone of this object.
	 * @exception CloneNotSupportedException if the class Object doesn't
	 *          support cloning; it should not happen.
	 */
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

}
