/*
 * JEOPS - The Java Embedded Object Production System
 * Copyright (c) 2000   Carlos Figueira Filho
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: Carlos Figueira Filho (csff@cin.ufpe.br)
 */

package jeops.conflict;

/**
 * An element present in the conflict set.
 *
 * @version 0.02  07 Apr 2000   The local declarations are now stored as a
 *                              two-dimensional array, which stores their
 *                              dependencies with the regular declarations.
 * @author Carlos Figueira Filho (<a href="mailto:csff@di.ufpe.br">csff@di.ufpe.br</a>)
 * @history 0.01  31 Mar 2000
 */
public class ConflictSetElement {

	/**
	 * The index of the fireable rule.
	 */
	private int ruleIndex;

	/**
	 * The objects bound to the declared variables.
	 */
	private Object[] objects;

	/**
	 * The objects bound to the locally declared variables, and their
	 * dependencies with the regular ones.
	 */
	private Object[][] localObjects;

	/**
	 * Class constructor.
	 *
	 * @param ruleIndex the index of the fireable rule.
	 * @param objects the objects bound to the declared variables.
	 * @param localOjects the the dependency table between the objects bound
	 *          to the to the locally declared variables and the objects
	 *          bound to the normal declarations.
	 */
	public ConflictSetElement(int ruleIndex, Object[] objects,
												Object[][] localObjects) {
		this.ruleIndex = ruleIndex;
		this.objects = objects;
		this.localObjects = localObjects;
	}

	/**
	 * Returns the index of the fireable rule.
	 *
	 * @return the index of the fireable rule.
	 */
	public int getRuleIndex() {
		return ruleIndex;
	}

	/**
	 * Returns the objects bound to the declared variables.
	 *
	 * @return the objects bound to the declared variables.
	 */
	public Object[] getObjects() {
		return objects;
	}

	/**
	 * Checks whether a given object is one of the objects bound to the
	 * declared variables in this element.
	 *
	 * @param obj the object to be checked.
	 * @return <code>true</code> if the given object bound to one of the
	 *          declared variables in this element; <code>false</code>
	 *          otherwise.
	 */
	public boolean isDeclared(Object obj) {
		for (int i = objects.length - 1; i >= 0; i--) {
			if (obj.equals(objects[i])) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the objects bound to the locally declared variables.
	 *
	 * @return the objects bound to the locally declared variables.
	 */
	public Object[][] getLocalObjects() {
		return localObjects;
	}

	/**
	 * Checks whether a given object is one of the objects bound to the
	 * locally declared variables in this element.
	 *
	 * @param obj the object to be checked.
	 * @return <code>true</code> if the given object bound to one of the
	 *          locally declared variables in this element;
	 *          <code>false</code> otherwise.
	 */
	public boolean isLocallyDeclared(Object obj) {
		for (int i = localObjects.length - 1; i >= 0; i--) {
			if (obj.equals(localObjects[i][0])) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Auxiliar method used to compare two arrays of objects.
	 *
	 * @param array1 the first array to be compared.
	 * @param array2 the second array to be compared.
	 * @return <code>true</code> if all elements of the arrays are equals;
	 *          <code>false</code> otherwise.
	 */
	private static boolean compareArrays(Object[] array1, Object[] array2) {
		if (array1.length != array2.length) {
			return false;
		} else {
			for (int i = array1.length - 1; i >= 0; i--) {
				if (array1[i] == null) {
					if (array2[i] != null) {
						return false;
					}
				} else if (!array1[i].equals(array2[i])) {
					return false;
				}
			}
			return true;
		}
	}

	/**
	 * Auxiliar method used to compare two 2-dimensional arrays of objects.
	 *
	 * @param array1 the first array to be compared.
	 * @param array2 the second array to be compared.
	 * @return <code>true</code> if the first elements of all rows of the
	 *          arrays are equals; <code>false</code> otherwise.
	 */
	private static boolean compareArrays2(Object[][] array1, Object[][] array2) {
		if (array1.length != array2.length) {
			return false;
		} else {
			for (int i = array1.length - 1; i >= 0; i--) {
				if (array1[i][0] == null) {
					if (array2[i][0] != null) {
						return false;
					}
				} else if (!array1[i][0].equals(array2[i][0])) {
					return false;
				}
			}
			return true;
		}
	}

	/**
	 * Compares this object with the given one.
	 *
	 * @param obj the object to be compared
	 * @return <code>true</code> if the two objects represent the same
	 *          conflict set element; <code>false</code> otherwise.
	 */
	public boolean equals(Object obj) {
		if (obj instanceof ConflictSetElement) {
			ConflictSetElement temp = (ConflictSetElement) obj;
			if (temp.ruleIndex != this.ruleIndex) {
				return false;
			} else {
				return compareArrays(temp.objects, this.objects) &&
					   compareArrays(temp.localObjects, this.localObjects);
			}
		}
		return false;
	}

}
