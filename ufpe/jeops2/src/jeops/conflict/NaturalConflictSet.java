/*
 * JEOPS - The Java Embedded Object Production System
 * Copyright (c) 2000   Carlos Figueira Filho
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: Carlos Figueira Filho (csff@cin.ufpe.br)
 */

package jeops.conflict;

import java.util.Vector;
import jeops.AbstractRuleBase;

/**
 * A conflict set whose conflict resolution policy specifies that no rule
 * can be fired more than once with the same objects. This conflict set
 * requires a large amount of memory to store the history of rule firing,
 * so it must be used with care. It also tends to get inefficient when
 * the history grows.
 *
 * @version 0.01  25 May 2000
 * @author Carlos Figueira Filho (<a href="mailto:csff@di.ufpe.br">csff@di.ufpe.br</a>)
 */
public class NaturalConflictSet extends DefaultConflictSet {

	/**
	 * The history rule firing.
	 */
	private Vector history;

	/**
	 * Class constructor.
	 */
	public NaturalConflictSet() {
		super();
		this.history = new Vector();
	}

	/**
	 * Removes all rules from this conflict set, as well as cleaning any
	 * history that might have been stored.
	 */
	public void flush() {
		super.flush();
		this.history = new Vector();
	}

	/**
	 * Inserts a rule instantiation.
	 * 
	 * @param element a conflict set element that holds the rule index as
	 *          well as the objects bound to the rule declarations.
	 */
	public void insertElement(ConflictSetElement element) {
		if (!history.contains(element)) {
			super.insertElement(element);
			history.addElement(element);
		}
	}

}
