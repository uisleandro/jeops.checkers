/*
 * JEOPS - The Java Embedded Object Production System
 * Copyright (c) 2000   Carlos Figueira Filho
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: Carlos Figueira Filho (csff@cin.ufpe.br)
 */

package jeops.conflict;

import java.util.Vector;

/**
 * A conflict set whose conflict resolution policy is one that rules defined
 * first in the rule base have a higher priority over the ones defined below
 * it.
 *
 * @version 0.01  14 Apr 2000
 * @author Carlos Figueira Filho (<a href="mailto:csff@di.ufpe.br">csff@di.ufpe.br</a>)
 */
public class PriorityConflictSet extends AbstractConflictSet {

	/**
	 * The vector where the firable rules are stored. It's actually
	 * a vector of vectors, to simulate a bidimensional matrix: the
	 * lines represent the rules and the columns will have their
	 * instantiations.
	 */
	private Vector firableRules;

	/**
	 * The number of elements in this conflict set.
	 */
	private int size;

	/**
	 * Class constructor.
	 */
	public PriorityConflictSet() {
		flush();
	}

	/**
	 * Removes all rules from this conflict set, as well as cleaning any
	 * history that might have been stored.
	 */
	public void flush() {
		this.firableRules = new Vector();
		this.size = 0;
	}

	/**
	 * Auxiliar method that ensures that the firable rules vector
	 * has as many elements as the given integer.
	 *
	 * @param size the size the vector must have.
	 */
	private void ensurefirableRulesCapacity(int size) {
		while (firableRules.size() < size) {
			firableRules.addElement(new Vector());
		}
	}

	/**
	 * Inserts a rule instantiation.
	 * 
	 * @param element a conflict set element that holds the rule index as
	 *          well as the objects bound to the rule declarations.
	 */
	public void insertElement(ConflictSetElement element) {
		int ruleIndex = element.getRuleIndex();
		ensurefirableRulesCapacity(ruleIndex + 1);
		Vector rules = (Vector) firableRules.elementAt(ruleIndex);
		if (!rules.contains(element)) {
			rules.addElement(element);
			elementAdded(element); // Callback method
			size++;
		}
	}

	/**
	 * Checks whether this set has any elements.
	 *
	 * @return <code>false</code> if there is at least one firable rule
	 *          in this set; <code>true</code> otherwise.
	 */
	public boolean isEmpty() {
		return (size == 0);
	}

	/**
	 * Returns the next rule to be fired.
	 *
	 * @return a conflict set element among those that have been inserted
	 *          in this object, according to the policy defined in this
	 *          conflict set.
	 * @exception NoMoreElementsException if there aren't any more elements
	 *          in this conflict set.
	 */
	public ConflictSetElement nextElement() throws NoMoreElementsException {
		ConflictSetElement result = null;
		if (size == 0) {
			throw new NoMoreElementsException("Empty conflict set");
		} else {
			for (int i = 0; result == null && i < firableRules.size(); i++) {
				Vector rules = (Vector) firableRules.elementAt(i);
				if (rules.size() != 0) {
					result = (ConflictSetElement) rules.elementAt(0);
					rules.removeElementAt(0);
				}
			}
			size--;
			return result;
		}
	}

	/**
	 * Remove all elements from this set that uses the given object
	 * in its instantiations.
	 *
	 * @param obj the given object
	 */
	public void removeElementsWith(Object obj) {
		size -= removeElementsWith_2D(firableRules, obj);
	}

	/**
	 * Returns all objects that were modified in response to a modification
	 * in the given object.
	 *
	 * @param obj the given object
	 * @return a vector with all objects that must be declared as modified
	 *          due to the modification in this one.
	 */
	public Vector getModifiedObjects(Object obj) {
		return getModifiedObjects_2D(firableRules, obj);
	}

}
