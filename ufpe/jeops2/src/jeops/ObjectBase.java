/*
 * JEOPS - The Java Embedded Object Production System
 * Copyright (c) 2000   Carlos Figueira Filho
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: Carlos Figueira Filho (csff@cin.ufpe.br)
 */

package jeops;

import java.util.Hashtable;
import java.util.Vector;

/**
 * This class models the facts over which the inference engine will act.
 * By facts we mean any object that is stored in this base - there's no
 * notion of truth or falseness. A fact simply exists or doesn't.
 *
 * @author Carlos Figueira Filho (<a href="mailto:csff@di.ufpe.br">csff@di.ufpe.br</a>)
 * @version 0.01  12 Mar 2000   Class adapted from previous version of JEOPS.
 */
public class ObjectBase {

	/**
	 * The hash table used to store the objects.
	 */
	private ObjectHashTable objects;

	/**
	 * A hashtable for caching previous results.
	 */
	private Hashtable cache;

	/**
	 * Class constructor. Creates a new object base, initially empty.
	 */
	public ObjectBase() {
		this.objects = new ObjectHashTable();
		this.cache = new Hashtable();
	}

	/**
	 * Inserts a new object into this object base.
	 *
	 * @param obj the object to be inserted.
	 */
	public void tell(Object obj) {
		objects.insert(obj);
		this.cache = new Hashtable();
	}

	/**
	 * Removes all objects of this base.
	 */
	public void flush() {
		objects.clear();
		this.cache = new Hashtable();
	}

	/**
	 * Returns the objects of the given class.
	 *
	 * @param className the name of the class whose objects are
	 *          being removed from this base.
	 * @return all objects that are instances of the given
	 *          class.
	 */
	public Vector objects(String className) {
		Object cached = cache.get(className);
		if (cached == null) {
			cached = objects.getObjects(className);
			cache.put(className, cached);
		}
		return (Vector) cached;
	}

	/**
	 * Removes an object from this object base.
	 *
	 * @param obj the object to be removed from this base.
	 * @return <code>true</code> if the given object belonged to this base;
	 *          <code>false</code> otherwise.
	 */
	public boolean remove(Object obj) {
		boolean result = objects.remove(obj);
		this.cache = new Hashtable();
		return result;
	}
}
