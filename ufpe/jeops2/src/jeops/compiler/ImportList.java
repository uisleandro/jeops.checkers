/*
 * JEOPS - The Java Embedded Object Production System
 * Copyright (c) 2000   Carlos Figueira Filho
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: Carlos Figueira Filho (csff@cin.ufpe.br)
 */

package jeops.compiler;

import java.io.File;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * A list of import statements.
 *
 * @version 0.01  09.03.2000
 * @author Carlos Figueira Filho (<a href="mailto:csff@di.ufpe.br">csff@di.ufpe.br</a>)
 */
public class ImportList {
	
	/**
	 * The set of generic import statements. A generic import statement is
	 * one such as <br>
	 * <code>import java.util.*;</code>
	 */
	private Vector genericImportStatements;

	/**
	 * The set of specific import statements. A specific import statement is
	 * one such as <br>
	 * <code>import java.util.Vector;</code>
	 */
	private Vector specificImportStatements;

	/**
	 * Auxiliar attribute used to store the directories of the classpath.
	 */
	private String[] classPath;

	/**
	 * Class constructor for rule bases in the default package.
	 */
	public ImportList() {
		this("");
	}

	/**
	 * Class constructor.
	 *
	 * @param packageName the package of the rule base.
	 */
	public ImportList(String packageName) {
		specificImportStatements = new Vector();
		genericImportStatements = new Vector();
		genericImportStatements.addElement("java.lang.");
		genericImportStatements.addElement(""); // The default package.
		if (packageName.length() != 0) {
			genericImportStatements.addElement(packageName + '.');
		}
		String classPath = System.getProperty("java.class.path");
		StringTokenizer st = new StringTokenizer(classPath, File.pathSeparator);
		Vector aux = new Vector();
		while (st.hasMoreTokens()) {
			String dir = st.nextToken();
			File f = new File(dir);
			if (f.exists() && f.isDirectory()) {
				if (dir.charAt(dir.length() - 1) != File.separatorChar) {
					dir = dir + File.separatorChar;
				}
				aux.addElement(dir);
			}
		}
		this.classPath = new String[aux.size()];
		for (int i = 0; i < aux.size(); i++) {
			this.classPath[i] = (String) aux.elementAt(i);
		}
	}

	/**
	 * Adds an import statement to this list.
	 *
	 * @param statement the statement to be added.
	 */
	public void addImport(String statement) {
		int len = statement.length();
		if (statement.charAt(len - 1) == '*') {
			genericImportStatements.addElement(statement.substring(0, len - 1));
		} else {
			specificImportStatements.addElement(statement);
		}
	}

	/**
	 * Returns the class that is represented by the given identifier for
	 * this import list.
	 *
	 * @param ident the identifier that represents a class.
	 * @return a set of classes that can be represented by the given
	 *          identifier.
	 * @exception ClassNotFoundException if the ident doesn't represent
	 *          any class, given the import statements of this list.
	 * @exception ImportException if the ident represents more than
	 *          one class, given the import statements of this list.
	 */
	public Class getRepresentingClass(String ident)
					throws ClassNotFoundException, ImportException {
		Class result = null;
		if (ident.indexOf('.') != -1) { // It's a fully qualified name
			result = Class.forName(ident);
		} else {
			for (int i = 0; i < specificImportStatements.size(); i++) {
				String statement = (String) specificImportStatements.elementAt(i);
				if (statement.endsWith(ident)) {
					try {
						Class c = Class.forName(statement);
						if (result == null || result.equals(c)) {
							result = c;
						} else {
							throw new ImportException("Ambiguous class: " +
								result + " and " + c);
						}
					} catch (ClassNotFoundException e) {}
				}
			}
			if (result == null) {
				for (int i = 0; i < genericImportStatements.size(); i++) {
					String statement = (String) genericImportStatements.elementAt(i);
					statement = statement.concat(ident);
					try {
						Class c = Class.forName(statement);
						if (result == null || result.equals(c)) {
							result = c;
						} else {
							throw new ImportException("Ambiguous class: " +
								result + " and " + c);
						}
					} catch (ClassNotFoundException e) {}
				}
			}
		}
		if (result == null) {
			throw new ClassNotFoundException(ident);
		} else {
			return result;
		}
	}

}
