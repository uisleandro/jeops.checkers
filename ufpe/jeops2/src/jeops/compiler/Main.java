/*
 * JEOPS - The Java Embedded Object Production System
 * Copyright (c) 2000   Carlos Figueira Filho
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: Carlos Figueira Filho (csff@cin.ufpe.br)
 */

package jeops.compiler;

import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import jeops.compiler.parser.Scanner;
import jeops.compiler.parser.Token;
import jeops.compiler.parser.TokenConstants;

/**
 * Main class in the Jeops phase of converting rule files into Java
 * classes.<p>
 * When invoked, the method <code>convert()</code> will create a java file
 * in the same directory as the rule file (with the extension changed from
 * <code>.rules</code> to <code>.java</code>). In this way, rule files can
 * be grouped into packages in the same way as java classes.
 *
 * @version 0.02  14.01.2000 Grouping of the preconditions according to the
 *								appearance of the declaration.
 * @author Carlos Figueira Filho (<a href="mailto:csff@di.ufpe.br">csff@di.ufpe.br</a>)
 * @history 0.01  06.01.2000 First version
 */
public class Main implements TokenConstants {
	
	/**
	 * The writer used to create the java file.
	 */
	private PrintWriter writer;

	/**
	 * The name of the rules file.
	 */
	private String ruleFileName;

	/**
	 * The name of the generated java file.
	 */
	private String javaFileName;

	/**
	 * The name of this rule base.
	 */
	private String ruleBaseName;

	/**
	 * The scanner used to read from the input file.
	 */
	private Scanner scanner;

	/**
	 * The current token.
	 */
	private Token token;

	/**
	 * The list of declaration of instance variables of the generated
	 * java file.
	 */
	private Vector variables;

	/**
	 * The list of the identifiers of the instance variables of the
	 * generated java file.
	 */
	private Vector variableIdents;

	/**
	 * The mapping from rule declared identifiers to the identifiers
	 * generated in the new java file.
	 */
	private Hashtable mapping;

	/**
	 * The mapping from rule declared identifiers to the class of the
	 * identifiers generated in the new java file.
	 */
	private Hashtable classMapping;

	/**
	 * The mapping from rule local declarations (identifiers) to the
	 * identifiers generated in the new java file.
	 */
	private Hashtable localDeclMapping;

	/**
	 * The name of the rule being converted.
	 */
	private String ruleName;

	/**
	 * The number of declarations in each rule.
	 */
	private Vector numberOfDeclarations;

	/**
	 * The number of local declarations in each rule.
	 */
	private Vector numberOfLocalDeclarations;

	/**
	 * The number of preconditions in each rule.
	 */
	private Vector numberOfPreconditions;

	/**
	 * The names of the rules.
	 */
	private Vector ruleNames;
	
	/**
	 * A mapping from the class of the objects declared to the maximum
	 * number of occurrences in a single rule.
	 */
	private Hashtable classDeclarationCount;
	
	/**
	 * The import list of this rule base.
	 */
	private ImportList importList;

	/**
	 * The list that stores the grouping of the preconditions according
	 * to the declarations. In other words, in a rule such as<br>
	 * <pre>
	 * rule makeCheck {
	 *   declarations
	 *     String s;
	 *     Vector v;
	 *  preconditions
	 *     s.length() == 6;
	 *     v.contains(s);
	 *  actions
	 *     v.removeElement(s);
	 * }
	 * </pre>
	 * when the the first declaration is set, only the first precondition
	 * needs to be checked, so that we can improve the performance of the
	 * system.<p>
	 * The elements of this vector will be also vectors (of Integer's),
	 * indicating the index(es) of the respective preconditions.
	 */
	private Vector precGrouping;

	/**
	 * The declarations of the current rule.
	 */
	private Vector ruleDeclarations;

	/**
	 * The converted declarations of the current rule.
	 */
	private Vector convertedRuleDeclarations;

	/**
	 * The converted declarations of the current rule.
	 */
	private Vector convertedRuleLocalDeclarations;

	/**
	 * The local declarations of the current rule.
	 */
	private Vector ruleLocalDeclarations;

	/**
	 * The index of the last declared identifier used by some
	 * rule local declaration.
	 */
	private Vector lastDeclForLocalDecl;

	/**
	 * The index of all declared identifiers used by some rule local
	 * declaration. It's a vector of Integer Vectors.
	 */
	private Vector allDeclForLocalDecl;
	
	/**
	 * Flag that indicates whether a rule base is declared in the file.
	 */
	private boolean thereIsRuleBase = false;

	/**
	 * Class constructor.
	 *
	 * @param ruleFileName the name of the rules file.
	 * @exception IOException if some IO error occurs.
	 */
	public Main(String ruleFileName) throws IOException {
		scanner = new Scanner(ruleFileName);
		this.ruleFileName = ruleFileName;
		variables = new Vector();
		variableIdents = new Vector();
		classDeclarationCount = new Hashtable();
		numberOfPreconditions = new Vector();
		numberOfDeclarations = new Vector();
		numberOfLocalDeclarations = new Vector();
		ruleNames = new Vector();
		importList = new ImportList();
		precGrouping = new Vector();
		File aux = new File(ruleFileName);
		if (!aux.exists()) {
			throw new FileNotFoundException("File not found: " + ruleFileName);
		}
		ruleFileName = aux.getAbsolutePath();
		int i = ruleFileName.lastIndexOf(".rules");
		if (i != -1) {
			ruleBaseName = ruleFileName.substring(0, i);
		} else {
			ruleBaseName = ruleFileName;
		}
		javaFileName = ruleBaseName + ".java";
	}

	/**
	 * Checks whether a token is of a given type, throwing an exception
	 * if it's not.
	 *
	 * @param token the token to be checked.
	 * @param type the desired token type.
	 * @param message the message in the exception.
	 * @exception JeopsException if the token is of an unexpected type.
	 */
	private void checkToken(Token token, int tokenType,
									String message) throws JeopsException {
		if (token.getTokenType() != tokenType) {
			message = message.concat(" at line " +
										scanner.getCurrentLine() + ", column " +
										scanner.getCurrentColumn());
			throw new JeopsException(message, scanner.getCurrentLine(),
												scanner.getCurrentColumn());
		}
	}

	/**
	 * Checks whether a token is one of several given types, throwing an
	 * exception if it's not.
	 *
	 * @param token the token to be checked.
	 * @param types the desired token types.
	 * @param message the message in the exception.
	 * @exception JeopsException if the token is of an unexpected type.
	 */
	private void checkToken(Token token, int[] tokenTypes,
									String message) throws JeopsException {
		boolean ok = false;
		for (int i = 0; !ok && i < tokenTypes.length; i++) {
			if (token.getTokenType() == tokenTypes[i]) {
				ok = true;
			}
		}
		if (!ok) {
			message = message.concat(" at line " +
										scanner.getCurrentLine() + ", column " +
										scanner.getCurrentColumn());
			throw new JeopsException(message, scanner.getCurrentLine(),
												scanner.getCurrentColumn());
		}
	}

	/**
	 * Auxiliar method used to advance through white space and comments.
	 * The caller must indicate whether the comments should be written to
	 * the output file. This method throws an error if EOF is found.
	 *
	 * @param writer the print writer used to write to the generated file.
	 * @param outputComments if the white space and comments should be
	 *          written to the output file
	 * @exception IOException if some IO error occurs.
	 */
	private void skipWhiteSpace(PrintWriter writer, boolean outputComments)
												throws IOException {
		while (token.getTokenType() == WHITE_SPACE ||
									token.getTokenType() == COMMENT) {
			if (outputComments) {
				writer.print(token.getLexeme());
			}
			token = scanner.nextToken();
		}
		if (token.getTokenType() == EOF) {
			String message = "EOF unexpected at line " +
								scanner.getCurrentLine() + ", column " +
								scanner.getCurrentColumn();
			throw new EOFException(message);
		}
	}

	/**
	 * Reads the next token from the scanner, throwing an exception in
	 * case of an error.
	 *
	 * @return the token read from the scanner.
	 * @exception IOException if some IO error occurs.
	 * @exception JeopsException is the token read is an error.
	 */
	private Token readNextToken() throws IOException, JeopsException {
		Token result = scanner.nextToken();
		if (result.getTokenType() == ERROR) {
			throw new JeopsException(token.getLexeme(),
										scanner.getCurrentLine(),
										scanner.getCurrentColumn());
		}
		return result;
	}

	/**
	 * Processes the local declarations part of a rule. This method
	 * continues filling the <code>mapping</code> field with the declared
	 * variables, so that their correspondent variables in the generated
	 * file can be easily discovered. It will also continues filling the
	 * </code>inverseMapping</code> which is the mirror of the
	 * </code>mapping</code> field, the fields <code>ruleDeclarations</code> and
	 * <code>precGrouping</code>.<p>
	 * This method also deals with the optional section of local
	 * declarations of the rule, so that it guarantees that after this
	 * method is successfully invoked, the token type is
	 * <code>PRECONDITIONS</code><p>.
	 * In order to be correctly invoked, the current token
	 * type must be <code>DECLARATIONS</code>. After successful completion,
	 * the token type is <code>PRECONDITIONS</code>.
	 *
	 * @param writer the print writer used to write to the generated file.
	 * @exception IOException if some IO error occurs.
	 * @exception JeopsException if some error occurs while converting the rule.
	 * @precondition token.getTokenType() == DECLARATIONS
	 * @postcondition token.getTokenType() == PRECONDITIONS
	 */
	private void convertRuleLocalDeclarations(PrintWriter writer)
								throws IOException, JeopsException {
		token = readNextToken();
		skipWhiteSpace(writer, false);
		int localDeclNumber = 0;
		Hashtable quantities = new Hashtable();
		Vector ruleLocalDeclarationsClass = new Vector();

		while (token.getTokenType() != PRECONDITIONS) {
			checkToken(token, IDENT, "identifier expected");
			String declaredType = token.getLexeme();
			token = readNextToken();
			while (token.getTokenType() == DOT) {
				declaredType = declaredType.concat(token.getLexeme());
				token = readNextToken();
				checkToken(token, IDENT, "identifier expected");
				declaredType = declaredType.concat(token.getLexeme());
				token = readNextToken();
			}
			skipWhiteSpace(writer, false);
			checkToken(token, IDENT, "identifier expected");

			try {			
				Class c = importList.getRepresentingClass(declaredType);
				declaredType = c.getName();
			} catch (ClassNotFoundException e) {
				throw new JeopsException("ClassNotFound: " + e.getMessage(),
												scanner.getCurrentLine(),
												scanner.getCurrentColumn());
			} catch (ImportException e) {
				throw new JeopsException(e.getMessage(),
											scanner.getCurrentLine(),
												scanner.getCurrentColumn());
			}

			// We've found a locally declared variable.
			String declaredIdent = token.getLexeme();
			token = readNextToken();
			skipWhiteSpace(writer, false);
			checkToken(token, EQUALS, "'=' expected");
			token = readNextToken();
			skipWhiteSpace(writer, false);
			
			Vector allDecls = new Vector(); // Stores all declarations used by
											// this locally declared variable.]

			StringBuffer convertedExpression = new StringBuffer();
			StringBuffer originalExpression = new StringBuffer();
			int lastDecl = 0;
			while (token.getTokenType() != SEMICOLON) {

				String lex = token.getLexeme();
				originalExpression.append(lex);
				Object map = null;
				if (token.getTokenType() == IDENT &&
						scanner.getLastNonWhiteSpaceToken().getTokenType() != DOT) {
					map = mapping.get(lex);
					if (map == null) {
						map = localDeclMapping.get(lex);
					}
				}
				if (map == null) {
					convertedExpression.append(lex);
				} else {
					convertedExpression.append((String) map);
					int indDecl = ruleDeclarations.indexOf(lex);
					if (indDecl == -1) { // It's not a declaration,
										 // it's a local declaration...
						int locDec = ruleLocalDeclarations.indexOf(lex);
						if (locDec != -1) {
							Vector vAux = (Vector) allDeclForLocalDecl.elementAt(locDec);
							for (int i = 0; i < vAux.size(); i++) {
								Integer declIndex = (Integer) vAux.elementAt(i);
								if (!allDecls.contains(declIndex)) {
									allDecls.addElement(declIndex);
								}
							}
						}
						indDecl = ((Integer) lastDeclForLocalDecl.elementAt(locDec)).intValue();
					} else {
						Integer declIndex = new Integer(indDecl);
						if (!allDecls.contains(declIndex)) {
							allDecls.addElement(declIndex);
						}
					}
					if (indDecl > lastDecl) {
						lastDecl = indDecl;
					}
				}
				token = readNextToken();
				if (token.getTokenType() == EOF) {
					String message = "EOF unexpected at line " +
										scanner.getCurrentLine() + ", column " +
										scanner.getCurrentColumn();
					throw new EOFException(message);
				}
			}
			ruleLocalDeclarations.addElement(declaredIdent);
			ruleLocalDeclarationsClass.addElement(declaredType);
			lastDeclForLocalDecl.addElement(new Integer(lastDecl));
			allDeclForLocalDecl.addElement(allDecls);

			token = readNextToken();
			skipWhiteSpace(writer, false);
			Object numberOfOccurrences = quantities.get(declaredType);
			if (numberOfOccurrences == null) {
				numberOfOccurrences = new Integer(1);
			} else {
				int aux = ((Integer) numberOfOccurrences).intValue();
				numberOfOccurrences = new Integer(aux + 1);
			}
			quantities.put(declaredType, numberOfOccurrences);
			String convertedDeclaration = "local_decl_" + declaredType.replace('.', '_') + '_' + numberOfOccurrences;

			convertedRuleLocalDeclarations.addElement(convertedDeclaration);

			if (!variableIdents.contains(convertedDeclaration)) {
				variables.addElement(declaredType + " " + convertedDeclaration + ";");
				variableIdents.addElement(convertedDeclaration);
			}
			localDeclMapping.put(declaredIdent, convertedDeclaration);

			writer.println();
			writer.println("    /**");
			writer.println("     * Local declaration " + localDeclNumber +
										" of rule " + ruleName + ".<p>");
			writer.println("     * The original expression was:<br>");
			writer.println("     * <code>" + originalExpression.toString() + "</code>");
			writer.println("     *");
			writer.println("     * @return <code>true</code> if the precondition is satisfied;");
			writer.println("     *          <code>false</code> otherwise.");
			writer.println("     */");
			writer.println("    public void set" + ruleName + "_localDecl_" + localDeclNumber + "() {");
			writer.println("        " + convertedDeclaration + " = (" +
									convertedExpression.toString() + ");");
			writer.println("    }");

			localDeclNumber++;
		}
		
		// Writes the setting function, that sets all the local declarations needed
		// for some given declaration index.
		writer.println();
		writer.println("    /**");
		writer.println("     * Sets the local declaration for a given declaration index");
		writer.println("     *");
		writer.println("     * @param declIndex the index of the declaration.");
		writer.println("     */");
		writer.println("    private void setLocalDeclarations_" + ruleName + "(int declIndex) {");
		for (int i = 0; i < lastDeclForLocalDecl.size(); i++) {
			Integer aux = (Integer) lastDeclForLocalDecl.elementAt(i);
			writer.println("        if (declIndex == " + aux + ") set" +
										ruleName + "_localDecl_" + i + "();");
		}
		writer.println("    }");

		writer.println();
		writer.println("    /**");
		writer.println("     * Returns a dependency table between the local declarations");
		writer.println("     * and the regular ones for rule " + ruleName);
		writer.println("     *");
		writer.println("     * @return a dependency table between the local declarations");
		writer.println("     *          and the regular ones for this rule.");
		writer.println("     */");
		writer.println("    private Object[][] getLocalDeclarationDependency_" + ruleName + "() {");
		if (convertedRuleLocalDeclarations.size() == 0) {
			writer.println("        return EMPTY_OBJECT_DOUBLE_ARRAY;");
		} else {
			writer.println("        Object[][] result = new Object[" +
									convertedRuleLocalDeclarations.size()+"][];");
			for (int i = 0; i < convertedRuleLocalDeclarations.size(); i++) {
				String convertedDeclaration = (String) convertedRuleLocalDeclarations.elementAt(i);
				Vector allDecls = (Vector) allDeclForLocalDecl.elementAt(i);
				writer.println("        result["+i+"] = new Object["+ (allDecls.size() + 1) +"];");
				writer.println("        result["+i+"][0] = " + convertedDeclaration + ";");
				for (int j = 0; j < allDecls.size(); j++) {
					int index = ((Integer) allDecls.elementAt(j)).intValue();
					String decl = (String) convertedRuleDeclarations.elementAt(index);
					writer.println("        result["+i+"]["+(j+1)+"] = " + decl + ";");
				}
			}
			writer.println("        return result;");
		}
		writer.println("    }");

		// Method that sets all the local objects of this rule.
		writer.println();
		writer.println("    /**");
		writer.println("     * Defines all variables bound to the local declarations ");
		writer.println("     * of rule " + ruleName);
		writer.println("     *");
		writer.println("     * @param objects a 2-dimensional object array whose first column");
		writer.println("     *          contains of the variables bound to the local declarations");
		writer.println("     *          of this rule.");
		writer.println("     */");
		writer.println("    protected void setLocalObjects_" + ruleName + "(Object[][] objects) {");
		for (int i = 0; i < convertedRuleLocalDeclarations.size(); i++) {
			String decl = (String) convertedRuleLocalDeclarations.elementAt(i);
			String className = (String) ruleLocalDeclarationsClass.elementAt(i);
			writer.println("        " + decl + " = (" + className + ") objects[" + i + "][0];");
		}
		writer.println("    }");

		numberOfLocalDeclarations.addElement(new Integer(ruleLocalDeclarations.size()));
	}

	/**
	 * Processes the declaration part of a rule. This method fills the
	 * <code>mapping</code> field with the declared variables, so that
	 * their correspondent variables in the generated file can be easily
	 * discovered. It will also fill the </code>inverseMapping</code>
	 * which is the mirror of the </code>mapping</code> field,
	 * the fields <code>ruleDeclarations</code> and
	 * <code>precGrouping</code>.<p>
	 * This method also deals with the optional section of local
	 * declarations of the rule, so that it guarantees that after this
	 * method is successfully invoked, the token type is
	 * <code>PRECONDITIONS</code><p>.
	 * In order to be correctly invoked, the current token
	 * type must be <code>DECLARATIONS</code>. After successful completion,
	 * the token type is <code>PRECONDITIONS</code>.
	 *
	 * @param writer the print writer used to write to the generated file.
	 * @exception IOException if some IO error occurs.
	 * @exception JeopsException if some error occurs while converting the rule.
	 * @precondition token.getTokenType() == DECLARATIONS
	 * @postcondition token.getTokenType() == PRECONDITIONS
	 */
	private void convertRuleDeclarations(PrintWriter writer)
								throws IOException, JeopsException {
		// Auxiliar variable used to indicate the number of declared
		// variables of each type (class).
		Hashtable quantities = new Hashtable();
		token = readNextToken();
		skipWhiteSpace(writer, false);
		ruleDeclarations = new Vector();
		convertedRuleDeclarations = new Vector();
		convertedRuleLocalDeclarations = new Vector();
		ruleLocalDeclarations = new Vector();
		precGrouping = new Vector();
		
		mapping = new Hashtable();
		classMapping = new Hashtable();
		localDeclMapping = new Hashtable();

		Vector declaredClassNames = new Vector();

		while (token.getTokenType() != PRECONDITIONS &&
									token.getTokenType() != LOCALDECL) {
			checkToken(token, IDENT, "identifier expected");
			String declaredType = token.getLexeme();
			token = readNextToken();
			while (token.getTokenType() == DOT) {
				declaredType = declaredType.concat(token.getLexeme());
				token = readNextToken();
				checkToken(token, IDENT, "identifier expected");
				declaredType = declaredType.concat(token.getLexeme());
				token = readNextToken();
			}
			skipWhiteSpace(writer, false);
			checkToken(token, IDENT, "identifier expected");

			try {			
				Class c = importList.getRepresentingClass(declaredType);
				declaredType = c.getName();
			} catch (ClassNotFoundException e) {
				throw new JeopsException("ClassNotFound: " + e.getMessage(),
												scanner.getCurrentLine(),
												scanner.getCurrentColumn());
			} catch (ImportException e) {
				throw new JeopsException(e.getMessage(),
											scanner.getCurrentLine(),
												scanner.getCurrentColumn());
			}

			// We've found a declared variable.
			String declaredIdent = token.getLexeme();

			while (declaredIdent != null) {
				ruleDeclarations.addElement(declaredIdent);
				declaredClassNames.addElement(declaredType);
				precGrouping.addElement(new Vector());
				Object numberOfOccurrences = quantities.get(declaredType);
				if (numberOfOccurrences == null) {
					numberOfOccurrences = new Integer(1);
				} else {
					int aux = ((Integer) numberOfOccurrences).intValue();
					numberOfOccurrences = new Integer(aux + 1);
				}
				quantities.put(declaredType, numberOfOccurrences);
				String convertedDeclaration = declaredType.replace('.', '_') + '_' + numberOfOccurrences;
				convertedRuleDeclarations.addElement(convertedDeclaration);

				if (!variableIdents.contains(convertedDeclaration)) {
					variables.addElement(declaredType + " " + convertedDeclaration + ";");
					variableIdents.addElement(convertedDeclaration);
					classDeclarationCount.put(declaredType, numberOfOccurrences);
				}
				mapping.put(declaredIdent, convertedDeclaration);
				classMapping.put(declaredIdent, declaredType);

				token = readNextToken();
				skipWhiteSpace(writer, false);

				// Checking whether there is more than one variable
				// in the same declaration.
				checkToken(token, new int[] {SEMICOLON, COMMA}, "\"" +
										token.getLexeme() + "\" unexpected");

				if (token.getTokenType() == SEMICOLON) {
					declaredIdent = null;
					token = readNextToken();
				} else { // It's a COMMA
					token = readNextToken();
					while (token.getTokenType() == WHITE_SPACE ||
												token.getTokenType() == COMMENT) {
						token = readNextToken();
					}
					checkToken(token, IDENT, "identifier expected");
					// We've found a declared variable.
					declaredIdent = token.getLexeme();
				}
			}
			skipWhiteSpace(writer, false);
		}
		if (token.getTokenType() == LOCALDECL) {
			// There are local declarations...
			lastDeclForLocalDecl = new Vector();
			allDeclForLocalDecl = new Vector();
			convertRuleLocalDeclarations(writer);
		} else {
			// There are no local declarations; writing a method that does
			// nothing when invoked to set the local declarations and return
			// the dependencies between the local and regular declarations.
			writer.println();
			writer.println("    /**");
			writer.println("     * Sets the local declaration for a given declaration index.");
			writer.println("     * As there are no local declarations for this rule, this");
			writer.println("     * method does nothing.");
			writer.println("     *");
			writer.println("     * @param declIndex the index of the declaration.");
			writer.println("     */");
			writer.println("    private void setLocalDeclarations_" + ruleName + "(int index) {");
			writer.println("    }");

			writer.println();
			writer.println("    /**");
			writer.println("     * Returns a dependency table between the local declarations ");
			writer.println("     * and the regular ones for rule " + ruleName + ". As here are");
			writer.println("     * no local declarations for this rule, this method returns an");
			writer.println("     * empty array.");
			writer.println("     *");
			writer.println("     * @return a dependency table between the local declarations");
			writer.println("     *          and the regular ones for this rule.");
			writer.println("     */");
			writer.println("    private Object[][] getLocalDeclarationDependency_" + ruleName + "() {");
			writer.println("        return EMPTY_OBJECT_DOUBLE_ARRAY;");
			writer.println("    }");

			// Method that sets all the local objects of this rule.
			writer.println();
			writer.println("    /**");
			writer.println("     * Defines all variables bound to the local declarations ");
			writer.println("     * of rule " + ruleName);
			writer.println("     *");
			writer.println("     * @param objects a 2-dimensional object array whose first column");
			writer.println("     *          contains of the variables bound to the local declarations");
			writer.println("     *          of this rule.");
			writer.println("     */");
			writer.println("    protected void setLocalObjects_" + ruleName + "(Object[][] objects) {");
			writer.println("    }");

		}

		// Writes the mapping function, that returns a map from each declared
		// identifier to the corresponding value.
		writer.println();
		writer.println("    /**");
		writer.println("     * Returns the mapping of declared identifiers into corresponding");
		writer.println("     * value for the rule " + ruleName);
		writer.println("     *");
		writer.println("     * @return the mapping of declared identifiers into corresponding");
		writer.println("     *          value for the rule " + ruleName);
		writer.println("     */");
		writer.println("    private java.util.Hashtable getMapRule" + ruleName + "() {");
		writer.println("        java.util.Hashtable result = new java.util.Hashtable();");
		for (int i = 0; i < ruleDeclarations.size(); i++) {
			String declared = (String) ruleDeclarations.elementAt(i);
			String object = (String) mapping.get(declared);
			writer.println("        result.put(\"" + declared + "\", " +
												object + ");");
		}
		writer.println("        return result;");
		writer.println("    }");

		numberOfDeclarations.addElement(new Integer(ruleDeclarations.size()));
		
		// Writes a function that returns the class names of the declared objects
		// in this rule.
		writer.println();
		writer.println("    /**");
		writer.println("     * Returns the name of the class of one declared object for");
		writer.println("     * rule " + ruleName + ".");
		writer.println("     *");
		writer.println("     * @param index the index of the declaration");
		writer.println("     * @return the name of the class of the declared objects for");
		writer.println("     *          this rule.");
		writer.println("     */");
		writer.println("    private String getDeclaredClassName_" + ruleName + "(int index) {");
		writer.println("        switch (index) {");
		for (int i = 0; i < declaredClassNames.size(); i++) {
			writer.println("            case " + i + ": return \"" +
									declaredClassNames.elementAt(i) + "\";");
		}
		writer.println("            default: return null;");
		writer.println("        }");
		writer.println("    }");

		// Writes a function that returns the class of the declared objects
		// in this rule.
		writer.println();
		writer.println("    /**");
		writer.println("     * Returns the class of one declared object for rule " + ruleName + ".");
		writer.println("     *");
		writer.println("     * @param index the index of the declaration");
		writer.println("     * @return the class of the declared objects for this rule.");
		writer.println("     */");
		writer.println("    private Class getDeclaredClass_" + ruleName + "(int index) {");
		writer.println("        switch (index) {");
		for (int i = 0; i < declaredClassNames.size(); i++) {
			writer.println("            case " + i + ": return " +
									declaredClassNames.elementAt(i) + ".class;");
		}
		writer.println("            default: return null;");
		writer.println("        }");
		writer.println("    }");

		// Writes a method that defines the value of an object in this rule.
		writer.println();
		writer.println("    /**");
		writer.println("     * Sets an object declared in the rule " + ruleName + ".");
		writer.println("     *");
		writer.println("     * @param index the index of the declared object");
		writer.println("     * @param value the value of the object being set.");
		writer.println("     */");
		writer.println("    private void setObject_" + ruleName + "(int index, Object value) {");
		writer.println("        switch (index) {");
		for (int i = 0; i < ruleDeclarations.size(); i++) {
			String declaration = (String) ruleDeclarations.elementAt(i);
			writer.println("            case " + i + ": this." +
									mapping.get(declaration) + " = (" +
									classMapping.get(declaration) + ") value; break;");
		}
		writer.println("        }");
		writer.println("    }");

		// Writes a method that returns the value of an object in this rule.
		writer.println();
		writer.println("    /**");
		writer.println("     * Returns an object declared in the rule " + ruleName + ".");
		writer.println("     *");
		writer.println("     * @param index the index of the declared object");
		writer.println("     * @return the value of the corresponding object.");
		writer.println("     */");
		writer.println("    private Object getObject_" + ruleName + "(int index) {");
		writer.println("        switch (index) {");
		for (int i = 0; i < convertedRuleDeclarations.size(); i++) {
			String declaration = (String) convertedRuleDeclarations.elementAt(i);
			writer.println("            case " + i + ": return " + declaration + ";");
		}
		writer.println("            default: return null;");
		writer.println("        }");
		writer.println("    }");

		// Method that returns all the declared objects of this rule.
		writer.println();
		writer.println("    /**");
		writer.println("     * Returns all variables bound to the declarations ");
		writer.println("     * of rule " + ruleName);
		writer.println("     *");
		writer.println("     * @return an object array of the variables bound to the");
		writer.println("     *          declarations of this rule.");
		writer.println("     */");
		writer.println("    protected Object[] getObjects_" + ruleName + "() {");
		writer.println("        return new Object[] {");
		for (int i = 0; i < convertedRuleDeclarations.size(); i++) {
			String decl = (String) convertedRuleDeclarations.elementAt(i);
			writer.print("                            " + decl);
			if (i != convertedRuleDeclarations.size() - 1) {
				writer.println(",");
			} else {
				writer.println();
			}
		}
		writer.println("                            };");
		writer.println("    }");

		// Method that sets all the declared objects of this rule.
		writer.println();
		writer.println("    /**");
		writer.println("     * Defines all variables bound to the declarations ");
		writer.println("     * of rule " + ruleName);
		writer.println("     *");
		writer.println("     * @param objects an object array of the variables bound to the");
		writer.println("     *          declarations of this rule.");
		writer.println("     */");
		writer.println("    protected void setObjects_" + ruleName + "(Object[] objects) {");
		for (int i = 0; i < convertedRuleDeclarations.size(); i++) {
			String decl = (String) convertedRuleDeclarations.elementAt(i);
			String className = (String) declaredClassNames.elementAt(i);
			writer.println("        " + decl + " = (" + className + ") objects[" + i + "];");
		}
		writer.println("    }");

	}


	/**
	 * Processes the preconditions part of a rule. This method creates one
	 * method (returning a boolean value) for every precondition of the
	 * rule. Each method will be named as the name of the rule followed
	 * by "_prec_<n>", where n is the index of the precondition.
	 * Additionally, another method will be created, named as the name
	 * of the rule followed only by "_prec", that will receive an integer
	 * used to check the correct precondition.<p>
	 * In order to be correctly invoked, the current token
	 * type must be <code>PRECONDITIONS</code>. After successful completion,
	 * the token type is <code>ACTIONS</code>.
	 *
	 * @param writer the print writer used to write to the generated file.
	 * @exception IOException if some IO error occurs.
	 * @exception JeopsException if some error occurs while converting the rule.
	 * @precondition token.getTokenType() == PRECONDITIONS
	 * @postcondition token.getTokenType() == ACTIONS
	 */
	private void convertRulePreconditions(PrintWriter writer)
								throws IOException, JeopsException {

		int precNumber = 0; // The number of the current precondition.
		StringBuffer originalExpression;
		StringBuffer convertedExpression;

		token = readNextToken();
		skipWhiteSpace(writer, false);

		while (token.getTokenType() != ACTIONS) {
			originalExpression = new StringBuffer();
			convertedExpression = new StringBuffer();
			int maximumDeclaration = 0; // The last declared identifier
										// used in this precondition

			while (token.getTokenType() != SEMICOLON) {
				String lex = token.getLexeme();
				originalExpression.append(lex);
				Object map = null;
				if (token.getTokenType() == IDENT &&
						scanner.getLastNonWhiteSpaceToken().getTokenType() != DOT) {
					map = mapping.get(lex);
					if (map == null) {
						map = localDeclMapping.get(lex);
					}
				}
				if (map == null) {
					convertedExpression.append(lex);
				} else {
					convertedExpression.append((String) map);
					int indDecl = ruleDeclarations.indexOf(lex);
					if (indDecl == -1) { // It's not a declaration,
										 // it's a local declaration...
						indDecl = ruleLocalDeclarations.indexOf(lex);
						if (indDecl >= 0 && indDecl < lastDeclForLocalDecl.size()) {
							Integer aux = (Integer) lastDeclForLocalDecl.elementAt(indDecl);
							indDecl = aux.intValue();
						}
					}
					if (indDecl > maximumDeclaration) {
						maximumDeclaration = indDecl;
					}
				}
				token = readNextToken();
				if (token.getTokenType() == EOF) {
					String message = "EOF unexpected at line " +
										scanner.getCurrentLine() + ", column " +
										scanner.getCurrentColumn();
					throw new EOFException(message);
				}
			}
			token = readNextToken();
			writer.println();
			writer.println("    /**");
			writer.println("     * Precondition " + precNumber +
										" of rule " + ruleName + ".<p>");
			writer.println("     * The original expression was:<br>");
			writer.println("     * <code>" + originalExpression.toString() + "</code>");
			writer.println("     *");
			writer.println("     * @return <code>true</code> if the precondition is satisfied;");
			writer.println("     *          <code>false</code> otherwise.");
			writer.println("     */");
			writer.println("    public boolean " + ruleName + "_prec_" + precNumber + "() {");
			writer.println("        return (" + convertedExpression.toString() + ");");
			writer.println("    }");
			Vector v = (Vector) precGrouping.elementAt(maximumDeclaration);
			v.addElement(new Integer(precNumber));
			precNumber++;
			skipWhiteSpace(writer, false);
		}
		writer.println();
		writer.println("    /**");
		writer.println("     * Checks whether some preconditions of rule " + ruleName + " is satisfied.");
		writer.println("     *");
		writer.println("     * @param index the index of the precondition to be checked.");
		writer.println("     * @return <code>true</code> if the precondition is satisfied;");
		writer.println("     *          <code>false</code> otherwise.");
		writer.println("     */");
		writer.println("    public boolean " + ruleName + "_prec(int index) {");
		writer.println("        switch (index) {");
		for (int i = 0; i < precNumber; i++) {
			writer.println("            case " + i + ": return " +
											ruleName + "_prec_" + i + "();");
		}
		writer.println("            default: return false;");
		writer.println("        }");
		writer.println("    }");
		
		// Method for checking the preconditions of a rule that use some
		// declared identifiers.
		writer.println();
		writer.println("    /**");
		writer.println("     * Checks whether all the preconditions of a rule which");
		writer.println("     * reference some declared element of the declarations are");
		writer.println("     * true.");
		writer.println("     *");
		writer.println("     * @param declIndex the index of the declared element.");
		writer.println("     * @return <code>true</code> if the preconditions that reference");
		writer.println("     *          up to the given declaration are true;");
		writer.println("     *          <code>false</code> otherwise.");
		writer.println("     */");
		writer.println("    public boolean checkPrecForDeclaration_" +
										ruleName + "(int declIndex) {");
		writer.println("        setLocalDeclarations_" + ruleName + "(declIndex);");
		writer.println("        switch (declIndex) {");
		for (int i = 0; i < precGrouping.size(); i++) {
			writer.println("            case " + i + ":");
			Vector v = (Vector) precGrouping.elementAt(i);
			for (int j = 0; j < v.size(); j++) {
				int precNo = ((Integer) v.elementAt(j)).intValue();
				writer.println("                if (!"+ruleName+"_prec_"+precNo+"()) return false;");
			}
			writer.println("                return true;");
		}
		writer.println("            default: return false;");
		writer.println("        }");
		writer.println("    }");
		numberOfPreconditions.addElement(new Integer(precNumber));
	}

	/**
	 * Processes the actions part of a rule. This method creates one void
	 * method for the whole body of actions of the rule. The method will
	 * have the same name of the rule.<p>
	 * In order to be correctly invoked, the current token
	 * type must be <code>ACTIONS</code>. After successful completion,
	 * the token type is <code>CLOSE_CURLY_BRACKET</code>, indicating
	 * the end of the rule.
	 *
	 * @param writer the print writer used to write to the generated file.
	 * @exception IOException if some IO error occurs.
	 * @exception JeopsException if some error occurs while converting the rule.
	 * @precondition token.getTokenType() == ACTIONS
	 * @postcondition token.getTokenType() == CLOSE_CURLY_BRACKET
	 */
	private void convertRuleActions(PrintWriter writer)
								throws IOException, JeopsException {

		int bracketCount = 1; // The number of opened brackets.

		token = readNextToken();

		writer.println();
		writer.println("    /**");
		writer.println("     * Executes the action part of the rule " + ruleName);
		writer.println("     */");
		writer.print("    public void " + ruleName + "() {");
		while (bracketCount > 0) {
			String lex = token.getLexeme();
			Object map = null;
			if (token.getTokenType() == IDENT &&
					scanner.getLastNonWhiteSpaceToken().getTokenType() != DOT) {
				map = mapping.get(lex);
				if (map == null) {
					map = localDeclMapping.get(lex);
				}
			}
			if (map == null) {
				writer.print(lex);
			} else {
				writer.print((String) map);
			}
			token = readNextToken();
			if (token.getTokenType() == EOF) {
				String message = "EOF unexpected at line " +
									scanner.getCurrentLine() + ", column " +
									scanner.getCurrentColumn();
				throw new EOFException(message);
			} else if (token.getTokenType() == CLOSE_CURLY_BRACKET) {
				bracketCount--;
			} else if (token.getTokenType() == OPEN_CURLY_BRACKET) {
				bracketCount++;
			}
		}
		writer.println("    }");
		writer.println();
	}

	/**
	 * Converts a rule into a set of java methods.
	 *
	 * @param writer the print writer used to write to the generated file.
	 * @exception IOException if some IO error occurs.
	 * @exception JeopsException if some error occurs while converting the rule.
	 */
	private void convertRule(PrintWriter writer)
								throws IOException, JeopsException {
		
		// We expect the next token to be the rule name...
		token = readNextToken();
		skipWhiteSpace(writer, false);
		checkToken(token, IDENT, "Ident expected");
		ruleName = token.getLexeme();
		ruleNames.addElement(ruleName);

		token = readNextToken();
		skipWhiteSpace(writer, false);
		checkToken(token, OPEN_CURLY_BRACKET, "'{' expected");

		int countBrackets = 1;

		token = readNextToken();
		skipWhiteSpace(writer, false);
		checkToken(token, DECLARATIONS, "'declarations' expected");
		convertRuleDeclarations(writer);
		convertRulePreconditions(writer);
		convertRuleActions(writer);
		
	}

	/**
	 * Processes the end of the rule base, adding control information.
	 *
	 * @param writer the print writer used to write to the generated file.
	 * @exception IOException if some IO error occurs.
	 */
	private void processEndOfRuleBase(PrintWriter writer) throws IOException {
		
		// Method used to return the names of the rules
		writer.println();
		writer.println("    /**");
		writer.println("     * Returns the name of the rules in this class file.");
		writer.println("     *");
		writer.println("     * @return the name of the rules in this class file.");
		writer.println("     */");
		writer.println("    public String[] getRuleNames() {");
		writer.println("        int numberOfRules = " + ruleNames.size() + ";");
		writer.println("    	String[] result = new String[numberOfRules];");
		for (int i = 0; i < ruleNames.size(); i++) {
			String name = (String) ruleNames.elementAt(i);
			writer.println("        result["+i+"] = \"" + name + "\";");
		}
		writer.println("        return result;");
		writer.println("    }");

		// Method used to return the number of declarations of the rules
		writer.println();
		writer.println("    /**");
		writer.println("     * Returns the number of declarations of the rules in this class file.");
		writer.println("     *");
		writer.println("     * @return the number of declarations  of the rules in this class file.");
		writer.println("     */");
		writer.println("    public int[] getNumberOfDeclarations() {");
		writer.println("        int numberOfRules = " + numberOfDeclarations.size() + ";");
		writer.println("    	int[] result = new int[numberOfRules];");
		for (int i = 0; i < numberOfDeclarations.size(); i++) {
			int number = ((Integer) numberOfDeclarations.elementAt(i)).intValue();
			writer.println("        result["+i+"] = " + number + ";");
		}
		writer.println("        return result;");
		writer.println("    }");

		// Method used to return the number of local declarations of the rules
		writer.println();
		writer.println("    /**");
		writer.println("     * Returns the number of local declarations of the rules in this class file.");
		writer.println("     *");
		writer.println("     * @return the number of local declarations  of the rules in this class file.");
		writer.println("     */");
		writer.println("    public int[] getNumberOfLocalDeclarations() {");
		writer.println("        int numberOfRules = " + numberOfLocalDeclarations.size() + ";");
		writer.println("    	int[] result = new int[numberOfRules];");
		for (int i = 0; i < numberOfLocalDeclarations.size(); i++) {
			int number = ((Integer) numberOfLocalDeclarations.elementAt(i)).intValue();
			writer.println("        result["+i+"] = " + number + ";");
		}
		writer.println("        return result;");
		writer.println("    }");

		// Method used to return the number of preconditions of the rules
		writer.println();
		writer.println("    /**");
		writer.println("     * Returns the number of preconditions of the rules in this class file.");
		writer.println("     *");
		writer.println("     * @return the number of preconditions  of the rules in this class file.");
		writer.println("     */");
		writer.println("    public int[] getNumberOfPreconditions() {");
		writer.println("        int numberOfRules = " + numberOfPreconditions.size() + ";");
		writer.println("    	int[] result = new int[numberOfRules];");
		for (int i = 0; i < numberOfPreconditions.size(); i++) {
			int number = ((Integer) numberOfPreconditions.elementAt(i)).intValue();
			writer.println("        result["+i+"] = " + number + ";");
		}
		writer.println("        return result;");
		writer.println("    }");

		// Method used as an entry point for the verification of the rules' preconditions
		writer.println();
		writer.println("    /**");
		writer.println("     * Checks whether some preconditions of some rule is satisfied.");
		writer.println("     *");
		writer.println("     * @param ruleIndex the index of the rule to be checked");
		writer.println("     * @param precIndex the index of the precondition to be checked");
		writer.println("     * @return <code>true</code> if the corresponding precondition for the");
		writer.println("     *          given rule is satisfied. <code>false</code> otherwise.");
		writer.println("     */");
		writer.println("    public boolean checkPrecondition(int ruleIndex, int precIndex) {");
		writer.println("        switch (ruleIndex) {");
		for (int i = 0; i < ruleNames.size(); i++) {
			String name = (String) ruleNames.elementAt(i);
			writer.println("            case " + i + ": return " + name + "_prec(precIndex);");
		}
		writer.println("            default: return false;");
		writer.println("        }");
		writer.println("    }");

		// Method used as an entry point for the verification of the rules'
		// preconditions based on the declared identifiers.
		writer.println();
		writer.println("    /**");
		writer.println("     * Checks whether all the preconditions of a rule which");
		writer.println("     * reference only the elements declared up to the given index");
		writer.println("     * are true.");
		writer.println("     *");
		writer.println("     * @param ruleIndex the index of the rule to be checked");
		writer.println("     * @param declIndex the index of the declaration to be checked");
		writer.println("     * @return <code>true</code> if all the preconditions of a rule which");
		writer.println("     *          reference only the elements declared up to the given index");
		writer.println("     *          are satisfied; <code>false</code> otherwise.");
		writer.println("     */");
		writer.println("    public boolean checkPrecForDeclaration(int ruleIndex, int declIndex) {");
		writer.println("        switch (ruleIndex) {");
		for (int i = 0; i < ruleNames.size(); i++) {
			String name = (String) ruleNames.elementAt(i);
			writer.println("            case " + i + ": return checkPrecForDeclaration_" + name + "(declIndex);");
		}
		writer.println("            default: return false;");
		writer.println("        }");
		writer.println("    }");

		// Method used to return the class name of an object declared in a rule.
		writer.println();
		writer.println("    /**");
		writer.println("     * Returns the class name of an object declared in a rule.");
		writer.println("     *");
		writer.println("     * @param ruleIndex the index of the rule");
		writer.println("     * @param declIndex the index of the declaration");
		writer.println("     * @return the class name of the declared object.");
		writer.println("     */");
		writer.println("    public String getDeclaredClassName(int ruleIndex, int declIndex) {");
		writer.println("        switch (ruleIndex) {");
		for (int i = 0; i < ruleNames.size(); i++) {
			String name = (String) ruleNames.elementAt(i);
			writer.println("            case " + i + ": return getDeclaredClassName_" + name + "(declIndex);");
		}
		writer.println("            default: return null;");
		writer.println("        }");
		writer.println("    }");

		// Method used to return the class of an object declared in a rule.
		writer.println();
		writer.println("    /**");
		writer.println("     * Returns the class of an object declared in a rule.");
		writer.println("     *");
		writer.println("     * @param ruleIndex the index of the rule");
		writer.println("     * @param declIndex the index of the declaration");
		writer.println("     * @return the class of the declared object.");
		writer.println("     */");
		writer.println("    public Class getDeclaredClass(int ruleIndex, int declIndex) {");
		writer.println("        switch (ruleIndex) {");
		for (int i = 0; i < ruleNames.size(); i++) {
			String name = (String) ruleNames.elementAt(i);
			writer.println("            case " + i + ": return getDeclaredClass_" + name + "(declIndex);");
		}
		writer.println("            default: return null;");
		writer.println("        }");
		writer.println("    }");

		// Method used as an entry point for firing the rules
		writer.println();
		writer.println("    /**");
		writer.println("     * Fires one of the rules in this rule base.");
		writer.println("     *");
		writer.println("     * @param ruleIndex the index of the rule to be fired");
		writer.println("     */");
		writer.println("    protected void internalFireRule(int ruleIndex) {");
		writer.println("        switch (ruleIndex) {");
		for (int i = 0; i < ruleNames.size(); i++) {
			String name = (String) ruleNames.elementAt(i);
			writer.println("            case " + i + ": " + name + "(); break;");
		}
		writer.println("        }");
		writer.println("    }");

		// Auxiliar method that returns the number of rules
		writer.println();
		writer.println("    /**");
		writer.println("     * Returns the number of rules.");
		writer.println("     *");
		writer.println("     * @return the number of rules.");
		writer.println("     */");
		writer.println("    public int getNumberOfRules() {");
		writer.println("        return " + ruleNames.size() + ";");
		writer.println("    }");

		// Method used to return the mapping between declarations and values.
		writer.println();
		writer.println("    /**");
		writer.println("     * Returns a table that maps each declared identifier of a rule");
		writer.println("     * into its corresponding value.");
		writer.println("     *");
		writer.println("     * @param ruleIndex the index of the rule");
		writer.println("     * @return a table that maps each declared identifier of a rule");
		writer.println("     *          into its corresponding value.");
		writer.println("     */");
		writer.println("    public java.util.Hashtable getInternalRuleMapping(int ruleIndex) {");
		writer.println("        switch (ruleIndex) {");
		for (int i = 0; i < ruleNames.size(); i++) {
			String name = (String) ruleNames.elementAt(i);
			writer.println("            case " + i + ": return getMapRule" + name + "();");
		}
		writer.println("            default: return new java.util.Hashtable();");
		writer.println("        }");
		writer.println("    }");

		// Method used to set the value of the declared objects.
		writer.println();
		writer.println("    /**");
		writer.println("     * Sets an object that represents a declaration of some rule.");
		writer.println("     *");
		writer.println("     * @param ruleIndex the index of the rule");
		writer.println("     * @param declIndex the index of the declaration in the rule.");
		writer.println("     * @param value the value of the object being set.");
		writer.println("     */");
		writer.println("    public void setObject(int ruleIndex, int declIndex, Object value) {");
		writer.println("        switch (ruleIndex) {");
		for (int i = 0; i < ruleNames.size(); i++) {
			String name = (String) ruleNames.elementAt(i);
			writer.println("            case " + i + ": setObject_" + name + "(declIndex, value); break;");
		}
		writer.println("        }");
		writer.println("    }");

		// Method used to set the value of the locally declared objects.
//		writer.println();
//		writer.println("    /**");
//		writer.println("     * Sets all the locally declared objects for a given rule.");
//		writer.println("     *");
//		writer.println("     * @param ruleIndex the index of the rule");
//		writer.println("     */");
//		writer.println("    protected void setLocalObjects(int ruleIndex) {");
//		writer.println("        int numberOfDecls = getNumberOfDeclarations()[ruleIndex];");
//		writer.println("        switch (ruleIndex) {");
//		for (int i = 0; i < ruleNames.size(); i++) {
//			String name = (String) ruleNames.elementAt(i);
//			writer.println("            case " + i + ":");
//			writer.println("                for (int i = 0; i < numberOfDecls; i++) {");
//			writer.println("                    setLocalDeclarations_" + name + "(i);");
//			writer.println("                }");
//			writer.println("                break;");
//		}
//		writer.println("        }");
//		writer.println("    }");

		// Method used to get the value of some declared object.
		writer.println();
		writer.println("    /**");
		writer.println("     * Returns an object that represents a declaration of some rule.");
		writer.println("     *");
		writer.println("     * @param ruleIndex the index of the rule");
		writer.println("     * @param declIndex the index of the declaration in the rule.");
		writer.println("     * @return the value of the corresponding object.");
		writer.println("     */");
		writer.println("    public Object getObject(int ruleIndex, int declIndex) {");
		writer.println("        switch (ruleIndex) {");
		for (int i = 0; i < ruleNames.size(); i++) {
			String name = (String) ruleNames.elementAt(i);
			writer.println("            case " + i + ": return getObject_" + name + "(declIndex);");
		}
		writer.println("            default: return null;");
		writer.println("        }");
		writer.println("    }");

		// Method used to get the dependency between local and normal declarations.
		writer.println();
		writer.println("    /**");
		writer.println("     * Returns a dependency table between the local declarations and the");
		writer.println("     * regular ones.");
		writer.println("     *");
		writer.println("     * @param ruleIndex the index of the rule");
		writer.println("     * @return a two-dimensional array with the dependencies between the");
		writer.println("     *          local declarations and the regular ones.");
		writer.println("     */");
		writer.println("    public Object[][] getLocalDeclarationDependency(int ruleIndex) {");
		writer.println("        switch (ruleIndex) {");
		for (int i = 0; i < ruleNames.size(); i++) {
			String name = (String) ruleNames.elementAt(i);
			writer.println("            case " + i + ": return getLocalDeclarationDependency_" + name + "();");
		}
		writer.println("            default: return null;");
		writer.println("        }");
		writer.println("    }");

		// Method used by the knwoledge base to set the value all locally
		// declared objects for some rule.
		writer.println("    /**");
		writer.println("     * Defines all variables bound to the local declarations of");
		writer.println("     * some rule.");
		writer.println("     *");
		writer.println("     * @param ruleIndex the index of the rule");
		writer.println("     * @param objects a 2-dimensional object array whose first column");
		writer.println("     *          contains of the variables bound to the local declarations.");
		writer.println("     *          of some rule.");
		writer.println("     */");
		writer.println("    protected void setLocalObjects(int ruleIndex, Object[][] objects) {");
		writer.println("        switch (ruleIndex) {");
		for (int i = 0; i < ruleNames.size(); i++) {
			String name = (String) ruleNames.elementAt(i);
			writer.println("            case " + i + ": setLocalObjects_" + name + "(objects); break;");
		}
		writer.println("        }");
		writer.println("    }");

		// Method used by the knwoledge base to get the value all
		// declared objects for some rule.
		writer.println("    /**");
		writer.println("     * Returns all variables bound to the declarations of");
		writer.println("     * some rule.");
		writer.println("     *");
		writer.println("     * @param ruleIndex the index of the rule");
		writer.println("     * @return an object array of the variables bound to the");
		writer.println("     *          declarations of some rule.");
		writer.println("     */");
		writer.println("    protected Object[] getObjects(int ruleIndex) {");
		writer.println("        switch (ruleIndex) {");
		for (int i = 0; i < ruleNames.size(); i++) {
			String name = (String) ruleNames.elementAt(i);
			writer.println("            case " + i + ": return getObjects_" + name + "();");
		}
		writer.println("            default: return null;");
		writer.println("        }");
		writer.println("    }");

		// Method used by the knwoledge base to set the value all
		// declared objects for some rule.
		writer.println("    /**");
		writer.println("     * Defines all variables bound to the declarations of");
		writer.println("     * some rule.");
		writer.println("     *");
		writer.println("     * @param ruleIndex the index of the rule");
		writer.println("     * @param objects an object array of the variables bound to the");
		writer.println("     *          declarations of some rule.");
		writer.println("     */");
		writer.println("    protected void setObjects(int ruleIndex, Object[] objects) {");
		writer.println("        switch (ruleIndex) {");
		for (int i = 0; i < ruleNames.size(); i++) {
			String name = (String) ruleNames.elementAt(i);
			writer.println("            case " + i + ": setObjects_" + name + "(objects); break;");
		}
		writer.println("        }");
		writer.println("    }");

		// Method used to return the number of declared objects for each class
		writer.println();
		writer.println("    /**");
		writer.println("     * Returns a mapping from the classes of the objects declared in the");
		writer.println("     * rules to the number of occurrences of each one.");
		writer.println("     *");
		writer.println("     * @return a mapping from the classes of the objects declared in the");
		writer.println("     *          rules to the number of occurrences of each one.");
		writer.println("     */");
		writer.println("    public java.util.Hashtable getClassDeclarationCount() {");
		writer.println("        java.util.Hashtable result = new java.util.Hashtable();");
		for (Enumeration e = classDeclarationCount.keys(); e.hasMoreElements(); ) {
			Object obj = e.nextElement();
			writer.println("        result.put(\"" + obj.toString() + "\", new Integer(" +
										classDeclarationCount.get(obj) + "));");
		}
		writer.println("        return result;");
		writer.println("    }");

		// Adds the declared variables.
		writer.println();
		writer.println("    /*");
		writer.println("     * The variables declared in the preconditions.");
		writer.println("     */");
		for (int i = 0; i < variables.size(); i++) {
			writer.print("    ");
			writer.println((String) variables.elementAt(i));
		}

		// Adds some useful constants and variables.
		writer.println();
		writer.println("    /**");
		writer.println("     * Constant used to aviod the creation of several empty arrays.");
		writer.println("     */");
		writer.println("    private static final Object[] EMPTY_OBJECT_ARRAY = new Object[0];");
		writer.println();
		writer.println("    /**");
		writer.println("     * Constant used to aviod the creation of several empty double arrays.");
		writer.println("     */");
		writer.println("    private static final Object[][] EMPTY_OBJECT_DOUBLE_ARRAY = new Object[0][0];");
		writer.println();
		writer.println("    /**");
		writer.println("     * Constant used to aviod the creation of several empty vectors.");
		writer.println("     */");
		writer.println("    private static final java.util.Vector EMPTY_VECTOR = new java.util.Vector();");


//		for (int i = 0; i < ruleNames.size(); i++) {
//			String name = (String) ruleNames.elementAt(i);
//			writer.println();
//			writer.println("    /**");
//			writer.println("     * Auxiliar 2-dimensional array used to store the dependencies");
//			writer.println("     * between the local and the normal declarations of rule " + name);
//			writer.println("     */");
//			writer.println("    private Object[][] declDependency_" + name + ";");
//			writer.println();
//			writer.println("    /**");
//			writer.println("     * Auxiliar array used to return the objects bound to the");
//			writer.println("     * declarations of rule " + name);
//			writer.println("     */");
//			writer.println("    private Object[] objects_" + name + ";");
//		}

		// Adds the constructor.
		writer.println();
		writer.println("    /**");
		writer.println("     * Class constructor.");
		writer.println("     *");
		writer.println("     * @param objectBase an object base over with this rule base will work.");
		writer.println("     * @param knowledgeBase the knowledge base that contains this rule base.");
		writer.println("     */");
		writer.println("    public Jeops_RuleBase_" + ruleBaseName + "(jeops.ObjectBase objectBase,");
		writer.println("                                jeops.AbstractKnowledgeBase knowledgeBase) {");
		writer.println("        super(objectBase, knowledgeBase);");
		writer.println("    }");

		// A last empty line...
		writer.println();
	}

	/**
	 * Processes an import statement of a rule base.
	 *
	 * @param writer the print writer used to write to the generated file.
	 * @exception IOException if some IO error occurs.
	 * @exception JeopsException if some error occurs while converting the rule.
	 */
	private void processImport(PrintWriter writer)
								throws IOException, JeopsException {
		StringBuffer statement = new StringBuffer();
		writer.print(token.getLexeme());
		token = readNextToken();
		skipWhiteSpace(writer, true);
		while (token.getTokenType() == IDENT ||
								token.getTokenType() == DOT ||
								token.getTokenType() == ASTERISK) {
			writer.print(token.getLexeme());
			statement.append(token.getLexeme());
			token = readNextToken();
		}
		skipWhiteSpace(writer, true);
		checkToken(token, SEMICOLON, "\";\" expected");
		importList.addImport(statement.toString());
		writer.print(token.getLexeme());
	}

	/**
	 * Processes the package statement of a rule base.
	 *
	 * @param writer the print writer used to write to the generated file.
	 * @exception IOException if some IO error occurs.
	 * @exception JeopsException if some error occurs while converting the rule.
	 */
	private void processPackage(PrintWriter writer)
								throws IOException, JeopsException {
		StringBuffer packageName = new StringBuffer();
		writer.print(token.getLexeme());
		token = readNextToken();
		skipWhiteSpace(writer, true);
		while (token.getTokenType() == IDENT ||
								token.getTokenType() == DOT) {
			writer.print(token.getLexeme());
			packageName.append(token.getLexeme());
			token = readNextToken();
		}
		skipWhiteSpace(writer, true);
		checkToken(token, SEMICOLON, "\";\" expected");
		importList = new ImportList(packageName.toString());
		writer.print(token.getLexeme());
	}

	/**
	 * Creates a java source file that behaves as defined in the rules file.
	 *
	 * @exception IOException if some IO error occurs.
	 * @exception JeopsException if some error occurs while converting the rule.
	 */
	public void convert() throws IOException, JeopsException {
		File file = new File("_istmp.tmp");
		FileWriter fw = new FileWriter(file);
		PrintWriter pw = new PrintWriter(fw);
		
		int openBracketCount = 0;
		boolean toPrint;
		boolean inRuleBaseDeclaration = false;
		boolean inRuleDeclaration = false;
		boolean inRuleBase = false;
		boolean inRule = false;
		boolean thereIsExtends = false;
		boolean thereIsPublic = false;
		token = readNextToken();
		while (token.getTokenType() != EOF) {
			toPrint = false;
			switch (token.getTokenType()) {
				case ERROR:
				{
					throw new JeopsException("Error: " + token.getLexeme(),
												scanner.getCurrentLine(),
												scanner.getCurrentColumn());
				}
				case OPEN_CURLY_BRACKET:
				{
					openBracketCount++;
					if (openBracketCount == 1) {
						inRuleBase = true;
					}
					if (inRuleBaseDeclaration) {
						if (!thereIsExtends) {
							pw.print("extends jeops.AbstractRuleBase ");
						}
						inRuleBaseDeclaration = false;
					}
					toPrint = true;
					break;
				}
				case CLOSE_CURLY_BRACKET:
				{
					openBracketCount--;
					if (openBracketCount == 0) {
						inRuleBase = false;
						processEndOfRuleBase(pw);
					} else if (openBracketCount == 1) {
						inRule = false;
					}
					toPrint = true;
					break;
				}
				case IDENT:
				{
					toPrint = true;
					break;
				}
				case RULE_BASE:
				{
					inRuleBaseDeclaration = true;
					thereIsRuleBase = true;
					token = readNextToken();
					skipWhiteSpace(pw, true);
					checkToken(token, IDENT, "identifier expected");
					ruleBaseName = token.getLexeme();
					pw.print("class Jeops_RuleBase_" + ruleBaseName);
					break;
				}
				case PUBLIC:
				{
					toPrint = false; // So' ha' uma classe publica: a base de regras.
					break;
				}
				case RULE:
				{
					convertRule(pw);
					break;
				}
				case EXTENDS:
				{
					toPrint = true;
					thereIsExtends = true;
					break;
				}
				case IMPORT:
				{
					toPrint = false;
					processImport(pw);
					break;
				}
				case PACKAGE:
				{
					toPrint = false;
					processPackage(pw);
					break;
				}
				default:
				{
					toPrint = true;
					break;
				}
			}
			if (toPrint) {
				pw.print(token.getLexeme());
			}
			token = readNextToken();
		}
		
		// We're at the end of the file; now that the rule base class is
		// already defined, let's define the knowledge base itself.
		pw.println();
		pw.println("/**");
		pw.println(" * Knowledge base created by JEOPS from file " + ruleFileName);
		pw.println(" *");
		Date now = new Date();
		DateFormat dateFormat = DateFormat.getDateInstance();
		pw.println(" * @version " + dateFormat.format(now));
		pw.println(" */");
		pw.println("public class " + ruleBaseName + " extends jeops.AbstractKnowledgeBase {");
		pw.println();
		pw.println("    /**");
		pw.println("     * Creates a new knowledge base with the specified conflict set with the");
		pw.println("     * desired conflict resolution policy.");
		pw.println("     *");
		pw.println("     * @param conflictSet a conflict set with the desired resolution policy");
		pw.println("     */");
		pw.println("    public " + ruleBaseName + "(jeops.conflict.ConflictSet conflictSet) {");
		pw.println("        super(conflictSet);");
		pw.println("        jeops.AbstractRuleBase ruleBase;");
		pw.println("        jeops.ObjectBase objectBase = getObjectBase();");
		pw.println("        ruleBase = new Jeops_RuleBase_" + ruleBaseName + "(objectBase, this);");
		pw.println("        setRuleBase(ruleBase);");
		pw.println("    }");
		pw.println();
		pw.println("    /**");
		pw.println("     * Creates a new knowledge base, using the default conflict resolution");
		pw.println("     * policy.");
		pw.println("     */");
		pw.println("    public " + ruleBaseName + "() {");
		pw.println("        this(new jeops.conflict.DefaultConflictSet());");
		pw.println("    }");
		pw.println();
		pw.println("}");

		pw.flush();
		pw.close();
		fw.close();
		
		if (!thereIsRuleBase) {
			throw new JeopsException("There should be a rule base in the input file!",
												scanner.getCurrentLine(),
												scanner.getCurrentColumn());
		}
		int separatorIndex = javaFileName.lastIndexOf(File.separatorChar);
		javaFileName = javaFileName.substring(0, separatorIndex + 1) +
									ruleBaseName + ".java";
		File finalFile = new File(javaFileName);
		if (finalFile.exists()) {
			finalFile.delete();
		}
		file.renameTo(finalFile);
	}

	/**
	 * Test method for this class.
	 *
	 * @param args command-line arguments. None is needed, but
	 *          one can pass the rule file name for the converting.
	 */
	public static void main(String[] args) {
		try {
			if (args.length == 0) {
				System.out.println("JEOPS v2.0 beta 1");
				System.out.println("Usage: java jeops.compiler.Main <file.rules>");
				return;
			}
			String fileName = args[0];
			if (fileName.lastIndexOf(".rules") == -1) {
				System.out.println("Error: the file must have a \".rules\" extension.");
				return;
			}
			Main j = new Main(fileName);
			j.convert();
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
		} catch (JeopsException e) {
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
		}
	}

}
