/*
 * JEOPS - The Java Embedded Object Production System
 * Copyright (c) 2000   Carlos Figueira Filho
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: Carlos Figueira Filho (csff@cin.ufpe.br)
 */

package jeops.compiler;

/**
 * An exception thrown by the import list when an identifier can represent
 * more than one class (ambiguity).
 *
 * @author Carlos Figueira Filho (<a href="mailto:csff@di.ufpe.br">csff@di.ufpe.br</a>)
 * @version 0.01  05.04.2000
 */
public class ImportException extends Exception {

	/**
	 * Constructs an ImportException with no detail message.
	 */
	public ImportException() {
		super();
	}

	/**
	 * Constructs an ImportException with the specified detail message.
	 *
	 * @param s the detail message.
	 */
	public ImportException(String s) {
		super(s);
	}

}
