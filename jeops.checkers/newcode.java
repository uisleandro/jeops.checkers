		Cell cell0 = new Cell(0,0);
		cell0.set_black(true);
		cell0.set_blocked_bot_left(false);
		cell0.set_blocked_bot_right(false);
		cell0.set_blocked_top_left(false);
		cell0.set_blocked_top_right(false);
		cell0.set_checker(new Checker(true));
		cell0.set_checker(new Checker(null));
		kb.tell(cell0);

		Cell cell2 = new Cell(0,2);
		cell2.set_black(false);
		cell2.set_blocked_bot_left(false);
		cell2.set_blocked_bot_right(false);
		cell2.set_blocked_top_left(false);
		cell2.set_blocked_top_right(false);
		cell2.set_checker(new Checker(true));
		cell2.set_checker(new Checker(null));
		kb.tell(cell2);

		Cell cell4 = new Cell(0,4);
		cell4.set_black(false);
		cell4.set_blocked_bot_left(false);
		cell4.set_blocked_bot_right(false);
		cell4.set_blocked_top_left(false);
		cell4.set_blocked_top_right(false);
		cell4.set_checker(new Checker(true));
		cell4.set_checker(new Checker(null));
		kb.tell(cell4);

		Cell cell6 = new Cell(0,6);
		cell6.set_black(false);
		cell6.set_blocked_bot_left(false);
		cell6.set_blocked_bot_right(false);
		cell6.set_blocked_top_left(false);
		cell6.set_blocked_top_right(false);
		cell6.set_checker(new Checker(true));
		cell6.set_checker(new Checker(null));
		kb.tell(cell6);

		Cell cell9 = new Cell(1,1);
		cell9.set_black(true);
		cell9.set_blocked_bot_left(false);
		cell9.set_blocked_bot_right(false);
		cell9.set_blocked_top_left(false);
		cell9.set_blocked_top_right(false);
		cell9.set_checker(new Checker(true));
		cell9.set_checker(new Checker(null));
		kb.tell(cell9);

		Cell cell11 = new Cell(1,3);
		cell11.set_black(false);
		cell11.set_blocked_bot_left(false);
		cell11.set_blocked_bot_right(false);
		cell11.set_blocked_top_left(false);
		cell11.set_blocked_top_right(false);
		cell11.set_checker(new Checker(true));
		cell11.set_checker(new Checker(null));
		kb.tell(cell11);

		Cell cell13 = new Cell(1,5);
		cell13.set_black(false);
		cell13.set_blocked_bot_left(false);
		cell13.set_blocked_bot_right(false);
		cell13.set_blocked_top_left(false);
		cell13.set_blocked_top_right(false);
		cell13.set_checker(new Checker(true));
		cell13.set_checker(new Checker(null));
		kb.tell(cell13);

		Cell cell15 = new Cell(1,7);
		cell15.set_black(false);
		cell15.set_blocked_bot_left(false);
		cell15.set_blocked_bot_right(false);
		cell15.set_blocked_top_left(false);
		cell15.set_blocked_top_right(false);
		cell15.set_checker(new Checker(true));
		cell15.set_checker(new Checker(null));
		kb.tell(cell15);

		Cell cell16 = new Cell(2,0);
		cell16.set_black(false);
		cell16.set_blocked_bot_left(false);
		cell16.set_blocked_bot_right(false);
		cell16.set_blocked_top_left(false);
		cell16.set_blocked_top_right(false);
		cell16.set_checker(new Checker(true));
		cell16.set_checker(new Checker(null));
		kb.tell(cell16);

		Cell cell18 = new Cell(2,2);
		cell18.set_black(true);
		cell18.set_blocked_bot_left(false);
		cell18.set_blocked_bot_right(false);
		cell18.set_blocked_top_left(false);
		cell18.set_blocked_top_right(false);
		cell18.set_checker(new Checker(true));
		cell18.set_checker(new Checker(null));
		kb.tell(cell18);

		Cell cell20 = new Cell(2,4);
		cell20.set_black(false);
		cell20.set_blocked_bot_left(false);
		cell20.set_blocked_bot_right(false);
		cell20.set_blocked_top_left(false);
		cell20.set_blocked_top_right(false);
		cell20.set_checker(new Checker(true));
		cell20.set_checker(new Checker(null));
		kb.tell(cell20);

		Cell cell22 = new Cell(2,6);
		cell22.set_black(false);
		cell22.set_blocked_bot_left(false);
		cell22.set_blocked_bot_right(false);
		cell22.set_blocked_top_left(false);
		cell22.set_blocked_top_right(false);
		cell22.set_checker(new Checker(true));
		cell22.set_checker(new Checker(null));
		kb.tell(cell22);

		Cell cell25 = new Cell(3,1);
		cell25.set_black(false);
		cell25.set_blocked_bot_left(false);
		cell25.set_blocked_bot_right(false);
		cell25.set_blocked_top_left(false);
		cell25.set_blocked_top_right(false);
		kb.tell(cell25);

		Cell cell27 = new Cell(3,3);
		cell27.set_black(true);
		cell27.set_blocked_bot_left(false);
		cell27.set_blocked_bot_right(false);
		cell27.set_blocked_top_left(false);
		cell27.set_blocked_top_right(false);
		kb.tell(cell27);

		Cell cell29 = new Cell(3,5);
		cell29.set_black(false);
		cell29.set_blocked_bot_left(false);
		cell29.set_blocked_bot_right(false);
		cell29.set_blocked_top_left(false);
		cell29.set_blocked_top_right(false);
		kb.tell(cell29);

		Cell cell31 = new Cell(3,7);
		cell31.set_black(false);
		cell31.set_blocked_bot_left(false);
		cell31.set_blocked_bot_right(false);
		cell31.set_blocked_top_left(false);
		cell31.set_blocked_top_right(false);
		kb.tell(cell31);

		Cell cell32 = new Cell(4,0);
		cell32.set_black(false);
		cell32.set_blocked_bot_left(false);
		cell32.set_blocked_bot_right(false);
		cell32.set_blocked_top_left(false);
		cell32.set_blocked_top_right(false);
		kb.tell(cell32);

		Cell cell34 = new Cell(4,2);
		cell34.set_black(false);
		cell34.set_blocked_bot_left(false);
		cell34.set_blocked_bot_right(false);
		cell34.set_blocked_top_left(false);
		cell34.set_blocked_top_right(false);
		kb.tell(cell34);

		Cell cell36 = new Cell(4,4);
		cell36.set_black(true);
		cell36.set_blocked_bot_left(false);
		cell36.set_blocked_bot_right(false);
		cell36.set_blocked_top_left(false);
		cell36.set_blocked_top_right(false);
		kb.tell(cell36);

		Cell cell38 = new Cell(4,6);
		cell38.set_black(false);
		cell38.set_blocked_bot_left(false);
		cell38.set_blocked_bot_right(false);
		cell38.set_blocked_top_left(false);
		cell38.set_blocked_top_right(false);
		kb.tell(cell38);

		Cell cell41 = new Cell(5,1);
		cell41.set_black(false);
		cell41.set_blocked_bot_left(false);
		cell41.set_blocked_bot_right(false);
		cell41.set_blocked_top_left(false);
		cell41.set_blocked_top_right(false);
		cell41.set_checker(new Checker(false));
		cell41.set_checker(new Checker(null));
		kb.tell(cell41);

		Cell cell43 = new Cell(5,3);
		cell43.set_black(false);
		cell43.set_blocked_bot_left(false);
		cell43.set_blocked_bot_right(false);
		cell43.set_blocked_top_left(false);
		cell43.set_blocked_top_right(false);
		cell43.set_checker(new Checker(false));
		cell43.set_checker(new Checker(null));
		kb.tell(cell43);

		Cell cell45 = new Cell(5,5);
		cell45.set_black(true);
		cell45.set_blocked_bot_left(false);
		cell45.set_blocked_bot_right(false);
		cell45.set_blocked_top_left(false);
		cell45.set_blocked_top_right(false);
		cell45.set_checker(new Checker(false));
		cell45.set_checker(new Checker(null));
		kb.tell(cell45);

		Cell cell47 = new Cell(5,7);
		cell47.set_black(false);
		cell47.set_blocked_bot_left(false);
		cell47.set_blocked_bot_right(false);
		cell47.set_blocked_top_left(false);
		cell47.set_blocked_top_right(false);
		cell47.set_checker(new Checker(false));
		cell47.set_checker(new Checker(null));
		kb.tell(cell47);

		Cell cell48 = new Cell(6,0);
		cell48.set_black(false);
		cell48.set_blocked_bot_left(false);
		cell48.set_blocked_bot_right(false);
		cell48.set_blocked_top_left(false);
		cell48.set_blocked_top_right(false);
		cell48.set_checker(new Checker(false));
		cell48.set_checker(new Checker(null));
		kb.tell(cell48);

		Cell cell50 = new Cell(6,2);
		cell50.set_black(false);
		cell50.set_blocked_bot_left(false);
		cell50.set_blocked_bot_right(false);
		cell50.set_blocked_top_left(false);
		cell50.set_blocked_top_right(false);
		cell50.set_checker(new Checker(false));
		cell50.set_checker(new Checker(null));
		kb.tell(cell50);

		Cell cell52 = new Cell(6,4);
		cell52.set_black(false);
		cell52.set_blocked_bot_left(false);
		cell52.set_blocked_bot_right(false);
		cell52.set_blocked_top_left(false);
		cell52.set_blocked_top_right(false);
		cell52.set_checker(new Checker(false));
		cell52.set_checker(new Checker(null));
		kb.tell(cell52);

		Cell cell54 = new Cell(6,6);
		cell54.set_black(true);
		cell54.set_blocked_bot_left(false);
		cell54.set_blocked_bot_right(false);
		cell54.set_blocked_top_left(false);
		cell54.set_blocked_top_right(false);
		cell54.set_checker(new Checker(false));
		cell54.set_checker(new Checker(null));
		kb.tell(cell54);

		Cell cell57 = new Cell(7,1);
		cell57.set_black(false);
		cell57.set_blocked_bot_left(false);
		cell57.set_blocked_bot_right(false);
		cell57.set_blocked_top_left(false);
		cell57.set_blocked_top_right(false);
		cell57.set_checker(new Checker(false));
		cell57.set_checker(new Checker(null));
		kb.tell(cell57);

		Cell cell59 = new Cell(7,3);
		cell59.set_black(false);
		cell59.set_blocked_bot_left(false);
		cell59.set_blocked_bot_right(false);
		cell59.set_blocked_top_left(false);
		cell59.set_blocked_top_right(false);
		cell59.set_checker(new Checker(false));
		cell59.set_checker(new Checker(null));
		kb.tell(cell59);

		Cell cell61 = new Cell(7,5);
		cell61.set_black(false);
		cell61.set_blocked_bot_left(false);
		cell61.set_blocked_bot_right(false);
		cell61.set_blocked_top_left(false);
		cell61.set_blocked_top_right(false);
		cell61.set_checker(new Checker(false));
		cell61.set_checker(new Checker(null));
		kb.tell(cell61);


		Cell cell63 = new Cell(7,7);
		cell63.set_black(true);
		cell63.set_blocked_bot_left(false);
		cell63.set_blocked_bot_right(false);
		cell63.set_blocked_top_left(false);
		cell63.set_blocked_top_right(false);
		cell63.set_checker(new Checker(false));
		cell63.set_checker(new Checker(null));
		kb.tell(cell63);


