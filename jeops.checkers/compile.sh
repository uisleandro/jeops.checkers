#!/bin/sh

JAVA=/usr/local/bin/java/jdk1.8.0_77/bin/java
JAVAC=/usr/local/bin/java/jdk1.8.0_77/bin/javac

CP=./src/main/resources/jeops2.0.jar:./target/classes;
D=./target/classes


echo "compila a base"
$JAVAC -cp $CP ./src/main/java/com/uisleandro/jeops/checkers/CheckersBase.java -d $D

echo "compila o programa"
$JAVAC -cp $CP ./src/main/java/com/uisleandro/jeops/checkers/TestCheckers.java -d $D
