set JAVA="C:/Program Files/Java/jdk1.8.0_92/bin/java"
set JAVAC="C:/Program Files/Java/jdk1.8.0_92/bin/javac"

set CP=./src/main/resources/jeops2.0.jar;./target/classes

set D=./target/classes

echo "compilling dependences"

%JAVA%C -cp %CP% ./src/main/java/com/uisleandro/jeops/checkers/Checker.java -d %D%

%JAVA%C -cp %CP% ./src/main/java/com/uisleandro/jeops/checkers/Cell.java -d %D%

echo "generating files"

%JAVA% -cp %CP% jeops.compiler.Main ./src/main/java/com/uisleandro/jeops/checkers/CheckersBase.rules

