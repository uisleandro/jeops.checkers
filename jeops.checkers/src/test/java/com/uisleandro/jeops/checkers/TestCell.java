package com.uisleandro.jeops.checkers;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;


/**
 * Unit test for simple App.
 */
public class TestCell {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */

	Cell cell;
	Checker checker;
	
	
	
	@Before
	public void beforeTest(){
		cell = new Cell(7,7);
		checker = new Checker();
		cell.setChecker(checker);
	}
	
	
	@Test
	public void test_cell_properties(){
		
		assertNotNull(cell.getChecker());
		assertEquals(cell.getIndex(), (Integer)63);
	}
	
	@Test
	public void test_cell_properties2(){
		
		assertEquals(cell.getBlocked_bot_left(), false);
		assertEquals(cell.getBlocked_bot_right(), false);
		assertEquals(cell.getBlocked_top_left(), false);
		assertEquals(cell.getBlocked_top_right(), false);
		
		cell.setBlocked_bot_left(true);
		assertEquals(cell.getBlocked_bot_left(), true);
		assertEquals(cell.getBlocked_bot_right(), false);
		assertEquals(cell.getBlocked_top_left(), false);
		assertEquals(cell.getBlocked_top_right(), false);
		
		
		cell.setBlocked_bot_right(true);
		assertEquals(cell.getBlocked_bot_left(), true);
		assertEquals(cell.getBlocked_bot_right(), true);
		assertEquals(cell.getBlocked_top_left(), false);
		assertEquals(cell.getBlocked_top_right(), false);
		
		cell.setBlocked_top_left(true);
		assertEquals(cell.getBlocked_bot_left(), true);
		assertEquals(cell.getBlocked_bot_right(), true);
		assertEquals(cell.getBlocked_top_left(), true);
		assertEquals(cell.getBlocked_top_right(), false);
		
		cell.setBlocked_top_right(true);
		assertEquals(cell.getBlocked_bot_left(), true);
		assertEquals(cell.getBlocked_bot_right(), true);
		assertEquals(cell.getBlocked_top_left(), true);
		assertEquals(cell.getBlocked_top_right(), true);
		
	}
	
	
	
	
    @Test
    public void testApp()
    {
    	assertEquals(0, 0);
        System.out.println("this is a test");
    }
}
