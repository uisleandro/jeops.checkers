package com.uisleandro.jeops.checkers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

//Adicionar à celula
public class HumanMove{
    
	private int action;
	private int source;
	private int target;
	public static final int TODO = 0;
	public static final int DOING = 1;
	public static final int DONE = 2;
	
	public static final int WALK = 10;
	public static final int EAT = 11;
	private int status;
	
    public HumanMove(){
    	source = -1;
    	target = -1;
    	status = TODO;
    }

    
    
	public int getAction() {
		return action;
	}



	public void setAction(int action) {
		this.action = action;
	}



	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getSource() {
		return source;
	}

	public void setSource(int source) {
		this.source = source;
	}

	public int getTarget() {
		return target;
	}

	public void setTarget(int target) {
		this.target = target;
	}

	public void getInput(){
		
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
				
		try {
			System.out.println("Por Favor Insira uma acao: 0 = Andar ou 1 = Comer peca");
			
			action = 10 + Integer.parseInt(in.readLine());
			
	        System.out.println("[--][57][--][59][--][61][--][63]\n"+
	        		"[48][--][50][--][52][--][54][--]\n"+
	        		"[--][41][--][43][--][45][--][47]\n"+
	        		"[32][--][34][--][36][--][38][--]\n"+
	        		"[--][25][--][27][--][29][--][31]\n"+
	        		"[16][--][18][--][20][--][22][--]\n"+
	        		"[--][09][--][11][--][13][--][15]\n"+
	        		"[00][--][02][--][04][--][06][--]");
			
			System.out.println("Por Favor Insira a celula de origem");
			
			source = Integer.parseInt(in.readLine());
			
			System.out.println("Por Favor Insira a celula de destino");
			target = Integer.parseInt(in.readLine());
			
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
			
	}
	
	@Override
	public String toString(){
		String s = "";
		
		if(action == 10){
			s+="HUMAN_WALK(" + source + "," + target + ")";
		}
		else if (action == 11){
			s+="HUMAN_EAT(" + source + "," + target + ")";
		}
		return s;
	}
    
}