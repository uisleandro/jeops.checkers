package com.uisleandro.jeops.checkers;

public class Cell{
    
    private Integer index;
    private Integer row;
    private Integer col;
    
    //private Boolean black;
    
    private Checker checker;

    private int cell_top_left;
    private int cell_top_right;
    private int cell_bot_left;
    private int cell_bot_right;
    
    private Boolean blocked_top_right;
    private Boolean blocked_top_left;
    private Boolean blocked_bot_right;
    private Boolean blocked_bot_left;
    
    public Cell(){
        index = -1;
        row = -1;
        col = -1;
        //black = false;
        checker = null;

        blocked_top_right = false;
        blocked_top_left = false;
        blocked_bot_right = false;
        blocked_bot_left = false;
        
        cell_top_left = -1;
        cell_top_right = -1;
        cell_bot_left = -1;
        cell_bot_right = -1;
        
    }

    public Cell(int Row, int Col){
        row = Row;
        col = Col;
        index = Row * 8 + Col;
        // black = false;
        checker = null;

        blocked_top_right = false;
        blocked_top_left = false;
        blocked_bot_right = false;
        blocked_bot_left = false;

        cell_top_left = -1;
        cell_top_right = -1;
        cell_bot_left = -1;
        cell_bot_right = -1;
    }
    
    public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public Integer getRow() {
		return row;
	}

	public void setRow(Integer row) {
		this.row = row;
	}

	public Integer getCol() {
		return col;
	}

	public void setCol(Integer col) {
		this.col = col;
	}

	/*
	public Boolean getBlack() {
		return black;
	}

	public void setBlack(Boolean black) {
		this.black = black;
	}
	*/

	public Checker getChecker() {
		return checker;
	}

	public void setChecker(Checker checker) {
		this.checker = checker;
	}

	public int getCell_top_left() {
		return cell_top_left;
	}

	public void setCell_top_left(int cell_top_left) {
		this.cell_top_left = cell_top_left;
	}

	public int getCell_top_right() {
		return cell_top_right;
	}

	public void setCell_top_right(int cell_top_right) {
		this.cell_top_right = cell_top_right;
	}

	public int getCell_bot_left() {
		return cell_bot_left;
	}

	public void setCell_bot_left(int cell_bot_left) {
		this.cell_bot_left = cell_bot_left;
	}

	public int getCell_bot_right() {
		return cell_bot_right;
	}

	public void setCell_bot_right(int cell_bot_right) {
		this.cell_bot_right = cell_bot_right;
	}
    
    
    public Boolean getBlocked_top_right() {
		return blocked_top_right;
	}

	public void setBlocked_top_right(Boolean blocked_top_right) {
		this.blocked_top_right = blocked_top_right;
	}

	public Boolean getBlocked_top_left() {
		return blocked_top_left;
	}

	public void setBlocked_top_left(Boolean blocked_top_left) {
		this.blocked_top_left = blocked_top_left;
	}

	public Boolean getBlocked_bot_right() {
		return blocked_bot_right;
	}

	public void setBlocked_bot_right(Boolean blocked_bot_right) {
		this.blocked_bot_right = blocked_bot_right;
	}

	public Boolean getBlocked_bot_left() {
		return blocked_bot_left;
	}

	public void setBlocked_bot_left(Boolean blocked_bot_left) {
		this.blocked_bot_left = blocked_bot_left;
	}


    
    
	public String describe(){
    	
    	String s = "";
    	
    	s+="Cell cell"+getIndex()+" = new Cell("+row+","+col+");\n";
    	//s+="cell"+getIndex()+".set_black("+black+");\n";
		s+="cell"+getIndex()+".set_blocked_bot_left("+getBlocked_bot_left()+");\n";
		s+="cell"+getIndex()+".set_blocked_bot_right("+getBlocked_bot_right()+");\n";
		s+="cell"+getIndex()+".set_blocked_top_left("+getBlocked_top_left()+");\n";
		s+="cell"+getIndex()+".set_blocked_top_right("+getBlocked_top_right()+");\n";
		
		if(checker != null){
    		s+="cell"+getIndex()+".setChecker(new Checker("+checker.getBlack()+"));\n";	
    	}
    	
    	if(checker != null){
    		s+="cell"+getIndex()+".setChecker(new Checker(null));\n";
    	}
    	
    	s+="kb.tell(cell"+getIndex()+");";
    	
    	
    	return s;
    }
    
    @Override
    public String toString(){
        String s = "Cell[" + 
        index.toString() + 
        "]("+ 
        row.toString() + 
        "," + 
        col.toString();
        
        
        if(checker != null){
            
            if(checker.getBlack()){
                s+=",B";
            }else{
                s+=",W";
            }
            
        }else{
            s+=",_";
        }
        
        
        s+=","+String.valueOf(cell_top_left);
        
        if(blocked_top_left){
            s+=",1";   
        }
        else{
            s+=",0";   
        }
        
        s+=","+String.valueOf(cell_top_right);
        
        if(blocked_top_right){
            s+=",1";
        }
        else{
            s+=",0";   
        }
        
        s+=","+String.valueOf(cell_bot_left);
        
        if(blocked_bot_left){
            s+=",1";   
        }
        else{
            s+=",0";   
        }
        
        
        s+=","+String.valueOf(cell_bot_right);
        

        if(blocked_bot_right){
            s+=",1";   
        }
        else{
            s+=",0";   
        }
        
        s+=")";
        
        return s;
    }

    @Override
    public Cell clone(){

        Cell clone = new Cell();

        clone.setIndex(index);
        clone.setRow(row);
        clone.setCol(col);
        clone.setChecker(checker);

        return clone;

    }

    @Override
    public boolean equals(Object o){

        boolean result = false;

        try{
            result = (getIndex() == ((Cell)o).getIndex());
        }
        catch(Exception e){

        }

        return result;
    }
    
}