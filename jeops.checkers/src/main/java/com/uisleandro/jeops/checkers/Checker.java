package com.uisleandro.jeops.checkers;

public class Checker{
    
    private Boolean black;
    
    public Checker(){
        
    }
    
    public Checker(Boolean Black){
        black = Black;
    }
    
    public void setBlack(Boolean Black){
        black = Black;
    }
    
    public Boolean getBlack(){
        return black;
    }
    
    
}