package com.uisleandro.jeops.checkers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task {
	
	public static final int TODO = 0;
	public static final int DOING = 1;
	public static final int DONE = 2;
	
	public static final int CREATE_DIAGONAL = 11;
	public static final int CREATE_CELLS = 12;
	public static final int DEFINE_NEIGHBORS = 13;  //used
	
	public static final int MOVIMENTS = 15;
	
	
	
	public static final int DEFINE_BLACK_CHECKERS = 40;
	public static final int DEFINE_WITE_CHECKERS = 41;
		
	int status;
	int task;
	int stept;
	
	public Task(int Index){
		task = Index;
		status = TODO;
		stept = 0;
	}

	
	public void waitSome(){
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int getStept() {
		return stept;
	}



	public void setStept(int stept) {
		this.stept = stept;
	}


	public void nextStept() {
		this.stept++;
	}
	

	public int getTask() {
		return task;
	}



	public void setTask(int task) {
		this.task = task;
		status = TODO;
		stept = 0;
	}


	public int getStatus() {
		return status;
	}



	public void setStatus(int status) {
		this.status = status;
	}



	@Override
    public boolean equals(Object o){

        boolean result = false;

        try{
            result = (getTask() == ((Task)o).getTask());
        }
        catch(Exception e){

        }

        return result;
    }
    
	
}
