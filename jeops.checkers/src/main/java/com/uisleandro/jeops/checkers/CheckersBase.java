package com.uisleandro.jeops.checkers;

 class Jeops_RuleBase_CheckersBase extends jeops.AbstractRuleBase {

/*

[7,0][7,1][7,2][7,3][7,4][7,5][7,6][7,7]
[6,0][6,1][6,2][6,3][6,4][6,5][6,6][6,7]
[5,0][5,1][5,2][5,3][5,4][5,5][5,6][5,7]
[4,0][4,1][4,2][4,3][4,4][4,5][4,6][4,7]
[3,0][3,1][3,2][3,3][3,4][3,5][3,6][3,7]
[2,0][2,1][2,2][2,3][2,4][2,5][2,6][2,7]
[1,0][1,1][1,2][1,3][1,4][1,5][1,6][1,7]
[0,0][0,1][0,2][0,3][0,4][0,5][0,6][0,7]

*/

/*

[_,_][_,_][_,_][_,_][_,_][_,_][_,_][7,7]
[_,_][_,_][_,_][_,_][_,_][_,_][6,6][_,_]
[_,_][_,_][_,_][_,_][_,_][5,5][_,_][_,_]
[_,_][_,_][_,_][_,_][4,4][_,_][_,_][_,_]
[_,_][_,_][_,_][3,3][_,_][_,_][_,_][_,_]
[_,_][_,_][2,2][_,_][_,_][_,_][_,_][_,_]
[_,_][1,1][_,_][_,_][_,_][_,_][_,_][_,_]
[0,0][_,_][_,_][_,_][_,_][_,_][_,_][_,_]

*/

/*

[_,_][7,1][_,_][7,3][_,_][7,5][_,_][7,7]
[6,0][_,_][6,2][_,_][6,4][_,_][6,6][_,_]
[_,_][5,1][_,_][5,3][_,_][5,5][_,_][5,7]
[4,0][_,_][4,2][_,_][4,4][_,_][4,6][_,_]
[_,_][3,1][_,_][3,3][_,_][3,5][_,_][3,7]
[2,0][_,_][2,2][_,_][2,4][_,_][2,6][_,_]
[_,_][1,1][_,_][1,3][_,_][1,5][_,_][1,7]
[0,0][_,_][0,2][_,_][0,4][_,_][0,6][_,_]

*/


/*

[--][57][--][59][--][61][--][63]
[48][--][50][--][52][--][54][--]
[--][41][--][43][--][45][--][47]
[32][--][34][--][36][--][38][--]
[--][25][--][27][--][29][--][31]
[16][--][18][--][20][--][22][--]
[--][09][--][11][--][13][--][15]
[00][--][02][--][04][--][06][--]

*/


/*

[_,_][www][_,_][www][_,_][www][_,_][www]
[www][_,_][www][_,_][www][_,_][www][_,_]
[_,_][www][_,_][www][_,_][www][_,_][www]
[4,0][_,_][4,2][_,_][4,4][_,_][4,6][_,_]
[_,_][3,1][_,_][3,3][_,_][3,5][_,_][3,7]
[BBB][_,_][BBB][_,_][BBB][_,_][BBB][_,_]
[_,_][BBB][_,_][BBB][_,_][BBB][_,_][BBB]
[BBB][_,_][BBB][_,_][BBB][_,_][BBB][_,_]

*/

    
    /**
     * Local declaration 0 of rule define_diagonal_cells.<p>
     * The original expression was:<br>
     * <code>new Cell(a.getCol() + 1, a.getRow() + 1)</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public void setdefine_diagonal_cells_localDecl_0() {
        local_decl_com_uisleandro_jeops_checkers_Cell_1 = (new Cell(com_uisleandro_jeops_checkers_Cell_1.getCol() + 1, com_uisleandro_jeops_checkers_Cell_1.getRow() + 1));
    }

    /**
     * Sets the local declaration for a given declaration index
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_define_diagonal_cells(int declIndex) {
        if (declIndex == 1) setdefine_diagonal_cells_localDecl_0();
    }

    /**
     * Returns a dependency table between the local declarations
     * and the regular ones for rule define_diagonal_cells
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_define_diagonal_cells() {
        Object[][] result = new Object[1][];
        result[0] = new Object[2];
        result[0][0] = local_decl_com_uisleandro_jeops_checkers_Cell_1;
        result[0][1] = com_uisleandro_jeops_checkers_Cell_1;
        return result;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule define_diagonal_cells
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_define_diagonal_cells(Object[][] objects) {
        local_decl_com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) objects[0][0];
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule define_diagonal_cells
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule define_diagonal_cells
     */
    private java.util.Hashtable getMapRuledefine_diagonal_cells() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("t", com_uisleandro_jeops_checkers_Task_1);
        result.put("a", com_uisleandro_jeops_checkers_Cell_1);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule define_diagonal_cells.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_define_diagonal_cells(int index) {
        switch (index) {
            case 0: return "com.uisleandro.jeops.checkers.Task";
            case 1: return "com.uisleandro.jeops.checkers.Cell";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule define_diagonal_cells.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_define_diagonal_cells(int index) {
        switch (index) {
            case 0: return com.uisleandro.jeops.checkers.Task.class;
            case 1: return com.uisleandro.jeops.checkers.Cell.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule define_diagonal_cells.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_define_diagonal_cells(int index, Object value) {
        switch (index) {
            case 0: this.com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) value; break;
            case 1: this.com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) value; break;
        }
    }

    /**
     * Returns an object declared in the rule define_diagonal_cells.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_define_diagonal_cells(int index) {
        switch (index) {
            case 0: return com_uisleandro_jeops_checkers_Task_1;
            case 1: return com_uisleandro_jeops_checkers_Cell_1;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule define_diagonal_cells
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_define_diagonal_cells() {
        return new Object[] {
                            com_uisleandro_jeops_checkers_Task_1,
                            com_uisleandro_jeops_checkers_Cell_1
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule define_diagonal_cells
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_define_diagonal_cells(Object[] objects) {
        com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) objects[0];
        com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) objects[1];
    }

    /**
     * Precondition 0 of rule define_diagonal_cells.<p>
     * The original expression was:<br>
     * <code>t.getStatus() == Task.TODO</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_diagonal_cells_prec_0() {
        return (com_uisleandro_jeops_checkers_Task_1.getStatus() == Task.TODO);
    }

    /**
     * Precondition 1 of rule define_diagonal_cells.<p>
     * The original expression was:<br>
     * <code>t.getTask() == Task.CREATE_DIAGONAL</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_diagonal_cells_prec_1() {
        return (com_uisleandro_jeops_checkers_Task_1.getTask() == Task.CREATE_DIAGONAL);
    }

    /**
     * Precondition 2 of rule define_diagonal_cells.<p>
     * The original expression was:<br>
     * <code>a.getCol() == a.getRow()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_diagonal_cells_prec_2() {
        return (com_uisleandro_jeops_checkers_Cell_1.getCol() == com_uisleandro_jeops_checkers_Cell_1.getRow());
    }

    /**
     * Precondition 3 of rule define_diagonal_cells.<p>
     * The original expression was:<br>
     * <code>a.getCol() < 7</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_diagonal_cells_prec_3() {
        return (com_uisleandro_jeops_checkers_Cell_1.getCol() < 7);
    }

    /**
     * Checks whether some preconditions of rule define_diagonal_cells is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_diagonal_cells_prec(int index) {
        switch (index) {
            case 0: return define_diagonal_cells_prec_0();
            case 1: return define_diagonal_cells_prec_1();
            case 2: return define_diagonal_cells_prec_2();
            case 3: return define_diagonal_cells_prec_3();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_define_diagonal_cells(int declIndex) {
        setLocalDeclarations_define_diagonal_cells(declIndex);
        switch (declIndex) {
            case 0:
                if (!define_diagonal_cells_prec_0()) return false;
                if (!define_diagonal_cells_prec_1()) return false;
                return true;
            case 1:
                if (!define_diagonal_cells_prec_2()) return false;
                if (!define_diagonal_cells_prec_3()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule define_diagonal_cells
     */
    public void define_diagonal_cells() {
            //b.setBlack(true);
            tell(local_decl_com_uisleandro_jeops_checkers_Cell_1);
            //System.out.println("diagonal cell");
        }



    //quando existir alguma que a diagonal seja igual a 7, para de funcionar
    
    /**
     * Sets the local declaration for a given declaration index
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_end_creating_diagonal(int declIndex) {
    }

    /**
     * Returns a dependency table between the local declarations
     * and the regular ones for rule end_creating_diagonal
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_end_creating_diagonal() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule end_creating_diagonal
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_end_creating_diagonal(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule end_creating_diagonal
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule end_creating_diagonal
     */
    private java.util.Hashtable getMapRuleend_creating_diagonal() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("t", com_uisleandro_jeops_checkers_Task_1);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule end_creating_diagonal.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_end_creating_diagonal(int index) {
        switch (index) {
            case 0: return "com.uisleandro.jeops.checkers.Task";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule end_creating_diagonal.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_end_creating_diagonal(int index) {
        switch (index) {
            case 0: return com.uisleandro.jeops.checkers.Task.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule end_creating_diagonal.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_end_creating_diagonal(int index, Object value) {
        switch (index) {
            case 0: this.com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) value; break;
        }
    }

    /**
     * Returns an object declared in the rule end_creating_diagonal.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_end_creating_diagonal(int index) {
        switch (index) {
            case 0: return com_uisleandro_jeops_checkers_Task_1;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule end_creating_diagonal
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_end_creating_diagonal() {
        return new Object[] {
                            com_uisleandro_jeops_checkers_Task_1
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule end_creating_diagonal
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_end_creating_diagonal(Object[] objects) {
        com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) objects[0];
    }

    /**
     * Precondition 0 of rule end_creating_diagonal.<p>
     * The original expression was:<br>
     * <code>t.getStatus() == Task.TODO</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean end_creating_diagonal_prec_0() {
        return (com_uisleandro_jeops_checkers_Task_1.getStatus() == Task.TODO);
    }

    /**
     * Precondition 1 of rule end_creating_diagonal.<p>
     * The original expression was:<br>
     * <code>t.getTask() == Task.CREATE_DIAGONAL</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean end_creating_diagonal_prec_1() {
        return (com_uisleandro_jeops_checkers_Task_1.getTask() == Task.CREATE_DIAGONAL);
    }

    /**
     * Checks whether some preconditions of rule end_creating_diagonal is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean end_creating_diagonal_prec(int index) {
        switch (index) {
            case 0: return end_creating_diagonal_prec_0();
            case 1: return end_creating_diagonal_prec_1();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_end_creating_diagonal(int declIndex) {
        setLocalDeclarations_end_creating_diagonal(declIndex);
        switch (declIndex) {
            case 0:
                if (!end_creating_diagonal_prec_0()) return false;
                if (!end_creating_diagonal_prec_1()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule end_creating_diagonal
     */
    public void end_creating_diagonal() {
            com_uisleandro_jeops_checkers_Task_1.setTask(Task.CREATE_CELLS);
            retract(com_uisleandro_jeops_checkers_Task_1);
            tell(com_uisleandro_jeops_checkers_Task_1);
            System.out.println("task: CREATE_CELLS");
        }




    
    /**
     * Local declaration 0 of rule define_black_cells.<p>
     * The original expression was:<br>
     * <code>new Cell(a.getCol(), b.getRow())</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public void setdefine_black_cells_localDecl_0() {
        local_decl_com_uisleandro_jeops_checkers_Cell_1 = (new Cell(com_uisleandro_jeops_checkers_Cell_1.getCol(), com_uisleandro_jeops_checkers_Cell_2.getRow()));
    }

    /**
     * Sets the local declaration for a given declaration index
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_define_black_cells(int declIndex) {
        if (declIndex == 1) setdefine_black_cells_localDecl_0();
    }

    /**
     * Returns a dependency table between the local declarations
     * and the regular ones for rule define_black_cells
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_define_black_cells() {
        Object[][] result = new Object[1][];
        result[0] = new Object[3];
        result[0][0] = local_decl_com_uisleandro_jeops_checkers_Cell_1;
        result[0][1] = com_uisleandro_jeops_checkers_Cell_1;
        result[0][2] = com_uisleandro_jeops_checkers_Cell_2;
        return result;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule define_black_cells
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_define_black_cells(Object[][] objects) {
        local_decl_com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) objects[0][0];
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule define_black_cells
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule define_black_cells
     */
    private java.util.Hashtable getMapRuledefine_black_cells() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("a", com_uisleandro_jeops_checkers_Cell_1);
        result.put("b", com_uisleandro_jeops_checkers_Cell_2);
        result.put("t", com_uisleandro_jeops_checkers_Task_1);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule define_black_cells.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_define_black_cells(int index) {
        switch (index) {
            case 0: return "com.uisleandro.jeops.checkers.Cell";
            case 1: return "com.uisleandro.jeops.checkers.Cell";
            case 2: return "com.uisleandro.jeops.checkers.Task";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule define_black_cells.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_define_black_cells(int index) {
        switch (index) {
            case 0: return com.uisleandro.jeops.checkers.Cell.class;
            case 1: return com.uisleandro.jeops.checkers.Cell.class;
            case 2: return com.uisleandro.jeops.checkers.Task.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule define_black_cells.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_define_black_cells(int index, Object value) {
        switch (index) {
            case 0: this.com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) value; break;
            case 1: this.com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) value; break;
            case 2: this.com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) value; break;
        }
    }

    /**
     * Returns an object declared in the rule define_black_cells.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_define_black_cells(int index) {
        switch (index) {
            case 0: return com_uisleandro_jeops_checkers_Cell_1;
            case 1: return com_uisleandro_jeops_checkers_Cell_2;
            case 2: return com_uisleandro_jeops_checkers_Task_1;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule define_black_cells
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_define_black_cells() {
        return new Object[] {
                            com_uisleandro_jeops_checkers_Cell_1,
                            com_uisleandro_jeops_checkers_Cell_2,
                            com_uisleandro_jeops_checkers_Task_1
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule define_black_cells
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_define_black_cells(Object[] objects) {
        com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) objects[0];
        com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) objects[1];
        com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) objects[2];
    }

    /**
     * Precondition 0 of rule define_black_cells.<p>
     * The original expression was:<br>
     * <code>t.getStatus() == Task.TODO</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_black_cells_prec_0() {
        return (com_uisleandro_jeops_checkers_Task_1.getStatus() == Task.TODO);
    }

    /**
     * Precondition 1 of rule define_black_cells.<p>
     * The original expression was:<br>
     * <code>t.getTask() == Task.CREATE_CELLS</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_black_cells_prec_1() {
        return (com_uisleandro_jeops_checkers_Task_1.getTask() == Task.CREATE_CELLS);
    }

    /**
     * Precondition 2 of rule define_black_cells.<p>
     * The original expression was:<br>
     * <code>a.getCol() == a.getRow()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_black_cells_prec_2() {
        return (com_uisleandro_jeops_checkers_Cell_1.getCol() == com_uisleandro_jeops_checkers_Cell_1.getRow());
    }

    /**
     * Precondition 3 of rule define_black_cells.<p>
     * The original expression was:<br>
     * <code>b.getCol() == b.getRow()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_black_cells_prec_3() {
        return (com_uisleandro_jeops_checkers_Cell_2.getCol() == com_uisleandro_jeops_checkers_Cell_2.getRow());
    }

    /**
     * Precondition 4 of rule define_black_cells.<p>
     * The original expression was:<br>
     * <code>a.getCol() != b.getRow()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_black_cells_prec_4() {
        return (com_uisleandro_jeops_checkers_Cell_1.getCol() != com_uisleandro_jeops_checkers_Cell_2.getRow());
    }

    /**
     * Precondition 5 of rule define_black_cells.<p>
     * The original expression was:<br>
     * <code>a.getCol() % 2 == b.getRow() % 2</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_black_cells_prec_5() {
        return (com_uisleandro_jeops_checkers_Cell_1.getCol() % 2 == com_uisleandro_jeops_checkers_Cell_2.getRow() % 2);
    }

    /**
     * Checks whether some preconditions of rule define_black_cells is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_black_cells_prec(int index) {
        switch (index) {
            case 0: return define_black_cells_prec_0();
            case 1: return define_black_cells_prec_1();
            case 2: return define_black_cells_prec_2();
            case 3: return define_black_cells_prec_3();
            case 4: return define_black_cells_prec_4();
            case 5: return define_black_cells_prec_5();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_define_black_cells(int declIndex) {
        setLocalDeclarations_define_black_cells(declIndex);
        switch (declIndex) {
            case 0:
                if (!define_black_cells_prec_2()) return false;
                return true;
            case 1:
                if (!define_black_cells_prec_3()) return false;
                if (!define_black_cells_prec_4()) return false;
                if (!define_black_cells_prec_5()) return false;
                return true;
            case 2:
                if (!define_black_cells_prec_0()) return false;
                if (!define_black_cells_prec_1()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule define_black_cells
     */
    public void define_black_cells() {
            tell(local_decl_com_uisleandro_jeops_checkers_Cell_1);
        }




    
    /**
     * Sets the local declaration for a given declaration index
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_end_define_black_cells(int declIndex) {
    }

    /**
     * Returns a dependency table between the local declarations
     * and the regular ones for rule end_define_black_cells
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_end_define_black_cells() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule end_define_black_cells
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_end_define_black_cells(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule end_define_black_cells
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule end_define_black_cells
     */
    private java.util.Hashtable getMapRuleend_define_black_cells() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("t", com_uisleandro_jeops_checkers_Task_1);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule end_define_black_cells.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_end_define_black_cells(int index) {
        switch (index) {
            case 0: return "com.uisleandro.jeops.checkers.Task";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule end_define_black_cells.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_end_define_black_cells(int index) {
        switch (index) {
            case 0: return com.uisleandro.jeops.checkers.Task.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule end_define_black_cells.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_end_define_black_cells(int index, Object value) {
        switch (index) {
            case 0: this.com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) value; break;
        }
    }

    /**
     * Returns an object declared in the rule end_define_black_cells.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_end_define_black_cells(int index) {
        switch (index) {
            case 0: return com_uisleandro_jeops_checkers_Task_1;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule end_define_black_cells
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_end_define_black_cells() {
        return new Object[] {
                            com_uisleandro_jeops_checkers_Task_1
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule end_define_black_cells
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_end_define_black_cells(Object[] objects) {
        com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) objects[0];
    }

    /**
     * Precondition 0 of rule end_define_black_cells.<p>
     * The original expression was:<br>
     * <code>t.getStatus() == Task.TODO</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean end_define_black_cells_prec_0() {
        return (com_uisleandro_jeops_checkers_Task_1.getStatus() == Task.TODO);
    }

    /**
     * Precondition 1 of rule end_define_black_cells.<p>
     * The original expression was:<br>
     * <code>t.getTask() == Task.CREATE_CELLS</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean end_define_black_cells_prec_1() {
        return (com_uisleandro_jeops_checkers_Task_1.getTask() == Task.CREATE_CELLS);
    }

    /**
     * Checks whether some preconditions of rule end_define_black_cells is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean end_define_black_cells_prec(int index) {
        switch (index) {
            case 0: return end_define_black_cells_prec_0();
            case 1: return end_define_black_cells_prec_1();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_end_define_black_cells(int declIndex) {
        setLocalDeclarations_end_define_black_cells(declIndex);
        switch (declIndex) {
            case 0:
                if (!end_define_black_cells_prec_0()) return false;
                if (!end_define_black_cells_prec_1()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule end_define_black_cells
     */
    public void end_define_black_cells() {
            com_uisleandro_jeops_checkers_Task_1.setTask(Task.DEFINE_NEIGHBORS);
            retract(com_uisleandro_jeops_checkers_Task_1);
            tell(com_uisleandro_jeops_checkers_Task_1);
            System.out.println("task: DEFINE_NEIGHBORS");
        }



    
    /**
     * Sets the local declaration for a given declaration index
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_define_top_right_neighbor(int declIndex) {
    }

    /**
     * Returns a dependency table between the local declarations
     * and the regular ones for rule define_top_right_neighbor
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_define_top_right_neighbor() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule define_top_right_neighbor
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_define_top_right_neighbor(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule define_top_right_neighbor
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule define_top_right_neighbor
     */
    private java.util.Hashtable getMapRuledefine_top_right_neighbor() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("t", com_uisleandro_jeops_checkers_Task_1);
        result.put("a", com_uisleandro_jeops_checkers_Cell_1);
        result.put("b", com_uisleandro_jeops_checkers_Cell_2);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule define_top_right_neighbor.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_define_top_right_neighbor(int index) {
        switch (index) {
            case 0: return "com.uisleandro.jeops.checkers.Task";
            case 1: return "com.uisleandro.jeops.checkers.Cell";
            case 2: return "com.uisleandro.jeops.checkers.Cell";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule define_top_right_neighbor.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_define_top_right_neighbor(int index) {
        switch (index) {
            case 0: return com.uisleandro.jeops.checkers.Task.class;
            case 1: return com.uisleandro.jeops.checkers.Cell.class;
            case 2: return com.uisleandro.jeops.checkers.Cell.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule define_top_right_neighbor.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_define_top_right_neighbor(int index, Object value) {
        switch (index) {
            case 0: this.com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) value; break;
            case 1: this.com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) value; break;
            case 2: this.com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) value; break;
        }
    }

    /**
     * Returns an object declared in the rule define_top_right_neighbor.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_define_top_right_neighbor(int index) {
        switch (index) {
            case 0: return com_uisleandro_jeops_checkers_Task_1;
            case 1: return com_uisleandro_jeops_checkers_Cell_1;
            case 2: return com_uisleandro_jeops_checkers_Cell_2;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule define_top_right_neighbor
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_define_top_right_neighbor() {
        return new Object[] {
                            com_uisleandro_jeops_checkers_Task_1,
                            com_uisleandro_jeops_checkers_Cell_1,
                            com_uisleandro_jeops_checkers_Cell_2
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule define_top_right_neighbor
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_define_top_right_neighbor(Object[] objects) {
        com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) objects[0];
        com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) objects[1];
        com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) objects[2];
    }

    /**
     * Precondition 0 of rule define_top_right_neighbor.<p>
     * The original expression was:<br>
     * <code>t.getStatus() == Task.TODO</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_top_right_neighbor_prec_0() {
        return (com_uisleandro_jeops_checkers_Task_1.getStatus() == Task.TODO);
    }

    /**
     * Precondition 1 of rule define_top_right_neighbor.<p>
     * The original expression was:<br>
     * <code>t.getTask() == Task.DEFINE_NEIGHBORS</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_top_right_neighbor_prec_1() {
        return (com_uisleandro_jeops_checkers_Task_1.getTask() == Task.DEFINE_NEIGHBORS);
    }

    /**
     * Precondition 2 of rule define_top_right_neighbor.<p>
     * The original expression was:<br>
     * <code>a.getCell_top_right() == -1</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_top_right_neighbor_prec_2() {
        return (com_uisleandro_jeops_checkers_Cell_1.getCell_top_right() == -1);
    }

    /**
     * Precondition 3 of rule define_top_right_neighbor.<p>
     * The original expression was:<br>
     * <code>a.getCol() + 1 == b.getCol()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_top_right_neighbor_prec_3() {
        return (com_uisleandro_jeops_checkers_Cell_1.getCol() + 1 == com_uisleandro_jeops_checkers_Cell_2.getCol());
    }

    /**
     * Precondition 4 of rule define_top_right_neighbor.<p>
     * The original expression was:<br>
     * <code>a.getRow() + 1 == b.getRow()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_top_right_neighbor_prec_4() {
        return (com_uisleandro_jeops_checkers_Cell_1.getRow() + 1 == com_uisleandro_jeops_checkers_Cell_2.getRow());
    }

    /**
     * Checks whether some preconditions of rule define_top_right_neighbor is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_top_right_neighbor_prec(int index) {
        switch (index) {
            case 0: return define_top_right_neighbor_prec_0();
            case 1: return define_top_right_neighbor_prec_1();
            case 2: return define_top_right_neighbor_prec_2();
            case 3: return define_top_right_neighbor_prec_3();
            case 4: return define_top_right_neighbor_prec_4();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_define_top_right_neighbor(int declIndex) {
        setLocalDeclarations_define_top_right_neighbor(declIndex);
        switch (declIndex) {
            case 0:
                if (!define_top_right_neighbor_prec_0()) return false;
                if (!define_top_right_neighbor_prec_1()) return false;
                return true;
            case 1:
                if (!define_top_right_neighbor_prec_2()) return false;
                return true;
            case 2:
                if (!define_top_right_neighbor_prec_3()) return false;
                if (!define_top_right_neighbor_prec_4()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule define_top_right_neighbor
     */
    public void define_top_right_neighbor() {
            com_uisleandro_jeops_checkers_Cell_1.setCell_top_right(com_uisleandro_jeops_checkers_Cell_2.getIndex());
            com_uisleandro_jeops_checkers_Cell_2.setCell_bot_left(com_uisleandro_jeops_checkers_Cell_1.getIndex());
            retract(com_uisleandro_jeops_checkers_Cell_1);
            tell(com_uisleandro_jeops_checkers_Cell_1);
            retract(com_uisleandro_jeops_checkers_Cell_2);
            tell(com_uisleandro_jeops_checkers_Cell_2);
            //System.out.println(a + "--" + b);
        }




    
    /**
     * Sets the local declaration for a given declaration index
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_define_top_left_neighbor(int declIndex) {
    }

    /**
     * Returns a dependency table between the local declarations
     * and the regular ones for rule define_top_left_neighbor
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_define_top_left_neighbor() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule define_top_left_neighbor
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_define_top_left_neighbor(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule define_top_left_neighbor
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule define_top_left_neighbor
     */
    private java.util.Hashtable getMapRuledefine_top_left_neighbor() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("t", com_uisleandro_jeops_checkers_Task_1);
        result.put("a", com_uisleandro_jeops_checkers_Cell_1);
        result.put("b", com_uisleandro_jeops_checkers_Cell_2);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule define_top_left_neighbor.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_define_top_left_neighbor(int index) {
        switch (index) {
            case 0: return "com.uisleandro.jeops.checkers.Task";
            case 1: return "com.uisleandro.jeops.checkers.Cell";
            case 2: return "com.uisleandro.jeops.checkers.Cell";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule define_top_left_neighbor.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_define_top_left_neighbor(int index) {
        switch (index) {
            case 0: return com.uisleandro.jeops.checkers.Task.class;
            case 1: return com.uisleandro.jeops.checkers.Cell.class;
            case 2: return com.uisleandro.jeops.checkers.Cell.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule define_top_left_neighbor.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_define_top_left_neighbor(int index, Object value) {
        switch (index) {
            case 0: this.com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) value; break;
            case 1: this.com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) value; break;
            case 2: this.com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) value; break;
        }
    }

    /**
     * Returns an object declared in the rule define_top_left_neighbor.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_define_top_left_neighbor(int index) {
        switch (index) {
            case 0: return com_uisleandro_jeops_checkers_Task_1;
            case 1: return com_uisleandro_jeops_checkers_Cell_1;
            case 2: return com_uisleandro_jeops_checkers_Cell_2;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule define_top_left_neighbor
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_define_top_left_neighbor() {
        return new Object[] {
                            com_uisleandro_jeops_checkers_Task_1,
                            com_uisleandro_jeops_checkers_Cell_1,
                            com_uisleandro_jeops_checkers_Cell_2
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule define_top_left_neighbor
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_define_top_left_neighbor(Object[] objects) {
        com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) objects[0];
        com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) objects[1];
        com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) objects[2];
    }

    /**
     * Precondition 0 of rule define_top_left_neighbor.<p>
     * The original expression was:<br>
     * <code>t.getStatus() == Task.TODO</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_top_left_neighbor_prec_0() {
        return (com_uisleandro_jeops_checkers_Task_1.getStatus() == Task.TODO);
    }

    /**
     * Precondition 1 of rule define_top_left_neighbor.<p>
     * The original expression was:<br>
     * <code>t.getTask() == Task.DEFINE_NEIGHBORS</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_top_left_neighbor_prec_1() {
        return (com_uisleandro_jeops_checkers_Task_1.getTask() == Task.DEFINE_NEIGHBORS);
    }

    /**
     * Precondition 2 of rule define_top_left_neighbor.<p>
     * The original expression was:<br>
     * <code>a.getCell_top_left() == -1</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_top_left_neighbor_prec_2() {
        return (com_uisleandro_jeops_checkers_Cell_1.getCell_top_left() == -1);
    }

    /**
     * Precondition 3 of rule define_top_left_neighbor.<p>
     * The original expression was:<br>
     * <code>a.getCol() - 1 == b.getCol()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_top_left_neighbor_prec_3() {
        return (com_uisleandro_jeops_checkers_Cell_1.getCol() - 1 == com_uisleandro_jeops_checkers_Cell_2.getCol());
    }

    /**
     * Precondition 4 of rule define_top_left_neighbor.<p>
     * The original expression was:<br>
     * <code>a.getRow() + 1 == b.getRow()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_top_left_neighbor_prec_4() {
        return (com_uisleandro_jeops_checkers_Cell_1.getRow() + 1 == com_uisleandro_jeops_checkers_Cell_2.getRow());
    }

    /**
     * Checks whether some preconditions of rule define_top_left_neighbor is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean define_top_left_neighbor_prec(int index) {
        switch (index) {
            case 0: return define_top_left_neighbor_prec_0();
            case 1: return define_top_left_neighbor_prec_1();
            case 2: return define_top_left_neighbor_prec_2();
            case 3: return define_top_left_neighbor_prec_3();
            case 4: return define_top_left_neighbor_prec_4();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_define_top_left_neighbor(int declIndex) {
        setLocalDeclarations_define_top_left_neighbor(declIndex);
        switch (declIndex) {
            case 0:
                if (!define_top_left_neighbor_prec_0()) return false;
                if (!define_top_left_neighbor_prec_1()) return false;
                return true;
            case 1:
                if (!define_top_left_neighbor_prec_2()) return false;
                return true;
            case 2:
                if (!define_top_left_neighbor_prec_3()) return false;
                if (!define_top_left_neighbor_prec_4()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule define_top_left_neighbor
     */
    public void define_top_left_neighbor() {
            com_uisleandro_jeops_checkers_Cell_1.setCell_top_left(com_uisleandro_jeops_checkers_Cell_2.getIndex());
            com_uisleandro_jeops_checkers_Cell_2.setCell_bot_right(com_uisleandro_jeops_checkers_Cell_1.getIndex());
            retract(com_uisleandro_jeops_checkers_Cell_1);
            tell(com_uisleandro_jeops_checkers_Cell_1);
            retract(com_uisleandro_jeops_checkers_Cell_2);
            tell(com_uisleandro_jeops_checkers_Cell_2);
            //System.out.println(a + "--" + b);
        }





    
    /**
     * Sets the local declaration for a given declaration index
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_add_black_checkers(int declIndex) {
    }

    /**
     * Returns a dependency table between the local declarations
     * and the regular ones for rule add_black_checkers
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_add_black_checkers() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule add_black_checkers
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_add_black_checkers(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule add_black_checkers
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule add_black_checkers
     */
    private java.util.Hashtable getMapRuleadd_black_checkers() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("t", com_uisleandro_jeops_checkers_Task_1);
        result.put("a", com_uisleandro_jeops_checkers_Cell_1);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule add_black_checkers.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_add_black_checkers(int index) {
        switch (index) {
            case 0: return "com.uisleandro.jeops.checkers.Task";
            case 1: return "com.uisleandro.jeops.checkers.Cell";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule add_black_checkers.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_add_black_checkers(int index) {
        switch (index) {
            case 0: return com.uisleandro.jeops.checkers.Task.class;
            case 1: return com.uisleandro.jeops.checkers.Cell.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule add_black_checkers.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_add_black_checkers(int index, Object value) {
        switch (index) {
            case 0: this.com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) value; break;
            case 1: this.com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) value; break;
        }
    }

    /**
     * Returns an object declared in the rule add_black_checkers.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_add_black_checkers(int index) {
        switch (index) {
            case 0: return com_uisleandro_jeops_checkers_Task_1;
            case 1: return com_uisleandro_jeops_checkers_Cell_1;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule add_black_checkers
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_add_black_checkers() {
        return new Object[] {
                            com_uisleandro_jeops_checkers_Task_1,
                            com_uisleandro_jeops_checkers_Cell_1
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule add_black_checkers
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_add_black_checkers(Object[] objects) {
        com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) objects[0];
        com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) objects[1];
    }

    /**
     * Precondition 0 of rule add_black_checkers.<p>
     * The original expression was:<br>
     * <code>t.getStatus() == Task.TODO</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean add_black_checkers_prec_0() {
        return (com_uisleandro_jeops_checkers_Task_1.getStatus() == Task.TODO);
    }

    /**
     * Precondition 1 of rule add_black_checkers.<p>
     * The original expression was:<br>
     * <code>t.getTask() == Task.DEFINE_NEIGHBORS</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean add_black_checkers_prec_1() {
        return (com_uisleandro_jeops_checkers_Task_1.getTask() == Task.DEFINE_NEIGHBORS);
    }

    /**
     * Precondition 2 of rule add_black_checkers.<p>
     * The original expression was:<br>
     * <code>a.getRow() < 3</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean add_black_checkers_prec_2() {
        return (com_uisleandro_jeops_checkers_Cell_1.getRow() < 3);
    }

    /**
     * Precondition 3 of rule add_black_checkers.<p>
     * The original expression was:<br>
     * <code>a.getChecker() == null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean add_black_checkers_prec_3() {
        return (com_uisleandro_jeops_checkers_Cell_1.getChecker() == null);
    }

    /**
     * Checks whether some preconditions of rule add_black_checkers is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean add_black_checkers_prec(int index) {
        switch (index) {
            case 0: return add_black_checkers_prec_0();
            case 1: return add_black_checkers_prec_1();
            case 2: return add_black_checkers_prec_2();
            case 3: return add_black_checkers_prec_3();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_add_black_checkers(int declIndex) {
        setLocalDeclarations_add_black_checkers(declIndex);
        switch (declIndex) {
            case 0:
                if (!add_black_checkers_prec_0()) return false;
                if (!add_black_checkers_prec_1()) return false;
                return true;
            case 1:
                if (!add_black_checkers_prec_2()) return false;
                if (!add_black_checkers_prec_3()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule add_black_checkers
     */
    public void add_black_checkers() {
            retract(com_uisleandro_jeops_checkers_Cell_1);
            com_uisleandro_jeops_checkers_Cell_1.setChecker(new Checker(true));
            tell(com_uisleandro_jeops_checkers_Cell_1);

        }



    
    /**
     * Sets the local declaration for a given declaration index
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_add_white_checkers(int declIndex) {
    }

    /**
     * Returns a dependency table between the local declarations
     * and the regular ones for rule add_white_checkers
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_add_white_checkers() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule add_white_checkers
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_add_white_checkers(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule add_white_checkers
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule add_white_checkers
     */
    private java.util.Hashtable getMapRuleadd_white_checkers() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("t", com_uisleandro_jeops_checkers_Task_1);
        result.put("a", com_uisleandro_jeops_checkers_Cell_1);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule add_white_checkers.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_add_white_checkers(int index) {
        switch (index) {
            case 0: return "com.uisleandro.jeops.checkers.Task";
            case 1: return "com.uisleandro.jeops.checkers.Cell";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule add_white_checkers.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_add_white_checkers(int index) {
        switch (index) {
            case 0: return com.uisleandro.jeops.checkers.Task.class;
            case 1: return com.uisleandro.jeops.checkers.Cell.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule add_white_checkers.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_add_white_checkers(int index, Object value) {
        switch (index) {
            case 0: this.com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) value; break;
            case 1: this.com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) value; break;
        }
    }

    /**
     * Returns an object declared in the rule add_white_checkers.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_add_white_checkers(int index) {
        switch (index) {
            case 0: return com_uisleandro_jeops_checkers_Task_1;
            case 1: return com_uisleandro_jeops_checkers_Cell_1;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule add_white_checkers
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_add_white_checkers() {
        return new Object[] {
                            com_uisleandro_jeops_checkers_Task_1,
                            com_uisleandro_jeops_checkers_Cell_1
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule add_white_checkers
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_add_white_checkers(Object[] objects) {
        com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) objects[0];
        com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) objects[1];
    }

    /**
     * Precondition 0 of rule add_white_checkers.<p>
     * The original expression was:<br>
     * <code>t.getStatus() == Task.TODO</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean add_white_checkers_prec_0() {
        return (com_uisleandro_jeops_checkers_Task_1.getStatus() == Task.TODO);
    }

    /**
     * Precondition 1 of rule add_white_checkers.<p>
     * The original expression was:<br>
     * <code>t.getTask() == Task.DEFINE_NEIGHBORS</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean add_white_checkers_prec_1() {
        return (com_uisleandro_jeops_checkers_Task_1.getTask() == Task.DEFINE_NEIGHBORS);
    }

    /**
     * Precondition 2 of rule add_white_checkers.<p>
     * The original expression was:<br>
     * <code>a.getRow() > 4</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean add_white_checkers_prec_2() {
        return (com_uisleandro_jeops_checkers_Cell_1.getRow() > 4);
    }

    /**
     * Precondition 3 of rule add_white_checkers.<p>
     * The original expression was:<br>
     * <code>a.getChecker() == null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean add_white_checkers_prec_3() {
        return (com_uisleandro_jeops_checkers_Cell_1.getChecker() == null);
    }

    /**
     * Checks whether some preconditions of rule add_white_checkers is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean add_white_checkers_prec(int index) {
        switch (index) {
            case 0: return add_white_checkers_prec_0();
            case 1: return add_white_checkers_prec_1();
            case 2: return add_white_checkers_prec_2();
            case 3: return add_white_checkers_prec_3();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_add_white_checkers(int declIndex) {
        setLocalDeclarations_add_white_checkers(declIndex);
        switch (declIndex) {
            case 0:
                if (!add_white_checkers_prec_0()) return false;
                if (!add_white_checkers_prec_1()) return false;
                return true;
            case 1:
                if (!add_white_checkers_prec_2()) return false;
                if (!add_white_checkers_prec_3()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule add_white_checkers
     */
    public void add_white_checkers() {
            retract(com_uisleandro_jeops_checkers_Cell_1);
            com_uisleandro_jeops_checkers_Cell_1.setChecker(new Checker(false));
            tell(com_uisleandro_jeops_checkers_Cell_1);
        }




    
    /**
     * Sets the local declaration for a given declaration index
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_wall_blocking_top_right(int declIndex) {
    }

    /**
     * Returns a dependency table between the local declarations
     * and the regular ones for rule wall_blocking_top_right
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_wall_blocking_top_right() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule wall_blocking_top_right
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_wall_blocking_top_right(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule wall_blocking_top_right
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule wall_blocking_top_right
     */
    private java.util.Hashtable getMapRulewall_blocking_top_right() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("t", com_uisleandro_jeops_checkers_Task_1);
        result.put("a", com_uisleandro_jeops_checkers_Cell_1);
        result.put("b", com_uisleandro_jeops_checkers_Cell_2);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule wall_blocking_top_right.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_wall_blocking_top_right(int index) {
        switch (index) {
            case 0: return "com.uisleandro.jeops.checkers.Task";
            case 1: return "com.uisleandro.jeops.checkers.Cell";
            case 2: return "com.uisleandro.jeops.checkers.Cell";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule wall_blocking_top_right.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_wall_blocking_top_right(int index) {
        switch (index) {
            case 0: return com.uisleandro.jeops.checkers.Task.class;
            case 1: return com.uisleandro.jeops.checkers.Cell.class;
            case 2: return com.uisleandro.jeops.checkers.Cell.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule wall_blocking_top_right.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_wall_blocking_top_right(int index, Object value) {
        switch (index) {
            case 0: this.com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) value; break;
            case 1: this.com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) value; break;
            case 2: this.com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) value; break;
        }
    }

    /**
     * Returns an object declared in the rule wall_blocking_top_right.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_wall_blocking_top_right(int index) {
        switch (index) {
            case 0: return com_uisleandro_jeops_checkers_Task_1;
            case 1: return com_uisleandro_jeops_checkers_Cell_1;
            case 2: return com_uisleandro_jeops_checkers_Cell_2;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule wall_blocking_top_right
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_wall_blocking_top_right() {
        return new Object[] {
                            com_uisleandro_jeops_checkers_Task_1,
                            com_uisleandro_jeops_checkers_Cell_1,
                            com_uisleandro_jeops_checkers_Cell_2
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule wall_blocking_top_right
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_wall_blocking_top_right(Object[] objects) {
        com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) objects[0];
        com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) objects[1];
        com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) objects[2];
    }

    /**
     * Precondition 0 of rule wall_blocking_top_right.<p>
     * The original expression was:<br>
     * <code>t.getStatus() == Task.TODO</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean wall_blocking_top_right_prec_0() {
        return (com_uisleandro_jeops_checkers_Task_1.getStatus() == Task.TODO);
    }

    /**
     * Precondition 1 of rule wall_blocking_top_right.<p>
     * The original expression was:<br>
     * <code>t.getTask() == Task.DEFINE_NEIGHBORS</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean wall_blocking_top_right_prec_1() {
        return (com_uisleandro_jeops_checkers_Task_1.getTask() == Task.DEFINE_NEIGHBORS);
    }

    /**
     * Precondition 2 of rule wall_blocking_top_right.<p>
     * The original expression was:<br>
     * <code>a.getChecker() != null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean wall_blocking_top_right_prec_2() {
        return (com_uisleandro_jeops_checkers_Cell_1.getChecker() != null);
    }

    /**
     * Precondition 3 of rule wall_blocking_top_right.<p>
     * The original expression was:<br>
     * <code>b.getChecker() != null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean wall_blocking_top_right_prec_3() {
        return (com_uisleandro_jeops_checkers_Cell_2.getChecker() != null);
    }

    /**
     * Precondition 4 of rule wall_blocking_top_right.<p>
     * The original expression was:<br>
     * <code>a.getCol() + 1 == b.getCol()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean wall_blocking_top_right_prec_4() {
        return (com_uisleandro_jeops_checkers_Cell_1.getCol() + 1 == com_uisleandro_jeops_checkers_Cell_2.getCol());
    }

    /**
     * Precondition 5 of rule wall_blocking_top_right.<p>
     * The original expression was:<br>
     * <code>a.getRow() + 1 == b.getRow()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean wall_blocking_top_right_prec_5() {
        return (com_uisleandro_jeops_checkers_Cell_1.getRow() + 1 == com_uisleandro_jeops_checkers_Cell_2.getRow());
    }

    /**
     * Precondition 6 of rule wall_blocking_top_right.<p>
     * The original expression was:<br>
     * <code>a.getBlocked_top_right() == false</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean wall_blocking_top_right_prec_6() {
        return (com_uisleandro_jeops_checkers_Cell_1.getBlocked_top_right() == false);
    }

    /**
     * Checks whether some preconditions of rule wall_blocking_top_right is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean wall_blocking_top_right_prec(int index) {
        switch (index) {
            case 0: return wall_blocking_top_right_prec_0();
            case 1: return wall_blocking_top_right_prec_1();
            case 2: return wall_blocking_top_right_prec_2();
            case 3: return wall_blocking_top_right_prec_3();
            case 4: return wall_blocking_top_right_prec_4();
            case 5: return wall_blocking_top_right_prec_5();
            case 6: return wall_blocking_top_right_prec_6();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_wall_blocking_top_right(int declIndex) {
        setLocalDeclarations_wall_blocking_top_right(declIndex);
        switch (declIndex) {
            case 0:
                if (!wall_blocking_top_right_prec_0()) return false;
                if (!wall_blocking_top_right_prec_1()) return false;
                return true;
            case 1:
                if (!wall_blocking_top_right_prec_2()) return false;
                if (!wall_blocking_top_right_prec_6()) return false;
                return true;
            case 2:
                if (!wall_blocking_top_right_prec_3()) return false;
                if (!wall_blocking_top_right_prec_4()) return false;
                if (!wall_blocking_top_right_prec_5()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule wall_blocking_top_right
     */
    public void wall_blocking_top_right() {
        	 com_uisleandro_jeops_checkers_Cell_1.setBlocked_top_right(true);
        	 com_uisleandro_jeops_checkers_Cell_2.setBlocked_bot_left(true);
        	 retract(com_uisleandro_jeops_checkers_Cell_1);
        	 retract(com_uisleandro_jeops_checkers_Cell_2);
        	 tell(com_uisleandro_jeops_checkers_Cell_1);
        	 tell(com_uisleandro_jeops_checkers_Cell_2);
        }


    
    
    
    /**
     * Sets the local declaration for a given declaration index
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_wall_blocking_top_left(int declIndex) {
    }

    /**
     * Returns a dependency table between the local declarations
     * and the regular ones for rule wall_blocking_top_left
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_wall_blocking_top_left() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule wall_blocking_top_left
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_wall_blocking_top_left(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule wall_blocking_top_left
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule wall_blocking_top_left
     */
    private java.util.Hashtable getMapRulewall_blocking_top_left() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("t", com_uisleandro_jeops_checkers_Task_1);
        result.put("a", com_uisleandro_jeops_checkers_Cell_1);
        result.put("b", com_uisleandro_jeops_checkers_Cell_2);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule wall_blocking_top_left.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_wall_blocking_top_left(int index) {
        switch (index) {
            case 0: return "com.uisleandro.jeops.checkers.Task";
            case 1: return "com.uisleandro.jeops.checkers.Cell";
            case 2: return "com.uisleandro.jeops.checkers.Cell";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule wall_blocking_top_left.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_wall_blocking_top_left(int index) {
        switch (index) {
            case 0: return com.uisleandro.jeops.checkers.Task.class;
            case 1: return com.uisleandro.jeops.checkers.Cell.class;
            case 2: return com.uisleandro.jeops.checkers.Cell.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule wall_blocking_top_left.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_wall_blocking_top_left(int index, Object value) {
        switch (index) {
            case 0: this.com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) value; break;
            case 1: this.com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) value; break;
            case 2: this.com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) value; break;
        }
    }

    /**
     * Returns an object declared in the rule wall_blocking_top_left.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_wall_blocking_top_left(int index) {
        switch (index) {
            case 0: return com_uisleandro_jeops_checkers_Task_1;
            case 1: return com_uisleandro_jeops_checkers_Cell_1;
            case 2: return com_uisleandro_jeops_checkers_Cell_2;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule wall_blocking_top_left
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_wall_blocking_top_left() {
        return new Object[] {
                            com_uisleandro_jeops_checkers_Task_1,
                            com_uisleandro_jeops_checkers_Cell_1,
                            com_uisleandro_jeops_checkers_Cell_2
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule wall_blocking_top_left
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_wall_blocking_top_left(Object[] objects) {
        com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) objects[0];
        com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) objects[1];
        com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) objects[2];
    }

    /**
     * Precondition 0 of rule wall_blocking_top_left.<p>
     * The original expression was:<br>
     * <code>t.getStatus() == Task.TODO</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean wall_blocking_top_left_prec_0() {
        return (com_uisleandro_jeops_checkers_Task_1.getStatus() == Task.TODO);
    }

    /**
     * Precondition 1 of rule wall_blocking_top_left.<p>
     * The original expression was:<br>
     * <code>t.getTask() == Task.DEFINE_NEIGHBORS</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean wall_blocking_top_left_prec_1() {
        return (com_uisleandro_jeops_checkers_Task_1.getTask() == Task.DEFINE_NEIGHBORS);
    }

    /**
     * Precondition 2 of rule wall_blocking_top_left.<p>
     * The original expression was:<br>
     * <code>a.getChecker() != null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean wall_blocking_top_left_prec_2() {
        return (com_uisleandro_jeops_checkers_Cell_1.getChecker() != null);
    }

    /**
     * Precondition 3 of rule wall_blocking_top_left.<p>
     * The original expression was:<br>
     * <code>b.getChecker() != null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean wall_blocking_top_left_prec_3() {
        return (com_uisleandro_jeops_checkers_Cell_2.getChecker() != null);
    }

    /**
     * Precondition 4 of rule wall_blocking_top_left.<p>
     * The original expression was:<br>
     * <code>a.getCol() - 1 == b.getCol()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean wall_blocking_top_left_prec_4() {
        return (com_uisleandro_jeops_checkers_Cell_1.getCol() - 1 == com_uisleandro_jeops_checkers_Cell_2.getCol());
    }

    /**
     * Precondition 5 of rule wall_blocking_top_left.<p>
     * The original expression was:<br>
     * <code>a.getRow() + 1 == b.getRow()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean wall_blocking_top_left_prec_5() {
        return (com_uisleandro_jeops_checkers_Cell_1.getRow() + 1 == com_uisleandro_jeops_checkers_Cell_2.getRow());
    }

    /**
     * Precondition 6 of rule wall_blocking_top_left.<p>
     * The original expression was:<br>
     * <code>a.getBlocked_top_left() == false</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean wall_blocking_top_left_prec_6() {
        return (com_uisleandro_jeops_checkers_Cell_1.getBlocked_top_left() == false);
    }

    /**
     * Checks whether some preconditions of rule wall_blocking_top_left is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean wall_blocking_top_left_prec(int index) {
        switch (index) {
            case 0: return wall_blocking_top_left_prec_0();
            case 1: return wall_blocking_top_left_prec_1();
            case 2: return wall_blocking_top_left_prec_2();
            case 3: return wall_blocking_top_left_prec_3();
            case 4: return wall_blocking_top_left_prec_4();
            case 5: return wall_blocking_top_left_prec_5();
            case 6: return wall_blocking_top_left_prec_6();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_wall_blocking_top_left(int declIndex) {
        setLocalDeclarations_wall_blocking_top_left(declIndex);
        switch (declIndex) {
            case 0:
                if (!wall_blocking_top_left_prec_0()) return false;
                if (!wall_blocking_top_left_prec_1()) return false;
                return true;
            case 1:
                if (!wall_blocking_top_left_prec_2()) return false;
                if (!wall_blocking_top_left_prec_6()) return false;
                return true;
            case 2:
                if (!wall_blocking_top_left_prec_3()) return false;
                if (!wall_blocking_top_left_prec_4()) return false;
                if (!wall_blocking_top_left_prec_5()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule wall_blocking_top_left
     */
    public void wall_blocking_top_left() {
        	 com_uisleandro_jeops_checkers_Cell_1.setBlocked_top_left(true);
        	 com_uisleandro_jeops_checkers_Cell_2.setBlocked_bot_right(true);
        	 retract(com_uisleandro_jeops_checkers_Cell_1);
        	 retract(com_uisleandro_jeops_checkers_Cell_2);
        	 tell(com_uisleandro_jeops_checkers_Cell_1);
        	 tell(com_uisleandro_jeops_checkers_Cell_2);
        }


    

    /**
     * Sets the local declaration for a given declaration index
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_end_define_map(int declIndex) {
    }

    /**
     * Returns a dependency table between the local declarations
     * and the regular ones for rule end_define_map
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_end_define_map() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule end_define_map
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_end_define_map(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule end_define_map
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule end_define_map
     */
    private java.util.Hashtable getMapRuleend_define_map() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("t", com_uisleandro_jeops_checkers_Task_1);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule end_define_map.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_end_define_map(int index) {
        switch (index) {
            case 0: return "com.uisleandro.jeops.checkers.Task";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule end_define_map.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_end_define_map(int index) {
        switch (index) {
            case 0: return com.uisleandro.jeops.checkers.Task.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule end_define_map.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_end_define_map(int index, Object value) {
        switch (index) {
            case 0: this.com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) value; break;
        }
    }

    /**
     * Returns an object declared in the rule end_define_map.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_end_define_map(int index) {
        switch (index) {
            case 0: return com_uisleandro_jeops_checkers_Task_1;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule end_define_map
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_end_define_map() {
        return new Object[] {
                            com_uisleandro_jeops_checkers_Task_1
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule end_define_map
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_end_define_map(Object[] objects) {
        com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) objects[0];
    }

    /**
     * Precondition 0 of rule end_define_map.<p>
     * The original expression was:<br>
     * <code>t.getStatus() == Task.TODO</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean end_define_map_prec_0() {
        return (com_uisleandro_jeops_checkers_Task_1.getStatus() == Task.TODO);
    }

    /**
     * Precondition 1 of rule end_define_map.<p>
     * The original expression was:<br>
     * <code>t.getTask() == Task.DEFINE_NEIGHBORS</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean end_define_map_prec_1() {
        return (com_uisleandro_jeops_checkers_Task_1.getTask() == Task.DEFINE_NEIGHBORS);
    }

    /**
     * Checks whether some preconditions of rule end_define_map is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean end_define_map_prec(int index) {
        switch (index) {
            case 0: return end_define_map_prec_0();
            case 1: return end_define_map_prec_1();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_end_define_map(int declIndex) {
        setLocalDeclarations_end_define_map(declIndex);
        switch (declIndex) {
            case 0:
                if (!end_define_map_prec_0()) return false;
                if (!end_define_map_prec_1()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule end_define_map
     */
    public void end_define_map() {
    	com_uisleandro_jeops_checkers_Task_1.setTask(Task.MOVIMENTS);
    	retract(com_uisleandro_jeops_checkers_Task_1);
    	tell(com_uisleandro_jeops_checkers_Task_1);
    }


    
    

//mover um peça para uma posicao em que ela pode ser comida (teste)
/**
rule test_move_cell{
    declarations
    	Task t;
    	Cell a;
    	Cell b;
    localdecl
    conditions
    	t.getStatus() == Task.TODO;
    	t.getTask() == Task.MOVIMENTS;
    	a.getIndex() == 43;
    	b.getIndex() == 29;
    	a.getChecker() != null;
    actions
    	t.setStatus(Task.DONE); //nao precisei fazer o retract
    	b.setChecker(a.getChecker());
    	retract(b);
    	tell(b);
    	a.setChecker(null);
    	retract(a);
    	tell(a);
    	retract(t); // nao estou fazendo mais o tell(t); 
    	System.out.println("CHECKER MOVED FROM 43 TO 29 v2");
}
/**/
  
/* 22,29,36 funciona */


    /**
     * Sets the local declaration for a given declaration index
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_eat_top_left(int declIndex) {
    }

    /**
     * Returns a dependency table between the local declarations
     * and the regular ones for rule eat_top_left
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_eat_top_left() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule eat_top_left
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_eat_top_left(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule eat_top_left
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule eat_top_left
     */
    private java.util.Hashtable getMapRuleeat_top_left() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("t", com_uisleandro_jeops_checkers_Task_1);
        result.put("a", com_uisleandro_jeops_checkers_Cell_1);
        result.put("b", com_uisleandro_jeops_checkers_Cell_2);
        result.put("c", com_uisleandro_jeops_checkers_Cell_3);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule eat_top_left.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_eat_top_left(int index) {
        switch (index) {
            case 0: return "com.uisleandro.jeops.checkers.Task";
            case 1: return "com.uisleandro.jeops.checkers.Cell";
            case 2: return "com.uisleandro.jeops.checkers.Cell";
            case 3: return "com.uisleandro.jeops.checkers.Cell";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule eat_top_left.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_eat_top_left(int index) {
        switch (index) {
            case 0: return com.uisleandro.jeops.checkers.Task.class;
            case 1: return com.uisleandro.jeops.checkers.Cell.class;
            case 2: return com.uisleandro.jeops.checkers.Cell.class;
            case 3: return com.uisleandro.jeops.checkers.Cell.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule eat_top_left.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_eat_top_left(int index, Object value) {
        switch (index) {
            case 0: this.com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) value; break;
            case 1: this.com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) value; break;
            case 2: this.com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) value; break;
            case 3: this.com_uisleandro_jeops_checkers_Cell_3 = (com.uisleandro.jeops.checkers.Cell) value; break;
        }
    }

    /**
     * Returns an object declared in the rule eat_top_left.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_eat_top_left(int index) {
        switch (index) {
            case 0: return com_uisleandro_jeops_checkers_Task_1;
            case 1: return com_uisleandro_jeops_checkers_Cell_1;
            case 2: return com_uisleandro_jeops_checkers_Cell_2;
            case 3: return com_uisleandro_jeops_checkers_Cell_3;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule eat_top_left
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_eat_top_left() {
        return new Object[] {
                            com_uisleandro_jeops_checkers_Task_1,
                            com_uisleandro_jeops_checkers_Cell_1,
                            com_uisleandro_jeops_checkers_Cell_2,
                            com_uisleandro_jeops_checkers_Cell_3
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule eat_top_left
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_eat_top_left(Object[] objects) {
        com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) objects[0];
        com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) objects[1];
        com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) objects[2];
        com_uisleandro_jeops_checkers_Cell_3 = (com.uisleandro.jeops.checkers.Cell) objects[3];
    }

    /**
     * Precondition 0 of rule eat_top_left.<p>
     * The original expression was:<br>
     * <code>t.getStatus() == Task.TODO</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_top_left_prec_0() {
        return (com_uisleandro_jeops_checkers_Task_1.getStatus() == Task.TODO);
    }

    /**
     * Precondition 1 of rule eat_top_left.<p>
     * The original expression was:<br>
     * <code>t.getTask() == Task.MOVIMENTS</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_top_left_prec_1() {
        return (com_uisleandro_jeops_checkers_Task_1.getTask() == Task.MOVIMENTS);
    }

    /**
     * Precondition 2 of rule eat_top_left.<p>
     * The original expression was:<br>
     * <code>a.getCell_top_left() == b.getIndex()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_top_left_prec_2() {
        return (com_uisleandro_jeops_checkers_Cell_1.getCell_top_left() == com_uisleandro_jeops_checkers_Cell_2.getIndex());
    }

    /**
     * Precondition 3 of rule eat_top_left.<p>
     * The original expression was:<br>
     * <code>b.getCell_top_left() == c.getIndex()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_top_left_prec_3() {
        return (com_uisleandro_jeops_checkers_Cell_2.getCell_top_left() == com_uisleandro_jeops_checkers_Cell_3.getIndex());
    }

    /**
     * Precondition 4 of rule eat_top_left.<p>
     * The original expression was:<br>
     * <code>a.getChecker() != null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_top_left_prec_4() {
        return (com_uisleandro_jeops_checkers_Cell_1.getChecker() != null);
    }

    /**
     * Precondition 5 of rule eat_top_left.<p>
     * The original expression was:<br>
     * <code>b.getChecker() != null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_top_left_prec_5() {
        return (com_uisleandro_jeops_checkers_Cell_2.getChecker() != null);
    }

    /**
     * Precondition 6 of rule eat_top_left.<p>
     * The original expression was:<br>
     * <code>c.getChecker() == null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_top_left_prec_6() {
        return (com_uisleandro_jeops_checkers_Cell_3.getChecker() == null);
    }

    /**
     * Precondition 7 of rule eat_top_left.<p>
     * The original expression was:<br>
     * <code>a.getChecker().getBlack() == true</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_top_left_prec_7() {
        return (com_uisleandro_jeops_checkers_Cell_1.getChecker().getBlack() == true);
    }

    /**
     * Precondition 8 of rule eat_top_left.<p>
     * The original expression was:<br>
     * <code>b.getChecker().getBlack() == false</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_top_left_prec_8() {
        return (com_uisleandro_jeops_checkers_Cell_2.getChecker().getBlack() == false);
    }

    /**
     * Checks whether some preconditions of rule eat_top_left is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_top_left_prec(int index) {
        switch (index) {
            case 0: return eat_top_left_prec_0();
            case 1: return eat_top_left_prec_1();
            case 2: return eat_top_left_prec_2();
            case 3: return eat_top_left_prec_3();
            case 4: return eat_top_left_prec_4();
            case 5: return eat_top_left_prec_5();
            case 6: return eat_top_left_prec_6();
            case 7: return eat_top_left_prec_7();
            case 8: return eat_top_left_prec_8();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_eat_top_left(int declIndex) {
        setLocalDeclarations_eat_top_left(declIndex);
        switch (declIndex) {
            case 0:
                if (!eat_top_left_prec_0()) return false;
                if (!eat_top_left_prec_1()) return false;
                return true;
            case 1:
                if (!eat_top_left_prec_4()) return false;
                if (!eat_top_left_prec_7()) return false;
                return true;
            case 2:
                if (!eat_top_left_prec_2()) return false;
                if (!eat_top_left_prec_5()) return false;
                if (!eat_top_left_prec_8()) return false;
                return true;
            case 3:
                if (!eat_top_left_prec_3()) return false;
                if (!eat_top_left_prec_6()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule eat_top_left
     */
    public void eat_top_left() {
	  	System.out.println(com_uisleandro_jeops_checkers_Cell_1.getIndex() +","+ com_uisleandro_jeops_checkers_Cell_2.getIndex() +","+ com_uisleandro_jeops_checkers_Cell_3.getIndex());
    	com_uisleandro_jeops_checkers_Cell_3.setChecker(com_uisleandro_jeops_checkers_Cell_1.getChecker());
    	com_uisleandro_jeops_checkers_Cell_1.setChecker(null);
    	com_uisleandro_jeops_checkers_Cell_2.setChecker(null);
    	retract(com_uisleandro_jeops_checkers_Cell_1);
    	retract(com_uisleandro_jeops_checkers_Cell_2);
    	retract(com_uisleandro_jeops_checkers_Cell_3);
    	tell(com_uisleandro_jeops_checkers_Cell_1);
    	tell(com_uisleandro_jeops_checkers_Cell_2);
    	tell(com_uisleandro_jeops_checkers_Cell_3);
    }



/**/

/* 20,29,38 funciona */

    /**
     * Sets the local declaration for a given declaration index
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_eat_top_right(int declIndex) {
    }

    /**
     * Returns a dependency table between the local declarations
     * and the regular ones for rule eat_top_right
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_eat_top_right() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule eat_top_right
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_eat_top_right(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule eat_top_right
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule eat_top_right
     */
    private java.util.Hashtable getMapRuleeat_top_right() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("t", com_uisleandro_jeops_checkers_Task_1);
        result.put("a", com_uisleandro_jeops_checkers_Cell_1);
        result.put("b", com_uisleandro_jeops_checkers_Cell_2);
        result.put("c", com_uisleandro_jeops_checkers_Cell_3);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule eat_top_right.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_eat_top_right(int index) {
        switch (index) {
            case 0: return "com.uisleandro.jeops.checkers.Task";
            case 1: return "com.uisleandro.jeops.checkers.Cell";
            case 2: return "com.uisleandro.jeops.checkers.Cell";
            case 3: return "com.uisleandro.jeops.checkers.Cell";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule eat_top_right.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_eat_top_right(int index) {
        switch (index) {
            case 0: return com.uisleandro.jeops.checkers.Task.class;
            case 1: return com.uisleandro.jeops.checkers.Cell.class;
            case 2: return com.uisleandro.jeops.checkers.Cell.class;
            case 3: return com.uisleandro.jeops.checkers.Cell.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule eat_top_right.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_eat_top_right(int index, Object value) {
        switch (index) {
            case 0: this.com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) value; break;
            case 1: this.com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) value; break;
            case 2: this.com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) value; break;
            case 3: this.com_uisleandro_jeops_checkers_Cell_3 = (com.uisleandro.jeops.checkers.Cell) value; break;
        }
    }

    /**
     * Returns an object declared in the rule eat_top_right.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_eat_top_right(int index) {
        switch (index) {
            case 0: return com_uisleandro_jeops_checkers_Task_1;
            case 1: return com_uisleandro_jeops_checkers_Cell_1;
            case 2: return com_uisleandro_jeops_checkers_Cell_2;
            case 3: return com_uisleandro_jeops_checkers_Cell_3;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule eat_top_right
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_eat_top_right() {
        return new Object[] {
                            com_uisleandro_jeops_checkers_Task_1,
                            com_uisleandro_jeops_checkers_Cell_1,
                            com_uisleandro_jeops_checkers_Cell_2,
                            com_uisleandro_jeops_checkers_Cell_3
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule eat_top_right
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_eat_top_right(Object[] objects) {
        com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) objects[0];
        com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) objects[1];
        com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) objects[2];
        com_uisleandro_jeops_checkers_Cell_3 = (com.uisleandro.jeops.checkers.Cell) objects[3];
    }

    /**
     * Precondition 0 of rule eat_top_right.<p>
     * The original expression was:<br>
     * <code>t.getStatus() == Task.TODO</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_top_right_prec_0() {
        return (com_uisleandro_jeops_checkers_Task_1.getStatus() == Task.TODO);
    }

    /**
     * Precondition 1 of rule eat_top_right.<p>
     * The original expression was:<br>
     * <code>t.getTask() == Task.MOVIMENTS</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_top_right_prec_1() {
        return (com_uisleandro_jeops_checkers_Task_1.getTask() == Task.MOVIMENTS);
    }

    /**
     * Precondition 2 of rule eat_top_right.<p>
     * The original expression was:<br>
     * <code>a.getCell_top_right() == b.getIndex()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_top_right_prec_2() {
        return (com_uisleandro_jeops_checkers_Cell_1.getCell_top_right() == com_uisleandro_jeops_checkers_Cell_2.getIndex());
    }

    /**
     * Precondition 3 of rule eat_top_right.<p>
     * The original expression was:<br>
     * <code>b.getCell_top_right() == c.getIndex()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_top_right_prec_3() {
        return (com_uisleandro_jeops_checkers_Cell_2.getCell_top_right() == com_uisleandro_jeops_checkers_Cell_3.getIndex());
    }

    /**
     * Precondition 4 of rule eat_top_right.<p>
     * The original expression was:<br>
     * <code>a.getChecker() != null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_top_right_prec_4() {
        return (com_uisleandro_jeops_checkers_Cell_1.getChecker() != null);
    }

    /**
     * Precondition 5 of rule eat_top_right.<p>
     * The original expression was:<br>
     * <code>b.getChecker() != null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_top_right_prec_5() {
        return (com_uisleandro_jeops_checkers_Cell_2.getChecker() != null);
    }

    /**
     * Precondition 6 of rule eat_top_right.<p>
     * The original expression was:<br>
     * <code>c.getChecker() == null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_top_right_prec_6() {
        return (com_uisleandro_jeops_checkers_Cell_3.getChecker() == null);
    }

    /**
     * Precondition 7 of rule eat_top_right.<p>
     * The original expression was:<br>
     * <code>a.getChecker().getBlack() == true</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_top_right_prec_7() {
        return (com_uisleandro_jeops_checkers_Cell_1.getChecker().getBlack() == true);
    }

    /**
     * Precondition 8 of rule eat_top_right.<p>
     * The original expression was:<br>
     * <code>b.getChecker().getBlack() == false</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_top_right_prec_8() {
        return (com_uisleandro_jeops_checkers_Cell_2.getChecker().getBlack() == false);
    }

    /**
     * Checks whether some preconditions of rule eat_top_right is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_top_right_prec(int index) {
        switch (index) {
            case 0: return eat_top_right_prec_0();
            case 1: return eat_top_right_prec_1();
            case 2: return eat_top_right_prec_2();
            case 3: return eat_top_right_prec_3();
            case 4: return eat_top_right_prec_4();
            case 5: return eat_top_right_prec_5();
            case 6: return eat_top_right_prec_6();
            case 7: return eat_top_right_prec_7();
            case 8: return eat_top_right_prec_8();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_eat_top_right(int declIndex) {
        setLocalDeclarations_eat_top_right(declIndex);
        switch (declIndex) {
            case 0:
                if (!eat_top_right_prec_0()) return false;
                if (!eat_top_right_prec_1()) return false;
                return true;
            case 1:
                if (!eat_top_right_prec_4()) return false;
                if (!eat_top_right_prec_7()) return false;
                return true;
            case 2:
                if (!eat_top_right_prec_2()) return false;
                if (!eat_top_right_prec_5()) return false;
                if (!eat_top_right_prec_8()) return false;
                return true;
            case 3:
                if (!eat_top_right_prec_3()) return false;
                if (!eat_top_right_prec_6()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule eat_top_right
     */
    public void eat_top_right() {
	  	System.out.println(com_uisleandro_jeops_checkers_Cell_1.getIndex() +","+ com_uisleandro_jeops_checkers_Cell_2.getIndex() +","+ com_uisleandro_jeops_checkers_Cell_3.getIndex());
    	com_uisleandro_jeops_checkers_Cell_3.setChecker(com_uisleandro_jeops_checkers_Cell_1.getChecker());
    	com_uisleandro_jeops_checkers_Cell_1.setChecker(null);
    	com_uisleandro_jeops_checkers_Cell_2.setChecker(null);
    	retract(com_uisleandro_jeops_checkers_Cell_1);
    	retract(com_uisleandro_jeops_checkers_Cell_2);
    	retract(com_uisleandro_jeops_checkers_Cell_3);
    	tell(com_uisleandro_jeops_checkers_Cell_1);
    	tell(com_uisleandro_jeops_checkers_Cell_2);
    	tell(com_uisleandro_jeops_checkers_Cell_3);
    }


/**/
 
/* compila */

    /**
     * Sets the local declaration for a given declaration index
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_eat_bot_left(int declIndex) {
    }

    /**
     * Returns a dependency table between the local declarations
     * and the regular ones for rule eat_bot_left
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_eat_bot_left() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule eat_bot_left
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_eat_bot_left(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule eat_bot_left
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule eat_bot_left
     */
    private java.util.Hashtable getMapRuleeat_bot_left() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("t", com_uisleandro_jeops_checkers_Task_1);
        result.put("a", com_uisleandro_jeops_checkers_Cell_1);
        result.put("b", com_uisleandro_jeops_checkers_Cell_2);
        result.put("c", com_uisleandro_jeops_checkers_Cell_3);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule eat_bot_left.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_eat_bot_left(int index) {
        switch (index) {
            case 0: return "com.uisleandro.jeops.checkers.Task";
            case 1: return "com.uisleandro.jeops.checkers.Cell";
            case 2: return "com.uisleandro.jeops.checkers.Cell";
            case 3: return "com.uisleandro.jeops.checkers.Cell";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule eat_bot_left.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_eat_bot_left(int index) {
        switch (index) {
            case 0: return com.uisleandro.jeops.checkers.Task.class;
            case 1: return com.uisleandro.jeops.checkers.Cell.class;
            case 2: return com.uisleandro.jeops.checkers.Cell.class;
            case 3: return com.uisleandro.jeops.checkers.Cell.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule eat_bot_left.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_eat_bot_left(int index, Object value) {
        switch (index) {
            case 0: this.com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) value; break;
            case 1: this.com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) value; break;
            case 2: this.com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) value; break;
            case 3: this.com_uisleandro_jeops_checkers_Cell_3 = (com.uisleandro.jeops.checkers.Cell) value; break;
        }
    }

    /**
     * Returns an object declared in the rule eat_bot_left.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_eat_bot_left(int index) {
        switch (index) {
            case 0: return com_uisleandro_jeops_checkers_Task_1;
            case 1: return com_uisleandro_jeops_checkers_Cell_1;
            case 2: return com_uisleandro_jeops_checkers_Cell_2;
            case 3: return com_uisleandro_jeops_checkers_Cell_3;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule eat_bot_left
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_eat_bot_left() {
        return new Object[] {
                            com_uisleandro_jeops_checkers_Task_1,
                            com_uisleandro_jeops_checkers_Cell_1,
                            com_uisleandro_jeops_checkers_Cell_2,
                            com_uisleandro_jeops_checkers_Cell_3
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule eat_bot_left
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_eat_bot_left(Object[] objects) {
        com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) objects[0];
        com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) objects[1];
        com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) objects[2];
        com_uisleandro_jeops_checkers_Cell_3 = (com.uisleandro.jeops.checkers.Cell) objects[3];
    }

    /**
     * Precondition 0 of rule eat_bot_left.<p>
     * The original expression was:<br>
     * <code>t.getStatus() == Task.TODO</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_bot_left_prec_0() {
        return (com_uisleandro_jeops_checkers_Task_1.getStatus() == Task.TODO);
    }

    /**
     * Precondition 1 of rule eat_bot_left.<p>
     * The original expression was:<br>
     * <code>t.getTask() == Task.MOVIMENTS</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_bot_left_prec_1() {
        return (com_uisleandro_jeops_checkers_Task_1.getTask() == Task.MOVIMENTS);
    }

    /**
     * Precondition 2 of rule eat_bot_left.<p>
     * The original expression was:<br>
     * <code>a.getCell_bot_left() == b.getIndex()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_bot_left_prec_2() {
        return (com_uisleandro_jeops_checkers_Cell_1.getCell_bot_left() == com_uisleandro_jeops_checkers_Cell_2.getIndex());
    }

    /**
     * Precondition 3 of rule eat_bot_left.<p>
     * The original expression was:<br>
     * <code>b.getCell_bot_left() == c.getIndex()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_bot_left_prec_3() {
        return (com_uisleandro_jeops_checkers_Cell_2.getCell_bot_left() == com_uisleandro_jeops_checkers_Cell_3.getIndex());
    }

    /**
     * Precondition 4 of rule eat_bot_left.<p>
     * The original expression was:<br>
     * <code>a.getChecker() != null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_bot_left_prec_4() {
        return (com_uisleandro_jeops_checkers_Cell_1.getChecker() != null);
    }

    /**
     * Precondition 5 of rule eat_bot_left.<p>
     * The original expression was:<br>
     * <code>b.getChecker() != null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_bot_left_prec_5() {
        return (com_uisleandro_jeops_checkers_Cell_2.getChecker() != null);
    }

    /**
     * Precondition 6 of rule eat_bot_left.<p>
     * The original expression was:<br>
     * <code>c.getChecker() == null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_bot_left_prec_6() {
        return (com_uisleandro_jeops_checkers_Cell_3.getChecker() == null);
    }

    /**
     * Precondition 7 of rule eat_bot_left.<p>
     * The original expression was:<br>
     * <code>a.getChecker().getBlack() == true</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_bot_left_prec_7() {
        return (com_uisleandro_jeops_checkers_Cell_1.getChecker().getBlack() == true);
    }

    /**
     * Precondition 8 of rule eat_bot_left.<p>
     * The original expression was:<br>
     * <code>b.getChecker().getBlack() == false</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_bot_left_prec_8() {
        return (com_uisleandro_jeops_checkers_Cell_2.getChecker().getBlack() == false);
    }

    /**
     * Checks whether some preconditions of rule eat_bot_left is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_bot_left_prec(int index) {
        switch (index) {
            case 0: return eat_bot_left_prec_0();
            case 1: return eat_bot_left_prec_1();
            case 2: return eat_bot_left_prec_2();
            case 3: return eat_bot_left_prec_3();
            case 4: return eat_bot_left_prec_4();
            case 5: return eat_bot_left_prec_5();
            case 6: return eat_bot_left_prec_6();
            case 7: return eat_bot_left_prec_7();
            case 8: return eat_bot_left_prec_8();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_eat_bot_left(int declIndex) {
        setLocalDeclarations_eat_bot_left(declIndex);
        switch (declIndex) {
            case 0:
                if (!eat_bot_left_prec_0()) return false;
                if (!eat_bot_left_prec_1()) return false;
                return true;
            case 1:
                if (!eat_bot_left_prec_4()) return false;
                if (!eat_bot_left_prec_7()) return false;
                return true;
            case 2:
                if (!eat_bot_left_prec_2()) return false;
                if (!eat_bot_left_prec_5()) return false;
                if (!eat_bot_left_prec_8()) return false;
                return true;
            case 3:
                if (!eat_bot_left_prec_3()) return false;
                if (!eat_bot_left_prec_6()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule eat_bot_left
     */
    public void eat_bot_left() {
	  	System.out.println(com_uisleandro_jeops_checkers_Cell_1.getIndex() +","+ com_uisleandro_jeops_checkers_Cell_2.getIndex() +","+ com_uisleandro_jeops_checkers_Cell_3.getIndex());
    	com_uisleandro_jeops_checkers_Cell_3.setChecker(com_uisleandro_jeops_checkers_Cell_1.getChecker());
    	com_uisleandro_jeops_checkers_Cell_1.setChecker(null);
    	com_uisleandro_jeops_checkers_Cell_2.setChecker(null);
    	retract(com_uisleandro_jeops_checkers_Cell_1);
    	retract(com_uisleandro_jeops_checkers_Cell_2);
    	retract(com_uisleandro_jeops_checkers_Cell_3);
    	tell(com_uisleandro_jeops_checkers_Cell_1);
    	tell(com_uisleandro_jeops_checkers_Cell_2);
    	tell(com_uisleandro_jeops_checkers_Cell_3);
    }



/* compila */

    /**
     * Sets the local declaration for a given declaration index
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_eat_bot_right(int declIndex) {
    }

    /**
     * Returns a dependency table between the local declarations
     * and the regular ones for rule eat_bot_right
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_eat_bot_right() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule eat_bot_right
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_eat_bot_right(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule eat_bot_right
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule eat_bot_right
     */
    private java.util.Hashtable getMapRuleeat_bot_right() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("t", com_uisleandro_jeops_checkers_Task_1);
        result.put("a", com_uisleandro_jeops_checkers_Cell_1);
        result.put("b", com_uisleandro_jeops_checkers_Cell_2);
        result.put("c", com_uisleandro_jeops_checkers_Cell_3);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule eat_bot_right.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_eat_bot_right(int index) {
        switch (index) {
            case 0: return "com.uisleandro.jeops.checkers.Task";
            case 1: return "com.uisleandro.jeops.checkers.Cell";
            case 2: return "com.uisleandro.jeops.checkers.Cell";
            case 3: return "com.uisleandro.jeops.checkers.Cell";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule eat_bot_right.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_eat_bot_right(int index) {
        switch (index) {
            case 0: return com.uisleandro.jeops.checkers.Task.class;
            case 1: return com.uisleandro.jeops.checkers.Cell.class;
            case 2: return com.uisleandro.jeops.checkers.Cell.class;
            case 3: return com.uisleandro.jeops.checkers.Cell.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule eat_bot_right.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_eat_bot_right(int index, Object value) {
        switch (index) {
            case 0: this.com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) value; break;
            case 1: this.com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) value; break;
            case 2: this.com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) value; break;
            case 3: this.com_uisleandro_jeops_checkers_Cell_3 = (com.uisleandro.jeops.checkers.Cell) value; break;
        }
    }

    /**
     * Returns an object declared in the rule eat_bot_right.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_eat_bot_right(int index) {
        switch (index) {
            case 0: return com_uisleandro_jeops_checkers_Task_1;
            case 1: return com_uisleandro_jeops_checkers_Cell_1;
            case 2: return com_uisleandro_jeops_checkers_Cell_2;
            case 3: return com_uisleandro_jeops_checkers_Cell_3;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule eat_bot_right
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_eat_bot_right() {
        return new Object[] {
                            com_uisleandro_jeops_checkers_Task_1,
                            com_uisleandro_jeops_checkers_Cell_1,
                            com_uisleandro_jeops_checkers_Cell_2,
                            com_uisleandro_jeops_checkers_Cell_3
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule eat_bot_right
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_eat_bot_right(Object[] objects) {
        com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) objects[0];
        com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) objects[1];
        com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) objects[2];
        com_uisleandro_jeops_checkers_Cell_3 = (com.uisleandro.jeops.checkers.Cell) objects[3];
    }

    /**
     * Precondition 0 of rule eat_bot_right.<p>
     * The original expression was:<br>
     * <code>t.getStatus() == Task.TODO</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_bot_right_prec_0() {
        return (com_uisleandro_jeops_checkers_Task_1.getStatus() == Task.TODO);
    }

    /**
     * Precondition 1 of rule eat_bot_right.<p>
     * The original expression was:<br>
     * <code>t.getTask() == Task.MOVIMENTS</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_bot_right_prec_1() {
        return (com_uisleandro_jeops_checkers_Task_1.getTask() == Task.MOVIMENTS);
    }

    /**
     * Precondition 2 of rule eat_bot_right.<p>
     * The original expression was:<br>
     * <code>a.getCell_bot_right() == b.getIndex()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_bot_right_prec_2() {
        return (com_uisleandro_jeops_checkers_Cell_1.getCell_bot_right() == com_uisleandro_jeops_checkers_Cell_2.getIndex());
    }

    /**
     * Precondition 3 of rule eat_bot_right.<p>
     * The original expression was:<br>
     * <code>b.getCell_bot_right() == c.getIndex()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_bot_right_prec_3() {
        return (com_uisleandro_jeops_checkers_Cell_2.getCell_bot_right() == com_uisleandro_jeops_checkers_Cell_3.getIndex());
    }

    /**
     * Precondition 4 of rule eat_bot_right.<p>
     * The original expression was:<br>
     * <code>a.getChecker() != null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_bot_right_prec_4() {
        return (com_uisleandro_jeops_checkers_Cell_1.getChecker() != null);
    }

    /**
     * Precondition 5 of rule eat_bot_right.<p>
     * The original expression was:<br>
     * <code>b.getChecker() != null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_bot_right_prec_5() {
        return (com_uisleandro_jeops_checkers_Cell_2.getChecker() != null);
    }

    /**
     * Precondition 6 of rule eat_bot_right.<p>
     * The original expression was:<br>
     * <code>c.getChecker() == null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_bot_right_prec_6() {
        return (com_uisleandro_jeops_checkers_Cell_3.getChecker() == null);
    }

    /**
     * Precondition 7 of rule eat_bot_right.<p>
     * The original expression was:<br>
     * <code>a.getChecker().getBlack() == true</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_bot_right_prec_7() {
        return (com_uisleandro_jeops_checkers_Cell_1.getChecker().getBlack() == true);
    }

    /**
     * Precondition 8 of rule eat_bot_right.<p>
     * The original expression was:<br>
     * <code>b.getChecker().getBlack() == false</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_bot_right_prec_8() {
        return (com_uisleandro_jeops_checkers_Cell_2.getChecker().getBlack() == false);
    }

    /**
     * Checks whether some preconditions of rule eat_bot_right is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean eat_bot_right_prec(int index) {
        switch (index) {
            case 0: return eat_bot_right_prec_0();
            case 1: return eat_bot_right_prec_1();
            case 2: return eat_bot_right_prec_2();
            case 3: return eat_bot_right_prec_3();
            case 4: return eat_bot_right_prec_4();
            case 5: return eat_bot_right_prec_5();
            case 6: return eat_bot_right_prec_6();
            case 7: return eat_bot_right_prec_7();
            case 8: return eat_bot_right_prec_8();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_eat_bot_right(int declIndex) {
        setLocalDeclarations_eat_bot_right(declIndex);
        switch (declIndex) {
            case 0:
                if (!eat_bot_right_prec_0()) return false;
                if (!eat_bot_right_prec_1()) return false;
                return true;
            case 1:
                if (!eat_bot_right_prec_4()) return false;
                if (!eat_bot_right_prec_7()) return false;
                return true;
            case 2:
                if (!eat_bot_right_prec_2()) return false;
                if (!eat_bot_right_prec_5()) return false;
                if (!eat_bot_right_prec_8()) return false;
                return true;
            case 3:
                if (!eat_bot_right_prec_3()) return false;
                if (!eat_bot_right_prec_6()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule eat_bot_right
     */
    public void eat_bot_right() {
	  	System.out.println(com_uisleandro_jeops_checkers_Cell_1.getIndex() +","+ com_uisleandro_jeops_checkers_Cell_2.getIndex() +","+ com_uisleandro_jeops_checkers_Cell_3.getIndex());
    	com_uisleandro_jeops_checkers_Cell_3.setChecker(com_uisleandro_jeops_checkers_Cell_1.getChecker());
    	com_uisleandro_jeops_checkers_Cell_1.setChecker(null);
    	com_uisleandro_jeops_checkers_Cell_2.setChecker(null);
    	retract(com_uisleandro_jeops_checkers_Cell_1);
    	retract(com_uisleandro_jeops_checkers_Cell_2);
    	retract(com_uisleandro_jeops_checkers_Cell_3);
    	tell(com_uisleandro_jeops_checkers_Cell_1);
    	tell(com_uisleandro_jeops_checkers_Cell_2);
    	tell(com_uisleandro_jeops_checkers_Cell_3);
    }




/* seems works */

    /**
     * Sets the local declaration for a given declaration index
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_move_left(int declIndex) {
    }

    /**
     * Returns a dependency table between the local declarations
     * and the regular ones for rule move_left
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_move_left() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule move_left
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_move_left(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule move_left
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule move_left
     */
    private java.util.Hashtable getMapRulemove_left() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("a", com_uisleandro_jeops_checkers_Cell_1);
        result.put("b", com_uisleandro_jeops_checkers_Cell_2);
        result.put("t", com_uisleandro_jeops_checkers_Task_1);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule move_left.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_move_left(int index) {
        switch (index) {
            case 0: return "com.uisleandro.jeops.checkers.Cell";
            case 1: return "com.uisleandro.jeops.checkers.Cell";
            case 2: return "com.uisleandro.jeops.checkers.Task";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule move_left.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_move_left(int index) {
        switch (index) {
            case 0: return com.uisleandro.jeops.checkers.Cell.class;
            case 1: return com.uisleandro.jeops.checkers.Cell.class;
            case 2: return com.uisleandro.jeops.checkers.Task.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule move_left.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_move_left(int index, Object value) {
        switch (index) {
            case 0: this.com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) value; break;
            case 1: this.com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) value; break;
            case 2: this.com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) value; break;
        }
    }

    /**
     * Returns an object declared in the rule move_left.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_move_left(int index) {
        switch (index) {
            case 0: return com_uisleandro_jeops_checkers_Cell_1;
            case 1: return com_uisleandro_jeops_checkers_Cell_2;
            case 2: return com_uisleandro_jeops_checkers_Task_1;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule move_left
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_move_left() {
        return new Object[] {
                            com_uisleandro_jeops_checkers_Cell_1,
                            com_uisleandro_jeops_checkers_Cell_2,
                            com_uisleandro_jeops_checkers_Task_1
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule move_left
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_move_left(Object[] objects) {
        com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) objects[0];
        com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) objects[1];
        com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) objects[2];
    }

    /**
     * Precondition 0 of rule move_left.<p>
     * The original expression was:<br>
     * <code>t.getStatus() == Task.TODO</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean move_left_prec_0() {
        return (com_uisleandro_jeops_checkers_Task_1.getStatus() == Task.TODO);
    }

    /**
     * Precondition 1 of rule move_left.<p>
     * The original expression was:<br>
     * <code>t.getTask() == Task.MOVIMENTS</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean move_left_prec_1() {
        return (com_uisleandro_jeops_checkers_Task_1.getTask() == Task.MOVIMENTS);
    }

    /**
     * Precondition 2 of rule move_left.<p>
     * The original expression was:<br>
     * <code>a.getCell_top_left() == b.getIndex()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean move_left_prec_2() {
        return (com_uisleandro_jeops_checkers_Cell_1.getCell_top_left() == com_uisleandro_jeops_checkers_Cell_2.getIndex());
    }

    /**
     * Precondition 3 of rule move_left.<p>
     * The original expression was:<br>
     * <code>a.getChecker() != null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean move_left_prec_3() {
        return (com_uisleandro_jeops_checkers_Cell_1.getChecker() != null);
    }

    /**
     * Precondition 4 of rule move_left.<p>
     * The original expression was:<br>
     * <code>b.getChecker() == null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean move_left_prec_4() {
        return (com_uisleandro_jeops_checkers_Cell_2.getChecker() == null);
    }

    /**
     * Precondition 5 of rule move_left.<p>
     * The original expression was:<br>
     * <code>a.getChecker().getBlack() == true</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean move_left_prec_5() {
        return (com_uisleandro_jeops_checkers_Cell_1.getChecker().getBlack() == true);
    }

    /**
     * Checks whether some preconditions of rule move_left is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean move_left_prec(int index) {
        switch (index) {
            case 0: return move_left_prec_0();
            case 1: return move_left_prec_1();
            case 2: return move_left_prec_2();
            case 3: return move_left_prec_3();
            case 4: return move_left_prec_4();
            case 5: return move_left_prec_5();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_move_left(int declIndex) {
        setLocalDeclarations_move_left(declIndex);
        switch (declIndex) {
            case 0:
                if (!move_left_prec_3()) return false;
                if (!move_left_prec_5()) return false;
                return true;
            case 1:
                if (!move_left_prec_2()) return false;
                if (!move_left_prec_4()) return false;
                return true;
            case 2:
                if (!move_left_prec_0()) return false;
                if (!move_left_prec_1()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule move_left
     */
    public void move_left() {
  	System.out.println(com_uisleandro_jeops_checkers_Cell_1.getIndex() +","+ com_uisleandro_jeops_checkers_Cell_2.getIndex());
	com_uisleandro_jeops_checkers_Cell_2.setChecker(com_uisleandro_jeops_checkers_Cell_1.getChecker());
	com_uisleandro_jeops_checkers_Cell_1.setChecker(null);
	com_uisleandro_jeops_checkers_Task_1.setStatus(Task.DOING);
	retract(com_uisleandro_jeops_checkers_Task_1);
	tell(com_uisleandro_jeops_checkers_Task_1);
	retract(com_uisleandro_jeops_checkers_Cell_1);
	retract(com_uisleandro_jeops_checkers_Cell_2);
	tell(com_uisleandro_jeops_checkers_Cell_1);
	tell(com_uisleandro_jeops_checkers_Cell_2);
	System.out.println("TODO->DOING");
    }





    /**
     * Sets the local declaration for a given declaration index
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_move_right(int declIndex) {
    }

    /**
     * Returns a dependency table between the local declarations
     * and the regular ones for rule move_right
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_move_right() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule move_right
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_move_right(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule move_right
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule move_right
     */
    private java.util.Hashtable getMapRulemove_right() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("t", com_uisleandro_jeops_checkers_Task_1);
        result.put("a", com_uisleandro_jeops_checkers_Cell_1);
        result.put("b", com_uisleandro_jeops_checkers_Cell_2);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule move_right.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_move_right(int index) {
        switch (index) {
            case 0: return "com.uisleandro.jeops.checkers.Task";
            case 1: return "com.uisleandro.jeops.checkers.Cell";
            case 2: return "com.uisleandro.jeops.checkers.Cell";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule move_right.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_move_right(int index) {
        switch (index) {
            case 0: return com.uisleandro.jeops.checkers.Task.class;
            case 1: return com.uisleandro.jeops.checkers.Cell.class;
            case 2: return com.uisleandro.jeops.checkers.Cell.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule move_right.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_move_right(int index, Object value) {
        switch (index) {
            case 0: this.com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) value; break;
            case 1: this.com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) value; break;
            case 2: this.com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) value; break;
        }
    }

    /**
     * Returns an object declared in the rule move_right.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_move_right(int index) {
        switch (index) {
            case 0: return com_uisleandro_jeops_checkers_Task_1;
            case 1: return com_uisleandro_jeops_checkers_Cell_1;
            case 2: return com_uisleandro_jeops_checkers_Cell_2;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule move_right
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_move_right() {
        return new Object[] {
                            com_uisleandro_jeops_checkers_Task_1,
                            com_uisleandro_jeops_checkers_Cell_1,
                            com_uisleandro_jeops_checkers_Cell_2
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule move_right
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_move_right(Object[] objects) {
        com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) objects[0];
        com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) objects[1];
        com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) objects[2];
    }

    /**
     * Precondition 0 of rule move_right.<p>
     * The original expression was:<br>
     * <code>t.getStatus() == Task.DOING</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean move_right_prec_0() {
        return (com_uisleandro_jeops_checkers_Task_1.getStatus() == Task.DOING);
    }

    /**
     * Precondition 1 of rule move_right.<p>
     * The original expression was:<br>
     * <code>t.getTask() == Task.MOVIMENTS</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean move_right_prec_1() {
        return (com_uisleandro_jeops_checkers_Task_1.getTask() == Task.MOVIMENTS);
    }

    /**
     * Precondition 2 of rule move_right.<p>
     * The original expression was:<br>
     * <code>a.getCell_top_right() == b.getIndex()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean move_right_prec_2() {
        return (com_uisleandro_jeops_checkers_Cell_1.getCell_top_right() == com_uisleandro_jeops_checkers_Cell_2.getIndex());
    }

    /**
     * Precondition 3 of rule move_right.<p>
     * The original expression was:<br>
     * <code>a.getChecker() != null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean move_right_prec_3() {
        return (com_uisleandro_jeops_checkers_Cell_1.getChecker() != null);
    }

    /**
     * Precondition 4 of rule move_right.<p>
     * The original expression was:<br>
     * <code>b.getChecker() == null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean move_right_prec_4() {
        return (com_uisleandro_jeops_checkers_Cell_2.getChecker() == null);
    }

    /**
     * Precondition 5 of rule move_right.<p>
     * The original expression was:<br>
     * <code>a.getChecker().getBlack() == true</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean move_right_prec_5() {
        return (com_uisleandro_jeops_checkers_Cell_1.getChecker().getBlack() == true);
    }

    /**
     * Checks whether some preconditions of rule move_right is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean move_right_prec(int index) {
        switch (index) {
            case 0: return move_right_prec_0();
            case 1: return move_right_prec_1();
            case 2: return move_right_prec_2();
            case 3: return move_right_prec_3();
            case 4: return move_right_prec_4();
            case 5: return move_right_prec_5();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_move_right(int declIndex) {
        setLocalDeclarations_move_right(declIndex);
        switch (declIndex) {
            case 0:
                if (!move_right_prec_0()) return false;
                if (!move_right_prec_1()) return false;
                return true;
            case 1:
                if (!move_right_prec_3()) return false;
                if (!move_right_prec_5()) return false;
                return true;
            case 2:
                if (!move_right_prec_2()) return false;
                if (!move_right_prec_4()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule move_right
     */
    public void move_right() {
	com_uisleandro_jeops_checkers_Task_1.setStatus(Task.DONE);
	retract(com_uisleandro_jeops_checkers_Task_1);
	tell(com_uisleandro_jeops_checkers_Task_1);
  	System.out.println(com_uisleandro_jeops_checkers_Cell_1.getIndex() +","+ com_uisleandro_jeops_checkers_Cell_2.getIndex());
	com_uisleandro_jeops_checkers_Cell_2.setChecker(com_uisleandro_jeops_checkers_Cell_1.getChecker());
	com_uisleandro_jeops_checkers_Cell_1.setChecker(null);
	retract(com_uisleandro_jeops_checkers_Cell_1);
	retract(com_uisleandro_jeops_checkers_Cell_2);
	tell(com_uisleandro_jeops_checkers_Cell_1);
	tell(com_uisleandro_jeops_checkers_Cell_2);
	System.out.println("DOING->DONE");
	
    }





    
/*

Arvore de decisao:

o jogo continua
    nao
        ->  contar os pontos
    sim
    e a minha vez de jogar
        nao
            ->  aguardar
        sim
        posso comer
            nao
            posso mover
                nao
                    ->  finalizar o jogo
                sim
                    ->  mover
            sim
                ->  comer



se 
   o jogo nao continua
   e nao mostrou os pontos
        mostrou os pontos(sim);
        mostrar os pontos();

se 
   o jogo continua
   nao e a minha vez de jogar
        nao fazer nada();

se 
    o jogo continua
    é minha vez de jogar
    e posso comer
        flag_comer();

se 
    o jogo continua
    é minha vez de jogar
    e nao posso comer
    e posso mover
        flag_mover();

se
    o jogo continua
    é minha vez de jogar
    e nao posso comer
    e nao posso mover
        finalizar o jogo();

*/
  
/*NOTE: you have to notice that the rules will run one after another you can use it to
 * controll the program better
 * */

/* once shows the changed cells @_@, cells who was never in this role */
    
    /**
     * Sets the local declaration for a given declaration index
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_inform(int declIndex) {
    }

    /**
     * Returns a dependency table between the local declarations
     * and the regular ones for rule inform
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_inform() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule inform
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_inform(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule inform
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule inform
     */
    private java.util.Hashtable getMapRuleinform() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("c", com_uisleandro_jeops_checkers_Cell_1);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule inform.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_inform(int index) {
        switch (index) {
            case 0: return "com.uisleandro.jeops.checkers.Cell";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule inform.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_inform(int index) {
        switch (index) {
            case 0: return com.uisleandro.jeops.checkers.Cell.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule inform.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_inform(int index, Object value) {
        switch (index) {
            case 0: this.com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) value; break;
        }
    }

    /**
     * Returns an object declared in the rule inform.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_inform(int index) {
        switch (index) {
            case 0: return com_uisleandro_jeops_checkers_Cell_1;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule inform
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_inform() {
        return new Object[] {
                            com_uisleandro_jeops_checkers_Cell_1
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule inform
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_inform(Object[] objects) {
        com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) objects[0];
    }

    /**
     * Checks whether some preconditions of rule inform is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean inform_prec(int index) {
        switch (index) {
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_inform(int declIndex) {
        setLocalDeclarations_inform(declIndex);
        switch (declIndex) {
            case 0:
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule inform
     */
    public void inform() {
            System.out.println(com_uisleandro_jeops_checkers_Cell_1.toString());
        }


/**/



/**/
//keep the program running    

    /**
     * Sets the local declaration for a given declaration index
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_add_loop(int declIndex) {
    }

    /**
     * Returns a dependency table between the local declarations
     * and the regular ones for rule add_loop
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_add_loop() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule add_loop
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_add_loop(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule add_loop
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule add_loop
     */
    private java.util.Hashtable getMapRuleadd_loop() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("h", com_uisleandro_jeops_checkers_HumanMove_1);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule add_loop.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_add_loop(int index) {
        switch (index) {
            case 0: return "com.uisleandro.jeops.checkers.HumanMove";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule add_loop.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_add_loop(int index) {
        switch (index) {
            case 0: return com.uisleandro.jeops.checkers.HumanMove.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule add_loop.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_add_loop(int index, Object value) {
        switch (index) {
            case 0: this.com_uisleandro_jeops_checkers_HumanMove_1 = (com.uisleandro.jeops.checkers.HumanMove) value; break;
        }
    }

    /**
     * Returns an object declared in the rule add_loop.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_add_loop(int index) {
        switch (index) {
            case 0: return com_uisleandro_jeops_checkers_HumanMove_1;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule add_loop
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_add_loop() {
        return new Object[] {
                            com_uisleandro_jeops_checkers_HumanMove_1
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule add_loop
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_add_loop(Object[] objects) {
        com_uisleandro_jeops_checkers_HumanMove_1 = (com.uisleandro.jeops.checkers.HumanMove) objects[0];
    }

    /**
     * Precondition 0 of rule add_loop.<p>
     * The original expression was:<br>
     * <code>h.getStatus() == HumanMove.TODO</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean add_loop_prec_0() {
        return (com_uisleandro_jeops_checkers_HumanMove_1.getStatus() == HumanMove.TODO);
    }

    /**
     * Checks whether some preconditions of rule add_loop is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean add_loop_prec(int index) {
        switch (index) {
            case 0: return add_loop_prec_0();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_add_loop(int declIndex) {
        setLocalDeclarations_add_loop(declIndex);
        switch (declIndex) {
            case 0:
                if (!add_loop_prec_0()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule add_loop
     */
    public void add_loop() {
		com_uisleandro_jeops_checkers_HumanMove_1.getInput();
		com_uisleandro_jeops_checkers_HumanMove_1.setStatus(HumanMove.DOING);
		modified(com_uisleandro_jeops_checkers_HumanMove_1);
		System.out.println(com_uisleandro_jeops_checkers_HumanMove_1);
    }


/**/



    /**
     * Sets the local declaration for a given declaration index
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_human_move(int declIndex) {
    }

    /**
     * Returns a dependency table between the local declarations
     * and the regular ones for rule human_move
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_human_move() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule human_move
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_human_move(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule human_move
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule human_move
     */
    private java.util.Hashtable getMapRulehuman_move() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("h", com_uisleandro_jeops_checkers_HumanMove_1);
        result.put("a", com_uisleandro_jeops_checkers_Cell_1);
        result.put("b", com_uisleandro_jeops_checkers_Cell_2);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule human_move.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_human_move(int index) {
        switch (index) {
            case 0: return "com.uisleandro.jeops.checkers.HumanMove";
            case 1: return "com.uisleandro.jeops.checkers.Cell";
            case 2: return "com.uisleandro.jeops.checkers.Cell";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule human_move.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_human_move(int index) {
        switch (index) {
            case 0: return com.uisleandro.jeops.checkers.HumanMove.class;
            case 1: return com.uisleandro.jeops.checkers.Cell.class;
            case 2: return com.uisleandro.jeops.checkers.Cell.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule human_move.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_human_move(int index, Object value) {
        switch (index) {
            case 0: this.com_uisleandro_jeops_checkers_HumanMove_1 = (com.uisleandro.jeops.checkers.HumanMove) value; break;
            case 1: this.com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) value; break;
            case 2: this.com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) value; break;
        }
    }

    /**
     * Returns an object declared in the rule human_move.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_human_move(int index) {
        switch (index) {
            case 0: return com_uisleandro_jeops_checkers_HumanMove_1;
            case 1: return com_uisleandro_jeops_checkers_Cell_1;
            case 2: return com_uisleandro_jeops_checkers_Cell_2;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule human_move
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_human_move() {
        return new Object[] {
                            com_uisleandro_jeops_checkers_HumanMove_1,
                            com_uisleandro_jeops_checkers_Cell_1,
                            com_uisleandro_jeops_checkers_Cell_2
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule human_move
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_human_move(Object[] objects) {
        com_uisleandro_jeops_checkers_HumanMove_1 = (com.uisleandro.jeops.checkers.HumanMove) objects[0];
        com_uisleandro_jeops_checkers_Cell_1 = (com.uisleandro.jeops.checkers.Cell) objects[1];
        com_uisleandro_jeops_checkers_Cell_2 = (com.uisleandro.jeops.checkers.Cell) objects[2];
    }

    /**
     * Precondition 0 of rule human_move.<p>
     * The original expression was:<br>
     * <code>h.getStatus() == HumanMove.DOING</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean human_move_prec_0() {
        return (com_uisleandro_jeops_checkers_HumanMove_1.getStatus() == HumanMove.DOING);
    }

    /**
     * Precondition 1 of rule human_move.<p>
     * The original expression was:<br>
     * <code>h.getAction() == HumanMove.WALK</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean human_move_prec_1() {
        return (com_uisleandro_jeops_checkers_HumanMove_1.getAction() == HumanMove.WALK);
    }

    /**
     * Precondition 2 of rule human_move.<p>
     * The original expression was:<br>
     * <code>a.getIndex() == h.getSource()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean human_move_prec_2() {
        return (com_uisleandro_jeops_checkers_Cell_1.getIndex() == com_uisleandro_jeops_checkers_HumanMove_1.getSource());
    }

    /**
     * Precondition 3 of rule human_move.<p>
     * The original expression was:<br>
     * <code>b.getIndex() == h.getTarget()</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean human_move_prec_3() {
        return (com_uisleandro_jeops_checkers_Cell_2.getIndex() == com_uisleandro_jeops_checkers_HumanMove_1.getTarget());
    }

    /**
     * Precondition 4 of rule human_move.<p>
     * The original expression was:<br>
     * <code>a.getChecker() != null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean human_move_prec_4() {
        return (com_uisleandro_jeops_checkers_Cell_1.getChecker() != null);
    }

    /**
     * Precondition 5 of rule human_move.<p>
     * The original expression was:<br>
     * <code>b.getChecker() == null</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean human_move_prec_5() {
        return (com_uisleandro_jeops_checkers_Cell_2.getChecker() == null);
    }

    /**
     * Checks whether some preconditions of rule human_move is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean human_move_prec(int index) {
        switch (index) {
            case 0: return human_move_prec_0();
            case 1: return human_move_prec_1();
            case 2: return human_move_prec_2();
            case 3: return human_move_prec_3();
            case 4: return human_move_prec_4();
            case 5: return human_move_prec_5();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_human_move(int declIndex) {
        setLocalDeclarations_human_move(declIndex);
        switch (declIndex) {
            case 0:
                if (!human_move_prec_0()) return false;
                if (!human_move_prec_1()) return false;
                return true;
            case 1:
                if (!human_move_prec_2()) return false;
                if (!human_move_prec_4()) return false;
                return true;
            case 2:
                if (!human_move_prec_3()) return false;
                if (!human_move_prec_5()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule human_move
     */
    public void human_move() {
	    com_uisleandro_jeops_checkers_Cell_2.setChecker(com_uisleandro_jeops_checkers_Cell_1.getChecker());
	    com_uisleandro_jeops_checkers_Cell_1.setChecker(null);
		com_uisleandro_jeops_checkers_HumanMove_1.setStatus(HumanMove.DONE);
		modified(com_uisleandro_jeops_checkers_HumanMove_1);
		modified(com_uisleandro_jeops_checkers_Cell_1);
		modified(com_uisleandro_jeops_checkers_Cell_2);
		System.out.println("human moviment");
    }




    /**
     * Sets the local declaration for a given declaration index
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_add_human_todo(int declIndex) {
    }

    /**
     * Returns a dependency table between the local declarations
     * and the regular ones for rule add_human_todo
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_add_human_todo() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule add_human_todo
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_add_human_todo(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule add_human_todo
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule add_human_todo
     */
    private java.util.Hashtable getMapRuleadd_human_todo() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("h", com_uisleandro_jeops_checkers_HumanMove_1);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule add_human_todo.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_add_human_todo(int index) {
        switch (index) {
            case 0: return "com.uisleandro.jeops.checkers.HumanMove";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule add_human_todo.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_add_human_todo(int index) {
        switch (index) {
            case 0: return com.uisleandro.jeops.checkers.HumanMove.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule add_human_todo.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_add_human_todo(int index, Object value) {
        switch (index) {
            case 0: this.com_uisleandro_jeops_checkers_HumanMove_1 = (com.uisleandro.jeops.checkers.HumanMove) value; break;
        }
    }

    /**
     * Returns an object declared in the rule add_human_todo.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_add_human_todo(int index) {
        switch (index) {
            case 0: return com_uisleandro_jeops_checkers_HumanMove_1;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule add_human_todo
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_add_human_todo() {
        return new Object[] {
                            com_uisleandro_jeops_checkers_HumanMove_1
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule add_human_todo
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_add_human_todo(Object[] objects) {
        com_uisleandro_jeops_checkers_HumanMove_1 = (com.uisleandro.jeops.checkers.HumanMove) objects[0];
    }

    /**
     * Precondition 0 of rule add_human_todo.<p>
     * The original expression was:<br>
     * <code>h.getStatus() == HumanMove.DONE</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean add_human_todo_prec_0() {
        return (com_uisleandro_jeops_checkers_HumanMove_1.getStatus() == HumanMove.DONE);
    }

    /**
     * Checks whether some preconditions of rule add_human_todo is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean add_human_todo_prec(int index) {
        switch (index) {
            case 0: return add_human_todo_prec_0();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_add_human_todo(int declIndex) {
        setLocalDeclarations_add_human_todo(declIndex);
        switch (declIndex) {
            case 0:
                if (!add_human_todo_prec_0()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule add_human_todo
     */
    public void add_human_todo() {
		com_uisleandro_jeops_checkers_HumanMove_1.setStatus(HumanMove.TODO);
		modified(com_uisleandro_jeops_checkers_HumanMove_1);
    }


/**/
//keep the program running    

    /**
     * Sets the local declaration for a given declaration index
     *
     * @param declIndex the index of the declaration.
     */
    private void setLocalDeclarations_add_move_left(int declIndex) {
    }

    /**
     * Returns a dependency table between the local declarations
     * and the regular ones for rule add_move_left
     *
     * @return a dependency table between the local declarations
     *          and the regular ones for this rule.
     */
    private Object[][] getLocalDeclarationDependency_add_move_left() {
        return EMPTY_OBJECT_DOUBLE_ARRAY;
    }

    /**
     * Defines all variables bound to the local declarations 
     * of rule add_move_left
     *
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations
     *          of this rule.
     */
    protected void setLocalObjects_add_move_left(Object[][] objects) {
    }

    /**
     * Returns the mapping of declared identifiers into corresponding
     * value for the rule add_move_left
     *
     * @return the mapping of declared identifiers into corresponding
     *          value for the rule add_move_left
     */
    private java.util.Hashtable getMapRuleadd_move_left() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("t", com_uisleandro_jeops_checkers_Task_1);
        return result;
    }

    /**
     * Returns the name of the class of one declared object for
     * rule add_move_left.
     *
     * @param index the index of the declaration
     * @return the name of the class of the declared objects for
     *          this rule.
     */
    private String getDeclaredClassName_add_move_left(int index) {
        switch (index) {
            case 0: return "com.uisleandro.jeops.checkers.Task";
            default: return null;
        }
    }

    /**
     * Returns the class of one declared object for rule add_move_left.
     *
     * @param index the index of the declaration
     * @return the class of the declared objects for this rule.
     */
    private Class getDeclaredClass_add_move_left(int index) {
        switch (index) {
            case 0: return com.uisleandro.jeops.checkers.Task.class;
            default: return null;
        }
    }

    /**
     * Sets an object declared in the rule add_move_left.
     *
     * @param index the index of the declared object
     * @param value the value of the object being set.
     */
    private void setObject_add_move_left(int index, Object value) {
        switch (index) {
            case 0: this.com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) value; break;
        }
    }

    /**
     * Returns an object declared in the rule add_move_left.
     *
     * @param index the index of the declared object
     * @return the value of the corresponding object.
     */
    private Object getObject_add_move_left(int index) {
        switch (index) {
            case 0: return com_uisleandro_jeops_checkers_Task_1;
            default: return null;
        }
    }

    /**
     * Returns all variables bound to the declarations 
     * of rule add_move_left
     *
     * @return an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected Object[] getObjects_add_move_left() {
        return new Object[] {
                            com_uisleandro_jeops_checkers_Task_1
                            };
    }

    /**
     * Defines all variables bound to the declarations 
     * of rule add_move_left
     *
     * @param objects an object array of the variables bound to the
     *          declarations of this rule.
     */
    protected void setObjects_add_move_left(Object[] objects) {
        com_uisleandro_jeops_checkers_Task_1 = (com.uisleandro.jeops.checkers.Task) objects[0];
    }

    /**
     * Precondition 0 of rule add_move_left.<p>
     * The original expression was:<br>
     * <code>t.getStatus() == Task.DONE</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean add_move_left_prec_0() {
        return (com_uisleandro_jeops_checkers_Task_1.getStatus() == Task.DONE);
    }

    /**
     * Precondition 1 of rule add_move_left.<p>
     * The original expression was:<br>
     * <code>t.getTask() == Task.MOVIMENTS</code>
     *
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean add_move_left_prec_1() {
        return (com_uisleandro_jeops_checkers_Task_1.getTask() == Task.MOVIMENTS);
    }

    /**
     * Checks whether some preconditions of rule add_move_left is satisfied.
     *
     * @param index the index of the precondition to be checked.
     * @return <code>true</code> if the precondition is satisfied;
     *          <code>false</code> otherwise.
     */
    public boolean add_move_left_prec(int index) {
        switch (index) {
            case 0: return add_move_left_prec_0();
            case 1: return add_move_left_prec_1();
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference some declared element of the declarations are
     * true.
     *
     * @param declIndex the index of the declared element.
     * @return <code>true</code> if the preconditions that reference
     *          up to the given declaration are true;
     *          <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration_add_move_left(int declIndex) {
        setLocalDeclarations_add_move_left(declIndex);
        switch (declIndex) {
            case 0:
                if (!add_move_left_prec_0()) return false;
                if (!add_move_left_prec_1()) return false;
                return true;
            default: return false;
        }
    }

    /**
     * Executes the action part of the rule add_move_left
     */
    public void add_move_left() {
		com_uisleandro_jeops_checkers_Task_1.setStatus(Task.TODO);
		retract(com_uisleandro_jeops_checkers_Task_1);
		tell(com_uisleandro_jeops_checkers_Task_1);
		System.out.println("DONE->TODO");
    }


/*TODO: nao esta comendo por causa do problema das dependencias */

/**/

    

    /**
     * Returns the name of the rules in this class file.
     *
     * @return the name of the rules in this class file.
     */
    public String[] getRuleNames() {
        int numberOfRules = 22;
    	String[] result = new String[numberOfRules];
        result[0] = "define_diagonal_cells";
        result[1] = "end_creating_diagonal";
        result[2] = "define_black_cells";
        result[3] = "end_define_black_cells";
        result[4] = "define_top_right_neighbor";
        result[5] = "define_top_left_neighbor";
        result[6] = "add_black_checkers";
        result[7] = "add_white_checkers";
        result[8] = "wall_blocking_top_right";
        result[9] = "wall_blocking_top_left";
        result[10] = "end_define_map";
        result[11] = "eat_top_left";
        result[12] = "eat_top_right";
        result[13] = "eat_bot_left";
        result[14] = "eat_bot_right";
        result[15] = "move_left";
        result[16] = "move_right";
        result[17] = "inform";
        result[18] = "add_loop";
        result[19] = "human_move";
        result[20] = "add_human_todo";
        result[21] = "add_move_left";
        return result;
    }

    /**
     * Returns the number of declarations of the rules in this class file.
     *
     * @return the number of declarations  of the rules in this class file.
     */
    public int[] getNumberOfDeclarations() {
        int numberOfRules = 22;
    	int[] result = new int[numberOfRules];
        result[0] = 2;
        result[1] = 1;
        result[2] = 3;
        result[3] = 1;
        result[4] = 3;
        result[5] = 3;
        result[6] = 2;
        result[7] = 2;
        result[8] = 3;
        result[9] = 3;
        result[10] = 1;
        result[11] = 4;
        result[12] = 4;
        result[13] = 4;
        result[14] = 4;
        result[15] = 3;
        result[16] = 3;
        result[17] = 1;
        result[18] = 1;
        result[19] = 3;
        result[20] = 1;
        result[21] = 1;
        return result;
    }

    /**
     * Returns the number of local declarations of the rules in this class file.
     *
     * @return the number of local declarations  of the rules in this class file.
     */
    public int[] getNumberOfLocalDeclarations() {
        int numberOfRules = 22;
    	int[] result = new int[numberOfRules];
        result[0] = 1;
        result[1] = 0;
        result[2] = 1;
        result[3] = 0;
        result[4] = 0;
        result[5] = 0;
        result[6] = 0;
        result[7] = 0;
        result[8] = 0;
        result[9] = 0;
        result[10] = 0;
        result[11] = 0;
        result[12] = 0;
        result[13] = 0;
        result[14] = 0;
        result[15] = 0;
        result[16] = 0;
        result[17] = 0;
        result[18] = 0;
        result[19] = 0;
        result[20] = 0;
        result[21] = 0;
        return result;
    }

    /**
     * Returns the number of preconditions of the rules in this class file.
     *
     * @return the number of preconditions  of the rules in this class file.
     */
    public int[] getNumberOfPreconditions() {
        int numberOfRules = 22;
    	int[] result = new int[numberOfRules];
        result[0] = 4;
        result[1] = 2;
        result[2] = 6;
        result[3] = 2;
        result[4] = 5;
        result[5] = 5;
        result[6] = 4;
        result[7] = 4;
        result[8] = 7;
        result[9] = 7;
        result[10] = 2;
        result[11] = 9;
        result[12] = 9;
        result[13] = 9;
        result[14] = 9;
        result[15] = 6;
        result[16] = 6;
        result[17] = 0;
        result[18] = 1;
        result[19] = 6;
        result[20] = 1;
        result[21] = 2;
        return result;
    }

    /**
     * Checks whether some preconditions of some rule is satisfied.
     *
     * @param ruleIndex the index of the rule to be checked
     * @param precIndex the index of the precondition to be checked
     * @return <code>true</code> if the corresponding precondition for the
     *          given rule is satisfied. <code>false</code> otherwise.
     */
    public boolean checkPrecondition(int ruleIndex, int precIndex) {
        switch (ruleIndex) {
            case 0: return define_diagonal_cells_prec(precIndex);
            case 1: return end_creating_diagonal_prec(precIndex);
            case 2: return define_black_cells_prec(precIndex);
            case 3: return end_define_black_cells_prec(precIndex);
            case 4: return define_top_right_neighbor_prec(precIndex);
            case 5: return define_top_left_neighbor_prec(precIndex);
            case 6: return add_black_checkers_prec(precIndex);
            case 7: return add_white_checkers_prec(precIndex);
            case 8: return wall_blocking_top_right_prec(precIndex);
            case 9: return wall_blocking_top_left_prec(precIndex);
            case 10: return end_define_map_prec(precIndex);
            case 11: return eat_top_left_prec(precIndex);
            case 12: return eat_top_right_prec(precIndex);
            case 13: return eat_bot_left_prec(precIndex);
            case 14: return eat_bot_right_prec(precIndex);
            case 15: return move_left_prec(precIndex);
            case 16: return move_right_prec(precIndex);
            case 17: return inform_prec(precIndex);
            case 18: return add_loop_prec(precIndex);
            case 19: return human_move_prec(precIndex);
            case 20: return add_human_todo_prec(precIndex);
            case 21: return add_move_left_prec(precIndex);
            default: return false;
        }
    }

    /**
     * Checks whether all the preconditions of a rule which
     * reference only the elements declared up to the given index
     * are true.
     *
     * @param ruleIndex the index of the rule to be checked
     * @param declIndex the index of the declaration to be checked
     * @return <code>true</code> if all the preconditions of a rule which
     *          reference only the elements declared up to the given index
     *          are satisfied; <code>false</code> otherwise.
     */
    public boolean checkPrecForDeclaration(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return checkPrecForDeclaration_define_diagonal_cells(declIndex);
            case 1: return checkPrecForDeclaration_end_creating_diagonal(declIndex);
            case 2: return checkPrecForDeclaration_define_black_cells(declIndex);
            case 3: return checkPrecForDeclaration_end_define_black_cells(declIndex);
            case 4: return checkPrecForDeclaration_define_top_right_neighbor(declIndex);
            case 5: return checkPrecForDeclaration_define_top_left_neighbor(declIndex);
            case 6: return checkPrecForDeclaration_add_black_checkers(declIndex);
            case 7: return checkPrecForDeclaration_add_white_checkers(declIndex);
            case 8: return checkPrecForDeclaration_wall_blocking_top_right(declIndex);
            case 9: return checkPrecForDeclaration_wall_blocking_top_left(declIndex);
            case 10: return checkPrecForDeclaration_end_define_map(declIndex);
            case 11: return checkPrecForDeclaration_eat_top_left(declIndex);
            case 12: return checkPrecForDeclaration_eat_top_right(declIndex);
            case 13: return checkPrecForDeclaration_eat_bot_left(declIndex);
            case 14: return checkPrecForDeclaration_eat_bot_right(declIndex);
            case 15: return checkPrecForDeclaration_move_left(declIndex);
            case 16: return checkPrecForDeclaration_move_right(declIndex);
            case 17: return checkPrecForDeclaration_inform(declIndex);
            case 18: return checkPrecForDeclaration_add_loop(declIndex);
            case 19: return checkPrecForDeclaration_human_move(declIndex);
            case 20: return checkPrecForDeclaration_add_human_todo(declIndex);
            case 21: return checkPrecForDeclaration_add_move_left(declIndex);
            default: return false;
        }
    }

    /**
     * Returns the class name of an object declared in a rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration
     * @return the class name of the declared object.
     */
    public String getDeclaredClassName(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return getDeclaredClassName_define_diagonal_cells(declIndex);
            case 1: return getDeclaredClassName_end_creating_diagonal(declIndex);
            case 2: return getDeclaredClassName_define_black_cells(declIndex);
            case 3: return getDeclaredClassName_end_define_black_cells(declIndex);
            case 4: return getDeclaredClassName_define_top_right_neighbor(declIndex);
            case 5: return getDeclaredClassName_define_top_left_neighbor(declIndex);
            case 6: return getDeclaredClassName_add_black_checkers(declIndex);
            case 7: return getDeclaredClassName_add_white_checkers(declIndex);
            case 8: return getDeclaredClassName_wall_blocking_top_right(declIndex);
            case 9: return getDeclaredClassName_wall_blocking_top_left(declIndex);
            case 10: return getDeclaredClassName_end_define_map(declIndex);
            case 11: return getDeclaredClassName_eat_top_left(declIndex);
            case 12: return getDeclaredClassName_eat_top_right(declIndex);
            case 13: return getDeclaredClassName_eat_bot_left(declIndex);
            case 14: return getDeclaredClassName_eat_bot_right(declIndex);
            case 15: return getDeclaredClassName_move_left(declIndex);
            case 16: return getDeclaredClassName_move_right(declIndex);
            case 17: return getDeclaredClassName_inform(declIndex);
            case 18: return getDeclaredClassName_add_loop(declIndex);
            case 19: return getDeclaredClassName_human_move(declIndex);
            case 20: return getDeclaredClassName_add_human_todo(declIndex);
            case 21: return getDeclaredClassName_add_move_left(declIndex);
            default: return null;
        }
    }

    /**
     * Returns the class of an object declared in a rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration
     * @return the class of the declared object.
     */
    public Class getDeclaredClass(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return getDeclaredClass_define_diagonal_cells(declIndex);
            case 1: return getDeclaredClass_end_creating_diagonal(declIndex);
            case 2: return getDeclaredClass_define_black_cells(declIndex);
            case 3: return getDeclaredClass_end_define_black_cells(declIndex);
            case 4: return getDeclaredClass_define_top_right_neighbor(declIndex);
            case 5: return getDeclaredClass_define_top_left_neighbor(declIndex);
            case 6: return getDeclaredClass_add_black_checkers(declIndex);
            case 7: return getDeclaredClass_add_white_checkers(declIndex);
            case 8: return getDeclaredClass_wall_blocking_top_right(declIndex);
            case 9: return getDeclaredClass_wall_blocking_top_left(declIndex);
            case 10: return getDeclaredClass_end_define_map(declIndex);
            case 11: return getDeclaredClass_eat_top_left(declIndex);
            case 12: return getDeclaredClass_eat_top_right(declIndex);
            case 13: return getDeclaredClass_eat_bot_left(declIndex);
            case 14: return getDeclaredClass_eat_bot_right(declIndex);
            case 15: return getDeclaredClass_move_left(declIndex);
            case 16: return getDeclaredClass_move_right(declIndex);
            case 17: return getDeclaredClass_inform(declIndex);
            case 18: return getDeclaredClass_add_loop(declIndex);
            case 19: return getDeclaredClass_human_move(declIndex);
            case 20: return getDeclaredClass_add_human_todo(declIndex);
            case 21: return getDeclaredClass_add_move_left(declIndex);
            default: return null;
        }
    }

    /**
     * Fires one of the rules in this rule base.
     *
     * @param ruleIndex the index of the rule to be fired
     */
    protected void internalFireRule(int ruleIndex) {
        switch (ruleIndex) {
            case 0: define_diagonal_cells(); break;
            case 1: end_creating_diagonal(); break;
            case 2: define_black_cells(); break;
            case 3: end_define_black_cells(); break;
            case 4: define_top_right_neighbor(); break;
            case 5: define_top_left_neighbor(); break;
            case 6: add_black_checkers(); break;
            case 7: add_white_checkers(); break;
            case 8: wall_blocking_top_right(); break;
            case 9: wall_blocking_top_left(); break;
            case 10: end_define_map(); break;
            case 11: eat_top_left(); break;
            case 12: eat_top_right(); break;
            case 13: eat_bot_left(); break;
            case 14: eat_bot_right(); break;
            case 15: move_left(); break;
            case 16: move_right(); break;
            case 17: inform(); break;
            case 18: add_loop(); break;
            case 19: human_move(); break;
            case 20: add_human_todo(); break;
            case 21: add_move_left(); break;
        }
    }

    /**
     * Returns the number of rules.
     *
     * @return the number of rules.
     */
    public int getNumberOfRules() {
        return 22;
    }

    /**
     * Returns a table that maps each declared identifier of a rule
     * into its corresponding value.
     *
     * @param ruleIndex the index of the rule
     * @return a table that maps each declared identifier of a rule
     *          into its corresponding value.
     */
    public java.util.Hashtable getInternalRuleMapping(int ruleIndex) {
        switch (ruleIndex) {
            case 0: return getMapRuledefine_diagonal_cells();
            case 1: return getMapRuleend_creating_diagonal();
            case 2: return getMapRuledefine_black_cells();
            case 3: return getMapRuleend_define_black_cells();
            case 4: return getMapRuledefine_top_right_neighbor();
            case 5: return getMapRuledefine_top_left_neighbor();
            case 6: return getMapRuleadd_black_checkers();
            case 7: return getMapRuleadd_white_checkers();
            case 8: return getMapRulewall_blocking_top_right();
            case 9: return getMapRulewall_blocking_top_left();
            case 10: return getMapRuleend_define_map();
            case 11: return getMapRuleeat_top_left();
            case 12: return getMapRuleeat_top_right();
            case 13: return getMapRuleeat_bot_left();
            case 14: return getMapRuleeat_bot_right();
            case 15: return getMapRulemove_left();
            case 16: return getMapRulemove_right();
            case 17: return getMapRuleinform();
            case 18: return getMapRuleadd_loop();
            case 19: return getMapRulehuman_move();
            case 20: return getMapRuleadd_human_todo();
            case 21: return getMapRuleadd_move_left();
            default: return new java.util.Hashtable();
        }
    }

    /**
     * Sets an object that represents a declaration of some rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration in the rule.
     * @param value the value of the object being set.
     */
    public void setObject(int ruleIndex, int declIndex, Object value) {
        switch (ruleIndex) {
            case 0: setObject_define_diagonal_cells(declIndex, value); break;
            case 1: setObject_end_creating_diagonal(declIndex, value); break;
            case 2: setObject_define_black_cells(declIndex, value); break;
            case 3: setObject_end_define_black_cells(declIndex, value); break;
            case 4: setObject_define_top_right_neighbor(declIndex, value); break;
            case 5: setObject_define_top_left_neighbor(declIndex, value); break;
            case 6: setObject_add_black_checkers(declIndex, value); break;
            case 7: setObject_add_white_checkers(declIndex, value); break;
            case 8: setObject_wall_blocking_top_right(declIndex, value); break;
            case 9: setObject_wall_blocking_top_left(declIndex, value); break;
            case 10: setObject_end_define_map(declIndex, value); break;
            case 11: setObject_eat_top_left(declIndex, value); break;
            case 12: setObject_eat_top_right(declIndex, value); break;
            case 13: setObject_eat_bot_left(declIndex, value); break;
            case 14: setObject_eat_bot_right(declIndex, value); break;
            case 15: setObject_move_left(declIndex, value); break;
            case 16: setObject_move_right(declIndex, value); break;
            case 17: setObject_inform(declIndex, value); break;
            case 18: setObject_add_loop(declIndex, value); break;
            case 19: setObject_human_move(declIndex, value); break;
            case 20: setObject_add_human_todo(declIndex, value); break;
            case 21: setObject_add_move_left(declIndex, value); break;
        }
    }

    /**
     * Returns an object that represents a declaration of some rule.
     *
     * @param ruleIndex the index of the rule
     * @param declIndex the index of the declaration in the rule.
     * @return the value of the corresponding object.
     */
    public Object getObject(int ruleIndex, int declIndex) {
        switch (ruleIndex) {
            case 0: return getObject_define_diagonal_cells(declIndex);
            case 1: return getObject_end_creating_diagonal(declIndex);
            case 2: return getObject_define_black_cells(declIndex);
            case 3: return getObject_end_define_black_cells(declIndex);
            case 4: return getObject_define_top_right_neighbor(declIndex);
            case 5: return getObject_define_top_left_neighbor(declIndex);
            case 6: return getObject_add_black_checkers(declIndex);
            case 7: return getObject_add_white_checkers(declIndex);
            case 8: return getObject_wall_blocking_top_right(declIndex);
            case 9: return getObject_wall_blocking_top_left(declIndex);
            case 10: return getObject_end_define_map(declIndex);
            case 11: return getObject_eat_top_left(declIndex);
            case 12: return getObject_eat_top_right(declIndex);
            case 13: return getObject_eat_bot_left(declIndex);
            case 14: return getObject_eat_bot_right(declIndex);
            case 15: return getObject_move_left(declIndex);
            case 16: return getObject_move_right(declIndex);
            case 17: return getObject_inform(declIndex);
            case 18: return getObject_add_loop(declIndex);
            case 19: return getObject_human_move(declIndex);
            case 20: return getObject_add_human_todo(declIndex);
            case 21: return getObject_add_move_left(declIndex);
            default: return null;
        }
    }

    /**
     * Returns a dependency table between the local declarations and the
     * regular ones.
     *
     * @param ruleIndex the index of the rule
     * @return a two-dimensional array with the dependencies between the
     *          local declarations and the regular ones.
     */
    public Object[][] getLocalDeclarationDependency(int ruleIndex) {
        switch (ruleIndex) {
            case 0: return getLocalDeclarationDependency_define_diagonal_cells();
            case 1: return getLocalDeclarationDependency_end_creating_diagonal();
            case 2: return getLocalDeclarationDependency_define_black_cells();
            case 3: return getLocalDeclarationDependency_end_define_black_cells();
            case 4: return getLocalDeclarationDependency_define_top_right_neighbor();
            case 5: return getLocalDeclarationDependency_define_top_left_neighbor();
            case 6: return getLocalDeclarationDependency_add_black_checkers();
            case 7: return getLocalDeclarationDependency_add_white_checkers();
            case 8: return getLocalDeclarationDependency_wall_blocking_top_right();
            case 9: return getLocalDeclarationDependency_wall_blocking_top_left();
            case 10: return getLocalDeclarationDependency_end_define_map();
            case 11: return getLocalDeclarationDependency_eat_top_left();
            case 12: return getLocalDeclarationDependency_eat_top_right();
            case 13: return getLocalDeclarationDependency_eat_bot_left();
            case 14: return getLocalDeclarationDependency_eat_bot_right();
            case 15: return getLocalDeclarationDependency_move_left();
            case 16: return getLocalDeclarationDependency_move_right();
            case 17: return getLocalDeclarationDependency_inform();
            case 18: return getLocalDeclarationDependency_add_loop();
            case 19: return getLocalDeclarationDependency_human_move();
            case 20: return getLocalDeclarationDependency_add_human_todo();
            case 21: return getLocalDeclarationDependency_add_move_left();
            default: return null;
        }
    }
    /**
     * Defines all variables bound to the local declarations of
     * some rule.
     *
     * @param ruleIndex the index of the rule
     * @param objects a 2-dimensional object array whose first column
     *          contains of the variables bound to the local declarations.
     *          of some rule.
     */
    protected void setLocalObjects(int ruleIndex, Object[][] objects) {
        switch (ruleIndex) {
            case 0: setLocalObjects_define_diagonal_cells(objects); break;
            case 1: setLocalObjects_end_creating_diagonal(objects); break;
            case 2: setLocalObjects_define_black_cells(objects); break;
            case 3: setLocalObjects_end_define_black_cells(objects); break;
            case 4: setLocalObjects_define_top_right_neighbor(objects); break;
            case 5: setLocalObjects_define_top_left_neighbor(objects); break;
            case 6: setLocalObjects_add_black_checkers(objects); break;
            case 7: setLocalObjects_add_white_checkers(objects); break;
            case 8: setLocalObjects_wall_blocking_top_right(objects); break;
            case 9: setLocalObjects_wall_blocking_top_left(objects); break;
            case 10: setLocalObjects_end_define_map(objects); break;
            case 11: setLocalObjects_eat_top_left(objects); break;
            case 12: setLocalObjects_eat_top_right(objects); break;
            case 13: setLocalObjects_eat_bot_left(objects); break;
            case 14: setLocalObjects_eat_bot_right(objects); break;
            case 15: setLocalObjects_move_left(objects); break;
            case 16: setLocalObjects_move_right(objects); break;
            case 17: setLocalObjects_inform(objects); break;
            case 18: setLocalObjects_add_loop(objects); break;
            case 19: setLocalObjects_human_move(objects); break;
            case 20: setLocalObjects_add_human_todo(objects); break;
            case 21: setLocalObjects_add_move_left(objects); break;
        }
    }
    /**
     * Returns all variables bound to the declarations of
     * some rule.
     *
     * @param ruleIndex the index of the rule
     * @return an object array of the variables bound to the
     *          declarations of some rule.
     */
    protected Object[] getObjects(int ruleIndex) {
        switch (ruleIndex) {
            case 0: return getObjects_define_diagonal_cells();
            case 1: return getObjects_end_creating_diagonal();
            case 2: return getObjects_define_black_cells();
            case 3: return getObjects_end_define_black_cells();
            case 4: return getObjects_define_top_right_neighbor();
            case 5: return getObjects_define_top_left_neighbor();
            case 6: return getObjects_add_black_checkers();
            case 7: return getObjects_add_white_checkers();
            case 8: return getObjects_wall_blocking_top_right();
            case 9: return getObjects_wall_blocking_top_left();
            case 10: return getObjects_end_define_map();
            case 11: return getObjects_eat_top_left();
            case 12: return getObjects_eat_top_right();
            case 13: return getObjects_eat_bot_left();
            case 14: return getObjects_eat_bot_right();
            case 15: return getObjects_move_left();
            case 16: return getObjects_move_right();
            case 17: return getObjects_inform();
            case 18: return getObjects_add_loop();
            case 19: return getObjects_human_move();
            case 20: return getObjects_add_human_todo();
            case 21: return getObjects_add_move_left();
            default: return null;
        }
    }
    /**
     * Defines all variables bound to the declarations of
     * some rule.
     *
     * @param ruleIndex the index of the rule
     * @param objects an object array of the variables bound to the
     *          declarations of some rule.
     */
    protected void setObjects(int ruleIndex, Object[] objects) {
        switch (ruleIndex) {
            case 0: setObjects_define_diagonal_cells(objects); break;
            case 1: setObjects_end_creating_diagonal(objects); break;
            case 2: setObjects_define_black_cells(objects); break;
            case 3: setObjects_end_define_black_cells(objects); break;
            case 4: setObjects_define_top_right_neighbor(objects); break;
            case 5: setObjects_define_top_left_neighbor(objects); break;
            case 6: setObjects_add_black_checkers(objects); break;
            case 7: setObjects_add_white_checkers(objects); break;
            case 8: setObjects_wall_blocking_top_right(objects); break;
            case 9: setObjects_wall_blocking_top_left(objects); break;
            case 10: setObjects_end_define_map(objects); break;
            case 11: setObjects_eat_top_left(objects); break;
            case 12: setObjects_eat_top_right(objects); break;
            case 13: setObjects_eat_bot_left(objects); break;
            case 14: setObjects_eat_bot_right(objects); break;
            case 15: setObjects_move_left(objects); break;
            case 16: setObjects_move_right(objects); break;
            case 17: setObjects_inform(objects); break;
            case 18: setObjects_add_loop(objects); break;
            case 19: setObjects_human_move(objects); break;
            case 20: setObjects_add_human_todo(objects); break;
            case 21: setObjects_add_move_left(objects); break;
        }
    }

    /**
     * Returns a mapping from the classes of the objects declared in the
     * rules to the number of occurrences of each one.
     *
     * @return a mapping from the classes of the objects declared in the
     *          rules to the number of occurrences of each one.
     */
    public java.util.Hashtable getClassDeclarationCount() {
        java.util.Hashtable result = new java.util.Hashtable();
        result.put("com.uisleandro.jeops.checkers.HumanMove", new Integer(1));
        result.put("com.uisleandro.jeops.checkers.Cell", new Integer(3));
        result.put("com.uisleandro.jeops.checkers.Task", new Integer(1));
        return result;
    }

    /*
     * The variables declared in the preconditions.
     */
    com.uisleandro.jeops.checkers.Task com_uisleandro_jeops_checkers_Task_1;
    com.uisleandro.jeops.checkers.Cell com_uisleandro_jeops_checkers_Cell_1;
    com.uisleandro.jeops.checkers.Cell local_decl_com_uisleandro_jeops_checkers_Cell_1;
    com.uisleandro.jeops.checkers.Cell com_uisleandro_jeops_checkers_Cell_2;
    com.uisleandro.jeops.checkers.Cell com_uisleandro_jeops_checkers_Cell_3;
    com.uisleandro.jeops.checkers.HumanMove com_uisleandro_jeops_checkers_HumanMove_1;

    /**
     * Constant used to aviod the creation of several empty arrays.
     */
    private static final Object[] EMPTY_OBJECT_ARRAY = new Object[0];

    /**
     * Constant used to aviod the creation of several empty double arrays.
     */
    private static final Object[][] EMPTY_OBJECT_DOUBLE_ARRAY = new Object[0][0];

    /**
     * Constant used to aviod the creation of several empty vectors.
     */
    private static final java.util.Vector EMPTY_VECTOR = new java.util.Vector();

    /**
     * Class constructor.
     *
     * @param objectBase an object base over with this rule base will work.
     * @param knowledgeBase the knowledge base that contains this rule base.
     */
    public Jeops_RuleBase_CheckersBase(jeops.ObjectBase objectBase,
                                jeops.AbstractKnowledgeBase knowledgeBase) {
        super(objectBase, knowledgeBase);
    }

};

/**
 * Knowledge base created by JEOPS from file ./src/main/java/com/uisleandro/jeops/checkers/CheckersBase.rules
 *
 * @version May 4, 2016
 */
public class CheckersBase extends jeops.AbstractKnowledgeBase {

    /**
     * Creates a new knowledge base with the specified conflict set with the
     * desired conflict resolution policy.
     *
     * @param conflictSet a conflict set with the desired resolution policy
     */
    public CheckersBase(jeops.conflict.ConflictSet conflictSet) {
        super(conflictSet);
        jeops.AbstractRuleBase ruleBase;
        jeops.ObjectBase objectBase = getObjectBase();
        ruleBase = new Jeops_RuleBase_CheckersBase(objectBase, this);
        setRuleBase(ruleBase);
    }

    /**
     * Creates a new knowledge base, using the default conflict resolution
     * policy.
     */
    public CheckersBase() {
        this(new jeops.conflict.DefaultConflictSet());
    }

}
