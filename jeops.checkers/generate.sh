#!/bin/sh

JAVA=/usr/local/bin/java/jdk1.8.0_77/bin/java
JAVAC=/usr/local/bin/java/jdk1.8.0_77/bin/javac

CP=./src/main/resources/jeops2.0.jar:./target/classes;
D=./target/classes


echo "compilling dependences"
$JAVAC -cp $CP ./src/main/java/com/uisleandro/jeops/checkers/Checker.java -d $D
$JAVAC -cp $CP ./src/main/java/com/uisleandro/jeops/checkers/Cell.java -d $D

echo "generating files"
$JAVA -cp $CP jeops.compiler.Main ./src/main/java/com/uisleandro/jeops/checkers/CheckersBase.rules
